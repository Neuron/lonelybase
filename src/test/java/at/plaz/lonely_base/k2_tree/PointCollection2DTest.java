package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Georg Plaz.
 */
abstract class PointCollection2DTest {
    public abstract <A> PointCollection2D<A> createInstance();
    public abstract Vector2D getFirstValid();
    public abstract Vector2D getSecondValid();
    public abstract Rectangle getContainsNone();
    public abstract Rectangle getContainsFirst();
    public abstract Rectangle getContainsBoth();
    
    @Test
    void testSizeEmpty() {
        PointCollection2D<String> instance = createInstance();
        assertEquals(0, instance.size());
    }

    @Test
    void testSizeOneElement() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        assertEquals(1, instance.size());
    }

    @Test
    void testSizeTwoElements() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        instance.store("bar", getSecondValid());
        assertEquals(2, instance.size());
    }

    @Test
    void testContainsNoneWhileNonePresent() {
        PointCollection2D<String> instance = createInstance();
        assertEquals(0, instance.get(getContainsBoth()).size());
    }

    @Test
    void testContainsNoneWhileOnePresent() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        assertEquals(0, instance.get(getContainsNone()).size());
    }

    @Test
    void testContainsNoneWhileTwoPresent() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        instance.store("bar", getSecondValid());
        assertEquals(0, instance.get(getContainsNone()).size());
    }

    @Test
    void testContainsFirst() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        assertEquals(1, instance.get(getContainsFirst()).size());
    }

    @Test
    void testContainsFirstWhileTwoPresent() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        instance.store("bar", getSecondValid());
        assertEquals(1, instance.get(getContainsFirst()).size());
    }

    @Test
    void testContainsBoth() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        instance.store("bar", getSecondValid());
        assertEquals(2, instance.get(getContainsBoth()).size());
    }
    
    
}