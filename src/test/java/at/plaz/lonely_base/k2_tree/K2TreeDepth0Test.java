package at.plaz.lonely_base.k2_tree;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Georg Plaz.
 */
class K2TreeDepth0Test extends K2TreeVariableDepthTest {

    @Override
    int getDepth() {
        return 0;
    }
}