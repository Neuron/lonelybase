package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;

/**
 * Created by Georg Plaz.
 */
abstract class K2TreeVariableDepthTest extends LimitedPointCollection2DTest {
    abstract int getDepth();

    @Override
    public <A> PointCollection2D<A> createInstance() {
        return new K2Tree<>(3, 1, getDepth());
    }

    @Override
    public Vector2D getFirstValid() {
        return Vector2D.of(0.5, 0.5);
    }

    @Override
    public Vector2D getSecondValid() {
        return Vector2D.of(1.5, 0.5);
    }

    @Override
    public Rectangle getContainsNone() {
        return Rectangle.ofFast(2, 0, 1, 1);
    }

    @Override
    public Rectangle getContainsFirst() {
        return Rectangle.ofFast(0, 0, 1, 1);
    }

    @Override
    public Rectangle getContainsBoth() {
        return Rectangle.ofFast(0, 0, 2, 1);
    }

    @Override
    public Rectangle getOutOfBoundsDisjunctRectangle() {
        return Rectangle.ofFast(0, 2, 1, 1);
    }

    @Override
    public Rectangle getOutOfBoundsOverlappingRectangle() {
        return Rectangle.ofFast(0, 0, 2, 2);
    }

    @Override
    public Vector2D getOutOfBoundsPoint() {
        return Vector2D.of(0, 2);
    }

    @Override
    public Rectangle getOutOfBoundsPointRectangle() {
        return Rectangle.ofFast(0, 1, 2, 2);
    }
}