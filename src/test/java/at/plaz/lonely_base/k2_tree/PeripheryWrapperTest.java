package at.plaz.lonely_base.k2_tree;

/**
 * Created by Georg Plaz.
 */
class PeripheryWrapperTest extends UnlimitedCollection2DTest {

    @Override
    public <A> PointCollection2D<A> createInstance() {
        K2Tree<A> delegate = new K2Tree<>(1, 1, 4);
        return new PeripheryWrapper<>(delegate);
    }
}