package at.plaz.lonely_base.k2_tree;

/**
 * Created by Georg Plaz.
 */
class K2TreeDepth2Test extends K2TreeVariableDepthTest {

    @Override
    int getDepth() {
        return 2;
    }
}