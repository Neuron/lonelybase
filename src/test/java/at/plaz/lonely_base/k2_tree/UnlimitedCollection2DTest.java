package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Georg Plaz.
 */
abstract class UnlimitedCollection2DTest extends PointCollection2DTest {

    @Override
    public Vector2D getFirstValid() {
        return Vector2D.of(0.5, 0.5);
    }

    @Override
    public Vector2D getSecondValid() {
        return Vector2D.of(100, 200);
    }

    @Override
    public Rectangle getContainsNone() {
        return Rectangle.ofFast(10, 10, 1, 1);
    }

    @Override
    public Rectangle getContainsFirst() {
        return Rectangle.ofFast(0, 0, 1, 1);
    }

    @Override
    public Rectangle getContainsBoth() {
        return Rectangle.ofFast(0, 0, 1000, 1000);
    }
}