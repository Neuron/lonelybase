package at.plaz.lonely_base.k2_tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Georg Plaz.
 */
class K2TreeDepth1Test extends K2TreeVariableDepthTest {

    @Override
    int getDepth() {
        return 1;
    }

}