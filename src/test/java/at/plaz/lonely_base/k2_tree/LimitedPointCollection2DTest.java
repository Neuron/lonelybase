package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.CollectionUtils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Created by Georg Plaz.
 */
abstract class LimitedPointCollection2DTest extends PointCollection2DTest {
    public abstract Vector2D getOutOfBoundsPoint();
    public abstract Rectangle getOutOfBoundsPointRectangle();
    public abstract Rectangle getOutOfBoundsOverlappingRectangle();
    public abstract Rectangle getOutOfBoundsDisjunctRectangle();

    @Test
    void testOutOfBoundsPoint() {
        assertDoesNotThrow(() -> {
            PointCollection2D<String> instance = createInstance();
            instance.store("foo", getOutOfBoundsPoint());
            String element = CollectionUtils.getOnlyElement(instance.get(getOutOfBoundsPointRectangle()));
            assertEquals("foo", element);
        });
    }

    @Test
    void testValidThenOutOfBoundsPoint() {
        assertDoesNotThrow(() -> {
            PointCollection2D<String> instance = createInstance();
            instance.store("foo", getFirstValid());
            instance.store("bar", getOutOfBoundsPoint());
        });
    }

    @Test
    void testGetOutOfBounds() {
        assertDoesNotThrow(() -> {
            PointCollection2D<String> instance = createInstance();
            instance.store("foo", getFirstValid());
            instance.get(getOutOfBoundsDisjunctRectangle());
        });
    }

    @Test
    void testGetOverlappingOutOfBounds() {
        assertDoesNotThrow(() -> {
            PointCollection2D<String> instance = createInstance();
            instance.store("foo", getFirstValid());
            instance.get(getOutOfBoundsOverlappingRectangle());
        });
    }
}