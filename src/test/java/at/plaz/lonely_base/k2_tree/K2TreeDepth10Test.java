package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by Georg Plaz.
 */
class K2TreeDepth10Test extends K2TreeVariableDepthTest {

    @Override
    int getDepth() {
        return 13;
    }

    @Test
    void testMassiveInput() {
        Random random = new Random(0);
        K2Tree<Integer> instance = new K2Tree<>(1, 1, getDepth());
        Rectangle bounding = Rectangle.ofFast(0.6, 0.3, 0.01, 0.02);
        Map<Integer, Vector2D> objects = new HashMap<>();
        int count = 1000000;
        for (int i = 0; i < count; i++) {
            Vector2D position = Vector2D.of(random.nextDouble(), random.nextDouble());
            instance.store(i, position);
            objects.put(i, position);
        }
        Collection<Integer> lambdaResult = objects.keySet()
                .stream()
                .filter(integer -> bounding.contains(objects.get(integer)))
                .collect(Collectors.toList());
        Collection<Integer> treeResult = instance.get(bounding);

        HashSet<Integer> lambdaSet = new HashSet<>(lambdaResult);
        treeResult.forEach(i -> assertTrue(lambdaSet.contains(i)));
        assertEquals(lambdaResult.size(), treeResult.size());

        instance.getBalances().forEach(i -> assertTrue(i > 50));
        instance.getBalances().forEach(i -> assertTrue(i < 200, "balance was " + i));
    }

    @Test
    void testSizeTwoElements() {
        PointCollection2D<String> instance = createInstance();
        instance.store("foo", getFirstValid());
        instance.store("bar", getSecondValid());
        assertEquals(2, instance.size());
    }
}