package at.plaz.lonely_base.streams;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Georg Plaz.
 */
class ExtraCollectorsTest {


    @Test
    void toSingleton() {
        assertEquals("foo", List.of("foo").stream().collect(ExtraCollectors.toSingleton()).get());

    }

    @Test
    void toSingletonZeroThrows() {
        assertFalse(List.of().stream().collect(ExtraCollectors.toSingleton()).isPresent());
    }

    @Test
    void toSingletonTwoThrows() {
        assertFalse(List.of("foo", "bar").stream().collect(ExtraCollectors.toSingleton()).isPresent());
    }
}