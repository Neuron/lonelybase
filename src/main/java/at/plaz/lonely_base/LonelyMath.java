package at.plaz.lonely_base;

import java.util.Random;

/**
 * Created by Georg Plaz.
 */
public class LonelyMath {
    public static final int DEFAULT_MIN_WEIGHT = 0;
    public static final int DEFAULT_MAX_WEIGHT = 1;

    public static double map(double toMap, double inMin, double inMax, double outMin, double outMax) {
        return (toMap - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    public static double snip(double value, double min, double max) {
        return java.lang.Math.min(java.lang.Math.max(min, value), max);
    }

    public static double[][] randomMatrix(int columns, int rows, Random random) {
        return randomMatrix(columns, rows, random);
    }

    public static double[][] randomMatrix(int columns, int rows, Random random, double minWeight, double maxWeight) {
        double[][] doubles = new double[columns][rows];
        for (int x = 0; x < doubles.length; x++) {
            for (int y = 0; y < doubles[x].length; y++) {
                doubles[x] = randomVector(rows, random, minWeight, maxWeight);
            }
        }
        return doubles;
    }

    public static double[] randomVector(int length, Random random) {
        return randomVector(length, random, DEFAULT_MIN_WEIGHT, DEFAULT_MAX_WEIGHT);
    }

    public static double[] randomVector(int length, Random random, double minWeight, double maxWeight) {
        double[] vector = new double[length];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = map(random.nextDouble(), 0, 1, minWeight, maxWeight);
        }
        return vector;
    }
}
