package at.plaz.lonely_base.math;

import java.util.Objects;
import java.util.stream.Stream;


/**
 * Created by Georg Plaz.
 */
public class PixelDirection extends Direction2D {
    public static final PixelDirection[][] DIRECTIONS = new PixelDirection[3][3];
    public static final PixelDirection[] DIRECTIONS_FLATTENED = new PixelDirection[3*3-1];
    private static int DIRECTIONS_COUNT = 0;

    public static PixelDirection of(Sign x, Sign y) {
        return DIRECTIONS[x.getIndex()][y.getIndex()];
    }

    public static PixelDirection of(int x, int y) {
        return DIRECTIONS[x + 1][y + 1];
    }

    public static PixelDirection inDirection(int dimension, boolean positive) {
        int isPositiveIndex = positive ? 2 : 0;
        switch (dimension % 2) {
            case 0:
                return DIRECTIONS[isPositiveIndex][1];
            case 1:
                return DIRECTIONS[1][isPositiveIndex];
            default:
                throw new RuntimeException();
        }
    }

    private IntVector2D intDirection;
    private Sign xSign;
    private Sign ySign;

    private PixelDirection(Sign x, Sign y) {
        super(x.getValue(), y.getValue());
        DIRECTIONS[x.getIndex()][y.getIndex()] = this;
        DIRECTIONS_FLATTENED[DIRECTIONS_COUNT++] = this;
        intDirection = IntVector2D.of(x.getValue(), y.getValue());
        this.xSign = x;
        this.ySign = y;
    }

    public static PixelDirection getClosest(Vector2D triplet) {
        double minDistance = Double.MAX_VALUE;
        PixelDirection minDirection = null;
        for (PixelDirection direction : DIRECTIONS_FLATTENED) {
            double distance = direction.getSubtracted(triplet).length();
            if (minDistance > distance) {
                minDistance = distance;
                minDirection = direction;
            }
        }
        return minDirection;
    }

    public IntVector2D asInts() {
        return intDirection;
    }

    public PixelDirection flipped() {
        return DIRECTIONS[xSign.flipped().getIndex()][ySign.flipped().getIndex()];
    }

    public Sign getxSign() {
        return xSign;
    }

    public Sign getySign() {
        return ySign;
    }

    static {
        for (Sign x : Sign.values()) {
            for (Sign y : Sign.values()) {
                if (DIRECTIONS[x.getIndex()][y.getIndex()] == null && !(Stream.of(x, y).allMatch(s -> Objects.equals(s, Sign.ZERO)))) {
                    new PixelDirection(x, y);
                }
            }
        }
    }
}
