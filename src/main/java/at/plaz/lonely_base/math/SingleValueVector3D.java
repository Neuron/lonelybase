package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SingleValueVector3D extends Vector3DBase implements ImmutableVector3D {
    private final double value;

    public SingleValueVector3D(double value) {
        this.value = value;
    }

    @Override
    public double getX() {
        return value;
    }

    @Override
    public double getY() {
        return value;
    }

    @Override
    public double getZ() {
        return value;
    }

    @Override
    public boolean allEqual(double other) {
        return value == other;
    }

    @Override
    public boolean allGreater(double other) {
        return value > other;
    }

    @Override
    public boolean allGreaterEqual(double other) {
        return value >= other;
    }

    @Override
    public ImmutableVector3D getAdded(double value) {
        double newValue = this.value + value;
        return newValue == 0 ? ZEROS : (newValue == 1 ? ONES : new SingleValueVector3D(newValue));
    }

    @Override
    public ImmutableVector3D getMultiplied(double factor) {
        return factor == 0 ? ZEROS : (factor == 1 ? this : new SingleValueVector3D(factor * value));
    }
}
