package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableIntVector3D extends IntVector3D {

    default ImmutableVector3D toVector3D() {
        return new SimpleImmutableVector3D(getX(), getY(), getZ());
    }

    default ImmutableIntVector3D truncateToInts() {
        return this;
    }

    @Override
    default ImmutableIntVector3D immutable() {
        return this;
    }

    @Override
    default MutableIntVector3D mutable() {
        return new SimpleMutableIntVector3D(getX(), getY(), getZ());
    }

    @Override
    default ImmutableIntVector3D getAdded(IntVector3D delta) {
        return delta.getAddedImmutable(this);
    }

    @Override
    default ImmutableIntVector3D getAdded(int x, int y, int z) {
        return (x == 0 && y == 0 && z == 0) ? this : new SimpleImmutableIntVector3D(getX() + x, getY() + y, getZ() + z);
    }

    @Override
    default ImmutableIntVector3D getAdded(int value) {
        return value == 0 ? this : new SimpleImmutableIntVector3D(getX() + value, getY() + value, getZ() + value);
    }

    @Override
    default ImmutableIntVector3D getMultiplied(IntVector3D delta) {
        return delta.getMultipliedImmutable(this);
    }

    @Override
    default ImmutableIntVector3D getMultiplied(int x, int y, int z) {
        if (x == 1 && y == 1 && z == 1) {
            return this;
        } else {
            return (x == 0 && y == 0 && z == 0) ? ZEROS : new SimpleImmutableIntVector3D(getX() * x, getY() * y, getZ() * z);
        }
    }

    @Override
    default ImmutableIntVector3D getMultiplied(int value) {
        if (value == 1) {
            return this;
        } else {
            return (value == 0) ? ZEROS : new SimpleImmutableIntVector3D(getX() * value, getY() * value, getZ() * value);
        }
    }
}
