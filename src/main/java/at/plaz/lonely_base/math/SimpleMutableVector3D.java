package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SimpleMutableVector3D extends Vector3DBase implements MutableVector3D {
    private double x;
    private double y;
    private double z;

    public SimpleMutableVector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public ImmutableIntVector3D truncateToInts() {
        return new SimpleImmutableIntVector3D((int)x, (int)y, (int)z);
    }

    public ImmutableVector3D toVector3D() {
        return new SimpleImmutableVector3D(x, y, z);
    }

    public SimpleMutableVector3D(double[] values) {
        this(values[0], values[1], values[2]);
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public ImmutableVector3D immutable() {
        return new SimpleImmutableVector3D(x, y, z);
    }

}
