package at.plaz.lonely_base.math;


import static at.plaz.lonely_base.math.Sign.*;

/**
 * Created by Georg Plaz.
 */
public class Direction3D extends Vector3DBase implements ImmutableVector3D {

    private double x;
    private double y;
    private double z;

    public Direction3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        double length = ImmutableVector3D.super.length();
        this.x /= length;
        this.y /= length;
        this.z /= length;
    }

    public Direction3D(Vector3D triplet) {
        this(triplet.getX(), triplet.getY(), triplet.getZ());
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double length() {
        return 1;
    }
}
