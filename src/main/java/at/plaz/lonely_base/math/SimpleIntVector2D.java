package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.StringTuple;
import at.plaz.lonely_base.tuples.Tuple;
import at.plaz.lonely_base.tuples.Tuples;

import java.awt.*;
import java.util.Random;

/**
 * Created by Georg Plaz.
 */
public class SimpleIntVector2D extends IntVector2DBase implements MutableIntVector2D {
    private int first;
    private int second;

    public SimpleIntVector2D(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public SimpleIntVector2D(Tuple<Integer, Integer> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleIntVector2D(int values) {
        this(values, values);
    }

    public SimpleIntVector2D(Integer[] values) {
        this(values[0], values[1]);
    }

    public SimpleIntVector2D(int[] array) {
        this(array[0], array[1]);
    }

    public SimpleIntVector2D(String toParse) {
        this(Tuples.parseIntTuple(toParse));
    }

    public SimpleIntVector2D(StringTuple valuesToParse) {
        this(Integer.valueOf(valuesToParse.getFirst()), Integer.valueOf(valuesToParse.getSecond()));
    }

    public SimpleIntVector2D(Random random, int upperLimit) {
        this(random.nextInt(upperLimit), random.nextInt(upperLimit));
    }

    public SimpleIntVector2D(Random random, ImmutableIntVector2D lowerBounds, ImmutableIntVector2D upperBounds) {
        this(lowerBounds.getFirst()+random.nextInt(upperBounds.getX()-lowerBounds.getX()),
                lowerBounds.getSecond()+random.nextInt(upperBounds.getY()-lowerBounds.getY()));
    }

    public SimpleIntVector2D(Point point) {
        this(point.x, point.y);
    }

    public SimpleIntVector2D(Dimension dimension) {
        this(dimension.width, dimension.height);
    }

    public int getX() {
        return first;
    }

    public int getY() {
        return second;
    }

    @Override
    public void setX(int first) {
        this.first = first;
    }

    @Override
    public void setY(int second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "("+first+", "+second+")";
    }
}
