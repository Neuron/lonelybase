package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableIntVector3D extends IntVector3DBase implements ImmutableIntVector3D {
    private final int x;
    private final int y;
    private final int z;

    public SimpleImmutableIntVector3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public SimpleImmutableIntVector3D(int[] values) {
        this(values[0], values[1], values[2]);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getZ() {
        return z;
    }

}
