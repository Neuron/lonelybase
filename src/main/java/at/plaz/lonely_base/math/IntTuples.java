package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.NumberTuple;

import java.awt.*;

/**
 * Created by Georg Plaz.
 */
public class IntTuples {
    public static final ImmutableIntVector2D ZEROS = new SimpleImmutableIntVector2D(0);
    public static final ImmutableIntVector2D ONES = new SimpleImmutableIntVector2D(1);
    public static final ImmutableIntVector2D NEGATIVE_ONES = new SimpleImmutableIntVector2D(-1);

    public static final ImmutableIntVector2D NORTH = new SimpleImmutableIntVector2D(0, 1);
    public static final ImmutableIntVector2D NORTH_EAST = ONES;
    public static final ImmutableIntVector2D EAST = new SimpleImmutableIntVector2D(1, 0);
    public static final ImmutableIntVector2D SOUTH_EAST = new SimpleImmutableIntVector2D(1, -1);
    public static final ImmutableIntVector2D SOUTH = new SimpleImmutableIntVector2D(0, -1);
    public static final ImmutableIntVector2D SOUTH_WEST = new SimpleImmutableIntVector2D(0, -1);
    public static final ImmutableIntVector2D WEST = NEGATIVE_ONES;
    public static final ImmutableIntVector2D NORTH_WEST = new SimpleImmutableIntVector2D(-1, 1);

    public static boolean equals(IntVector2D tuple, Object o) {
        if (tuple == o) return true;
        if (!(o instanceof IntVector2D)) return false;

        IntVector2D other = (IntVector2D) o;

        return tuple.getX() == other.getX() && tuple.getY() == other.getY();
    }

    public static int hashCode(IntVector2D tuple) {
        return tuple.getX()*31 + tuple.getY();
    }

    public static ImmutableIntVector2D toTupleF(int first, int second) {
        return new SimpleImmutableIntVector2D(first, second);
    }

    public static ImmutableIntVector2D toTupleF(IntVector2D tuple) {
        return new SimpleImmutableIntVector2D(tuple);
    }

    public static ImmutableIntVector2D toTupleF(NumberTuple tuple) {
        return new SimpleImmutableIntVector2D(tuple);
    }

    public static ImmutableIntVector2D toTupleF(Dimension dimension) {
        return new SimpleImmutableIntVector2D(dimension);
    }

    public static ImmutableIntVector2D toTupleF(Point point) {
        return new SimpleImmutableIntVector2D(point);
    }

    public static ImmutableIntVector2D add(int firstX, int firstY, int secondX, int secondY) {
        return new SimpleImmutableIntVector2D(firstX + secondX, firstY + secondY);
    }

    public static ImmutableIntVector2D add(IntVector2D first, int secondX, int secondY) {
        return add(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableIntVector2D add(int firstX, int firstY, IntVector2D second) {
        return add(firstX, firstY, second.getX(), second.getY());
    }

    public static ImmutableIntVector2D add(IntVector2D first, IntVector2D second) {
        return add(first.getX(), first.getY(), second.getX(), second.getY());
    }

    public static ImmutableIntVector2D add(IntVector2D first, int value) {
        return add(first.getX(), first.getY(), value, value);
    }

    public static ImmutableIntVector2D subtract(int firstX, int firstY, int secondX, int secondY) {
        return new SimpleImmutableIntVector2D(firstX - secondX, firstY - secondY);
    }

    public static ImmutableIntVector2D subtract(IntVector2D first, int secondX, int secondY) {
        return subtract(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableIntVector2D subtract(int firstX, int firstY, IntVector2D second) {
        return subtract(firstX, firstY, second.getX(), second.getY());
    }

    public static ImmutableIntVector2D subtract(IntVector2D first, IntVector2D second) {
        return subtract(first.getX(), first.getY(), second.getX(), second.getY());
    }

    public static ImmutableIntVector2D subtract(IntVector2D first, int value) {
        return subtract(first.getX(), first.getY(), value, value);
    }

    public static ImmutableIntVector2D multiply(int firstX, int firstY, int secondX, int secondY) {
        return new SimpleImmutableIntVector2D(firstX * secondX, firstY * secondY);
    }

    public static ImmutableIntVector2D multiply(IntVector2D first, int secondX, int secondY) {
        return multiply(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableIntVector2D multiply(int firstX, int firstY, IntVector2D second) {
        return multiply(firstX, firstY, second.getX(), second.getY());
    }

    public static ImmutableIntVector2D multiply(IntVector2D first, IntVector2D second) {
        return multiply(first.getX(), first.getY(), second.getX(), second.getY());
    }

    public static ImmutableIntVector2D multiply(IntVector2D first, int value) {
        return multiply(first.getX(), first.getY(), value, value);
    }

    public static ImmutableIntVector2D divide(int firstX, int firstY, int secondX, int secondY) {
        return new SimpleImmutableIntVector2D(firstX / secondX, firstY / secondY);
    }

    public static ImmutableIntVector2D divide(IntVector2D first, int secondX, int secondY) {
        return divide(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableIntVector2D divide(int firstX, int firstY, IntVector2D second) {
        return divide(firstX, firstY, second.getX(), second.getY());
    }

    public static ImmutableIntVector2D divide(IntVector2D first, IntVector2D second) {
        return divide(first.getX(), first.getY(), second.getX(), second.getY());
    }

    public static ImmutableIntVector2D divide(IntVector2D first, int value) {
        return divide(first.getX(), first.getY(), value, value);
    }
}
