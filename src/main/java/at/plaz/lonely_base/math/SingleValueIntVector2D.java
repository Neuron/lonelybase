package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SingleValueIntVector2D extends IntVector2DBase implements ImmutableIntVector2D {
    private final int value;

    public SingleValueIntVector2D(int value) {
        this.value = value;
    }

    @Override
    public int getX() {
        return value;
    }

    @Override
    public int getY() {
        return value;
    }

    @Override
    public boolean greaterThan(int other) {
        return value > other;
    }

    @Override
    public boolean greaterEqualThan(int other) {
        return value >= other;
    }

    @Override
    public ImmutableIntVector2D getAdded(int value) {
        int newValue = this.value + value;
        return newValue == 0 ? ZEROS : (newValue == 1 ? ONES : new SingleValueIntVector2D(newValue));
    }

    @Override
    public ImmutableIntVector2D getMultiplied(int factor) {
        return factor == 0 ? ZEROS : new SingleValueIntVector2D(factor * value);
    }
}
