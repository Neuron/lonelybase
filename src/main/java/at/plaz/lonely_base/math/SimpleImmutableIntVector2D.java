package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.StringTuple;
import at.plaz.lonely_base.tuples.Tuple;
import at.plaz.lonely_base.tuples.Tuples;

import java.awt.*;
import java.util.Random;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableIntVector2D extends IntVector2DBase implements ImmutableIntVector2D {
    private final int first;
    private final int second;

    public SimpleImmutableIntVector2D(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public SimpleImmutableIntVector2D(Tuple<Integer, Integer> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleImmutableIntVector2D(int values) {
        this(values, values);
    }

    public SimpleImmutableIntVector2D(Integer[] values) {
        this(values[0], values[1]);
    }

    public SimpleImmutableIntVector2D(int[] array) {
        this(array[0], array[1]);
    }

    public SimpleImmutableIntVector2D(String toParse) {
        this(Tuples.parseIntTuple(toParse));
    }

    public SimpleImmutableIntVector2D(StringTuple valuesToParse) {
        this(Integer.valueOf(valuesToParse.getFirst()), Integer.valueOf(valuesToParse.getSecond()));
    }

    public SimpleImmutableIntVector2D(Random random, int upperLimit) {
        this(random.nextInt(upperLimit), random.nextInt(upperLimit));
    }

    public SimpleImmutableIntVector2D(Random random, ImmutableIntVector2D lowerBounds, ImmutableIntVector2D upperBounds) {
        this(lowerBounds.getFirst()+random.nextInt(upperBounds.getX()-lowerBounds.getX()),
                lowerBounds.getSecond()+random.nextInt(upperBounds.getY()-lowerBounds.getY()));
    }

    public SimpleImmutableIntVector2D(Point point) {
        this(point.x, point.y);
    }

    public SimpleImmutableIntVector2D(Dimension dimension) {
        this(dimension.width, dimension.height);
    }

    public int getX() {
        return first;
    }

    public int getY() {
        return second;
    }

    @Override
    public ImmutableIntVector2D toFinal() {
        return this;
    }

    @Override
    public String toString() {
        return "("+first+", "+second+")";
    }
}
