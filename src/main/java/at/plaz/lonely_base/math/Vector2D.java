package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.*;

import java.awt.*;
import java.util.Collection;

/**
 * Created by Georg Plaz.
 */
public interface Vector2D extends NumberTuple<Double> {

    ImmutableVector2D ZEROS = new SingleValueVector2D(0) {
        @Override
        public ImmutableVector2D getAddedImmutable(ImmutableVector2D delta) {
            return delta;
        }

        @Override
        public ImmutableVector2D getMultipliedImmutable(ImmutableVector2D factor) {
            return this;
        }
    };

    ImmutableVector2D ONES = new SingleValueVector2D(0) {
        @Override
        public ImmutableVector2D getMultipliedImmutable(ImmutableVector2D factor) {
            return factor;
        }
    };

    static ImmutableVector2D of(double x, double y) {
        return new SimpleImmutableVector2D(x, y);
    }

    static ImmutableVector2D of(double[] values) {
        return new SimpleImmutableVector2D(values);
    }

    static ImmutableVector2D of(double value) {
        return new SimpleImmutableVector2D(value);
    }

    static ImmutableVector2D of(Vector3D value, int dimensionToStrip) {
        return value.stripDimension(dimensionToStrip);
    }

    static ImmutableVector2D of(int first, int second) {
        return new SimpleImmutableVector2D(first, second);
    }

    static ImmutableVector2D of(Vector2D tuple) {
        return new SimpleImmutableVector2D(tuple);
    }

    static ImmutableVector2D of(Dimension dimension) {
        return new SimpleImmutableVector2D(dimension);
    }

    static ImmutableVector2D of(Point point) {
        return new SimpleImmutableVector2D(point);
    }

    double getX();

    double getY();

    @Override
    default Vector2D toDoubleVector() {
        return this;
    }

    ImmutableVector2D immutable();

    default MutableVector2D mutable() {
        return new SimpleMutableVector2D(getX(), getY());
    }

    @Override
    default ImmutableVector2D getSwapped() {
        return new SimpleImmutableVector2D(getY(), getX());
    }

    @Override
    default Double getFirst() {
        return getX();
    }

    @Override
    default Double getSecond() {
        return getY();
    }

    default ImmutableBooleanTuple isNaN() {
        return new SimpleImmutableBooleanTuple(Double.isNaN(getX()), Double.isNaN(getY()));
    }

    default boolean anyIsNaN() {
        return Double.isNaN(getX()) || Double.isNaN(getY());
    }

    default ImmutableIntVector2D roundToIntVector() {
        return new SimpleImmutableIntVector2D(Math.round(getFirst().floatValue()), Math.round(getSecond().floatValue()));
    }

    default ImmutableVector2D getAdded(Vector2D delta) {
        return delta.getAdded(getX(), getY());
    }

    default ImmutableVector2D getAddedImmutable(ImmutableVector2D delta) {
        return new SimpleImmutableVector2D(getX() + delta.getX(), getY() + delta.getY());
    }

    default ImmutableVector2D getAdded(double x, double y) {
        return new SimpleImmutableVector2D(getX() + x, getY() + y);
    }

    default ImmutableVector2D getAdded(double value) {
        return new SimpleImmutableVector2D(getX() + value, getY() + value);
    }

    default ImmutableVector2D getSubtracted(Vector2D delta) {
        return new SimpleImmutableVector2D(getX() - delta.getX(), getY() -  delta.getY());
    }

    default ImmutableVector2D getSubtractedImmutable(ImmutableVector2D delta) {
        return new SimpleImmutableVector2D(getX() - delta.getX(), getY() - delta.getY());
    }

    default ImmutableVector2D getSubtracted(double x, double y) {
        return new SimpleImmutableVector2D(getX() - x, getY() - y);
    }

    default ImmutableVector2D getSubtracted(double value) {
        return new SimpleImmutableVector2D(getX() - value, getY() - value);
    }

    default ImmutableVector2D getMultiplied(Vector2D factor) {
        return factor.getMultiplied(getX(), getY());
    }

    default ImmutableVector2D getMultipliedImmutable(ImmutableVector2D factor) {
        return new SimpleImmutableVector2D(getX() * factor.getX(), getY() * factor.getY());
    }

    default ImmutableVector2D getMultiplied(double x, double y) {
        return new SimpleImmutableVector2D(getX() * x, getY() * y);
    }

    default ImmutableVector2D getMultiplied(double factor) {
        return new SimpleImmutableVector2D(getX() * factor, getY() * factor);
    }

    default ImmutableVector2D getDividedImmutable(ImmutableVector2D dividend) {
        return new SimpleImmutableVector2D(getX() / dividend.getX(), getY() / dividend.getY());
    }

    default ImmutableVector2D getDivided(double dividendX, double dividendY) {
        return new SimpleImmutableVector2D(getX() / dividendX, getY() / dividendY);
    }

    default ImmutableVector2D getDivided(double dividend) {
        return new SimpleImmutableVector2D(getX() / dividend, getY() / dividend);
    }

    default ImmutableVector2D getRotatedCCW(double radians) {
        double x = getX() * Math.cos(radians) - getY() * Math.sin(radians);
        double y = getX() * Math.sin(radians) + getY() * Math.cos(radians);
        return Vector2D.of(x, y);
    }

    default ImmutableVector2D getRotatedCW(double radians) {
        return getRotatedCCW(-radians);
    }

    default double max() {
        return Math.max(getX(), getY());
    }

    default double min() {
        return Math.min(getX(), getY());
    }

    default double[] toArrayUnboxed() {
        return new double[]{getX(), getY()};
    }

    default Vector2D min(double first, double second) {
        if (getX() <= first && getY() <= second) {
            return this;
        }
        return new SimpleImmutableVector2D(Math.min(getX(), first), Math.min(getY(), second));
    }

    default ImmutableVector2D minF(Vector2D other) {
        if (bothSmallerEqual(other)) {
            if (this instanceof ImmutableVector2D) {
                return (ImmutableVector2D) this;
            }
        } else if (other.bothSmallerEqual(this)) {
            if (other instanceof ImmutableVector2D) {
                return (ImmutableVector2D) other;
            }
        }
        return new SimpleImmutableVector2D(Math.min(getX(), other.getX()), Math.min(getY(), other.getY()));
    }

    default Vector2D min(Vector2D other) {
        if (bothSmallerEqual(other)) {
            return this;
        } else if (other.bothSmallerEqual(this)) {
            return other;
        }
        return new SimpleImmutableVector2D(Math.min(getX(), other.getX()), Math.min(getY(), other.getY()));
    }

    default Vector2D max(double first, double second) {
        if (getX()>=first && getY()>=second) {
            return this;
        }
        return new SimpleImmutableVector2D(Math.max(getX(), first), Math.max(getY(), second));
    }

    default Vector2D max(Vector2D other) {
        if (bothGreaterEqual(other)) {
            return this;
        } else if (other.bothGreaterEqual(this)) {
            return other;
        }
        return new SimpleImmutableVector2D(Math.max(getX(), other.getX()), Math.max(getY(), other.getY()));
    }

    default ImmutableVector2D maxF(Vector2D other) {
        if (bothGreaterEqual(other)) {
            if (this instanceof ImmutableVector2D) {
                return (ImmutableVector2D) this;
            }
        } else if (other.bothGreaterEqual(this)) {
            if (other instanceof ImmutableVector2D) {
                return (ImmutableVector2D) other;
            }
        }
        return new SimpleImmutableVector2D(Math.max(getX(), other.getX()), Math.max(getY(), other.getY()));
    }

    default boolean containedIn(Tuple<Vector2D, Vector2D> box) {
        return bothGreaterEqual(box.getFirst()) &&
                bothSmallerEqual(box.getSecond());
    }

    default boolean containedIn(Collection<Tuple<Vector2D, Vector2D>> boxes) {
        for(Tuple<Vector2D, Vector2D> box : boxes) {
            if (containedIn(box)) {
                return true;
            }
        }
        return false;
    }

    default boolean bothGreaterEqual(Vector2D other) {
        return bothGreaterEqual(other.getX(), other.getY());
    }

    default boolean bothGreaterEqual(double first, double second) {
        return getX()>=first && getY()>=second;
    }

    default boolean anyGreaterEqual(Vector2D other) {
        return anyGreaterEqual(other.getX(), other.getY());
    }

    default boolean anyGreaterEqual(double first, double second) {
        return getX()>=first || getY()>=second;
    }

    default boolean bothSmallerEqual(Vector2D other) {
        return getX()<=other.getX() && getY()<=other.getY();
    }

    default boolean anySmallerEqual(Vector2D other) {
        return getX()<=other.getX() || getY()<=other.getY();
    }

    default double sum() {
        return getX() + getY();
    }

    default boolean equals(double first, double second) {
        return getX() == first && getY() == second;
    }

    default double distance(Vector2D other) {
        return Vector2Ds.subtract(this, other).length();
    }

    default double length() {
        return Math.sqrt(getX() * getX() + getY() * getY());
    }

}
