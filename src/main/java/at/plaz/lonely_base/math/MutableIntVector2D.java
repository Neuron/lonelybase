package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.MutableNumberTuple;

/**
 * Created by Georg Plaz
 */
public interface MutableIntVector2D extends MutableNumberTuple<Integer>, IntVector2D {
    void setX(int first);

    void setY(int second);

    @Override
    default void setFirst(Integer first) {
        setX(first);
    }

    @Override
    default void setSecond(Integer second) {
        setY(second);
    }

    default void set(IntVector2D center) {
        setX(center.getX());
        setY(center.getY());
    }

    default void set(int first, int second) {
        setX(first);
        setY(second);
    }
}
