package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public enum Sign {
    PLUS(1), ZERO(0), MINUS(-1);

    private int value;
    private int index;

    Sign(int value) {
        this.value = value;
        this.index = value + 1;
    }

    public int getIndex() {
        return index;
    }

    public int getValue() {
        return value;
    }

    public Sign flipped() {
        return this == ZERO ? ZERO : (this == PLUS ? MINUS : PLUS);
    }
}
