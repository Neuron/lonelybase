package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.NumberTuple;
import at.plaz.lonely_base.tuples.Tuple;

import java.awt.*;

/**
 * Created by Georg Plaz.
 */
public interface IntVector2D extends NumberTuple<Integer> {
    ImmutableIntVector2D ZEROS = new SingleValueIntVector2D(0) {
        @Override
        public ImmutableIntVector2D getAddedImmutable(ImmutableIntVector2D delta) {
            return delta;
        }

        @Override
        public ImmutableIntVector2D getMultipliedImmutable(ImmutableIntVector2D factor) {
            return this;
        }
    };

    ImmutableIntVector2D ONES = new SingleValueIntVector2D(0) {
        @Override
        public ImmutableIntVector2D getMultipliedImmutable(ImmutableIntVector2D factor) {
            return factor;
        }
    };

    static ImmutableIntVector2D of(int x, int y) {
        return new SimpleImmutableIntVector2D(x, y);
    }

    static ImmutableIntVector2D of(int[] values) {
        return new SimpleImmutableIntVector2D(values);
    }

    static ImmutableIntVector2D of(String toParse) {
        return new SimpleImmutableIntVector2D(toParse);
    }

    static ImmutableIntVector2D of(int value) {
        return new SingleValueIntVector2D(value);
    }

    @Override
    default IntVector2D flipped() {
        return IntVector2D.of(getY(), getX());
    }

    int getX();

    int getY();

    @Override
    default Integer getFirst() {
        return getX();
    }

    @Override
    default Integer getSecond() {
        return getY();
    }

    default ImmutableIntVector2D getAdded(IntVector2D delta) {
        return delta.getAdded(getX(), getY());
    }

    default ImmutableVector2D getAdded(Vector2D delta) {
        return delta.getAdded(getX(), getY());
    }

    default ImmutableIntVector2D getAddedImmutable(ImmutableIntVector2D delta) {
        return new SimpleImmutableIntVector2D(getX() + delta.getX(), getY() + delta.getY());
    }

    default ImmutableVector2D getAddedImmutable(ImmutableVector2D delta) {
        return new SimpleImmutableVector2D(getX() + delta.getX(), getY() + delta.getY());
    }

    default ImmutableIntVector2D getAdded(int x, int y) {
        return new SimpleImmutableIntVector2D(getX() + x, getY() + y);
    }

    default ImmutableVector2D getAdded(double x, double y) {
        return new SimpleImmutableVector2D(getX() + x, getY() + y);
    }

    default ImmutableIntVector2D getAdded(int value) {
        return new SimpleImmutableIntVector2D(getX() + value, getY() + value);
    }

    default ImmutableVector2D getAdded(double value) {
        return new SimpleImmutableVector2D(getX() + value, getY() + value);
    }

    default ImmutableIntVector2D getSubtracted(IntVector2D delta) {
        return new SimpleImmutableIntVector2D(getX() - delta.getX(), getY() - delta.getY());
    }

    default ImmutableIntVector2D getSubtractedImmutable(ImmutableIntVector2D delta) {
        return new SimpleImmutableIntVector2D(getX() - delta.getX(), getY() - delta.getY());
    }

    default ImmutableIntVector2D getSubtracted(int x, int y) {
        return new SimpleImmutableIntVector2D(getX() - x, getY() - y);
    }

    default ImmutableIntVector2D getSubtracted(int value) {
        return new SimpleImmutableIntVector2D(getX() - value, getY() - value);
    }

    default ImmutableIntVector2D getMultiplied(IntVector2D factor) {
        return factor.getMultiplied(getX(), getY());
    }

    default ImmutableIntVector2D getMultipliedImmutable(ImmutableIntVector2D factor) {
        return new SimpleImmutableIntVector2D(getX() * factor.getX(), getY() * factor.getY());
    }

    default SimpleImmutableVector2D getMultipliedImmutable(ImmutableVector2D factor) {
        return new SimpleImmutableVector2D(getX() * factor.getX(), getY() * factor.getY());
    }

    default ImmutableIntVector2D getMultiplied(int x, int y) {
        return new SimpleImmutableIntVector2D(getX() * x, getY() * y);
    }

    default ImmutableVector2D getMultiplied(double x, double y) {
        return new SimpleImmutableVector2D(getX() * x, getY() * y);
    }

    default ImmutableIntVector2D getMultiplied(int factor) {
        return new SimpleImmutableIntVector2D(getX() * factor, getY() * factor);
    }

    default ImmutableVector2D getMultiplied(double factor) {
        return new SimpleImmutableVector2D(getX() * factor, getY() * factor);
    }

    default ImmutableIntVector2D toFinal() {
        return new SimpleImmutableIntVector2D(this);
    }

    default int sum() {
        return getX() + getY();
    }

    default IntVector2D div(int first, int second) {
        return new SimpleImmutableIntVector2D(getX() / first, getY() / second);
    }

    default SimpleImmutableIntVector2D abs() {return new SimpleImmutableIntVector2D(Math.abs(getX()), Math.abs(getY()));}

    default int max() {
        return Math.max(getX(), getY());
    }

    default int min() {
        return Math.min(getX(), getY());
    }

    default Dimension toAWTDimension() {
        return new Dimension(getX(), getY());
    }

    default IntVector2D min(int first, int second) {
        if (smallerEqualThan(first, second)) {
            return this;
        }
        return new SimpleImmutableIntVector2D(Math.min(getX(),first), Math.min(getY(), second));
    }

    default IntVector2D min(IntVector2D other) {
        if (smallerEqualThan(other)) {
            return this;
        } else if (other.smallerEqualThan(this)) {
            return other;
        }
        return new SimpleImmutableIntVector2D(
                Math.min(getX(), other.getX()),
                Math.min(getY(), other.getY()));
    }

    default IntVector2D max(IntVector2D other) {
        if (greaterEqualThan(other)) {
            return this;
        } else if (other.greaterEqualThan(this)) {
            return other;
        }
        return new SimpleImmutableIntVector2D(Math.max(getX(), other.getX()), Math.max(getY(), other.getY()));
    }

    default boolean smallerEqualThan(IntVector2D other) {
        return smallerEqualThan(other.getX(), other.getY());
    }

    default boolean smallerEqualThan(int first, int second) {
        return getX() <= first && getY() <= second;
    }

    default boolean smallerThan(IntVector2D other) {
        return smallerThan(other.getX(), other.getY());
    }

    default boolean smallerThan(int first, int second) {
        return getX() < first && getY() < second;
    }

    default boolean greaterThan(IntVector2D other) {
        return getX() > other.getX() && getY() > other.getY();
    }

    default boolean greaterThan(int other) {
        return getX() > other && getY() > other;
    }

    default boolean greaterEqualThan(IntVector2D other) {
        return getX() >= other.getX() && getY() >= other.getY();
    }

    default boolean greaterEqualThan(int other) {
        return getX() >= other && getY() >= other;
    }

    @Override
    default IntVector2D toIntVector() {
        return this;
    }

    static ImmutableIntVector2D add(IntVector2D first, IntVector2D second) {
        return first.getAdded(second);
    }

    static ImmutableIntVector2D add(IntVector2D first, int x, int y) {
        return first.getAdded(x, y);
    }

    static ImmutableIntVector2D add(IntVector2D first, int value) {
        return first.getAdded(value);
    }

    static ImmutableIntVector2D subtract(IntVector2D first, IntVector2D second) {
        return first.getSubtracted(second);
    }

    static ImmutableIntVector2D subtract(IntVector2D first, int x, int y) {
        return first.getSubtracted(x, y);
    }

    static ImmutableIntVector2D subtract(IntVector2D first, int value) {
        return first.getSubtracted(value);
    }

    static ImmutableIntVector2D multiply(IntVector2D first, IntVector2D factor) {
        return first.getMultiplied(factor);
    }

    static ImmutableIntVector2D multiply(IntVector2D first, int x, int y) {
        return first.getMultiplied(x, y);
    }

    static ImmutableIntVector2D multiply(IntVector2D first, int factor) {
        return first.getMultiplied(factor);
    }
}
