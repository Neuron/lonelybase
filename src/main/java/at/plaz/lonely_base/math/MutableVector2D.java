package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.MutableNumberTuple;

/**
 * Created by Georg Plaz
 */
public interface MutableVector2D extends MutableNumberTuple<Double>, Vector2D {

    static MutableVector2D create() {
        return new SimpleMutableVector2D(0, 0);
    }

    static MutableVector2D of(double x, double y) {
        return new SimpleMutableVector2D(x, y);
    }

    static MutableVector2D of(double[] values) {
        return new SimpleMutableVector2D(values);
    }

    static MutableVector2D of(Vector2D triplet) {
        return new SimpleMutableVector2D(triplet.getX(), triplet.getY());
    }

    void setX(double x);

    void setY(double y);

    @Override
    default void setFirst(Double x) {
        setX(x);
    }

    @Override
    default void setSecond(Double y) {
        setY(y);
    }

    default void set(Vector2D from) {
        setX(from.getX());
        setY(from.getY());
    }

    default void set(double x, double y) {
        setX(x);
        setY(y);
    }

    default void add(double value) {
        add(value, value);
    }

    default void add(double x, double y) {
        set(getX() + x, getY() + y);
    }

    default void add(Vector2D other) {
        add(other.getX(), other.getY());
    }

    default void subtract(double value) {
        subtract(value, value);
    }

    default void subtract(double x, double y) {
        set(getX() - x, getY() - y);
    }

    default void subtract(Vector2D other) {
        subtract(other.getX(), other.getY());
    }

    default void multiply(double scalar) {
        multiply(scalar, scalar);
    }

    default void multiply(double x, double y) {
        set(getX() * x, getY() * y);
    }

    default void multiply(Vector2D other) {
        multiply(other.getX(), other.getY());
    }

    default void divide(double dividend) {
        divide(dividend, dividend);
    }

    default void divide(double x, double y) {
        set(getX() / x, getY() / y);
    }

    default void divide(Vector2D other) {
        divide(other.getX(), other.getY());
    }
}
