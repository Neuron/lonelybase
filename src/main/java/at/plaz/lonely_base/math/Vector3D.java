package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public interface Vector3D extends NumberTriplet<Double> {

    ImmutableVector3D ZEROS = new SingleValueVector3D(0) {
        @Override
        public ImmutableVector3D getAddedImmutable(ImmutableVector3D delta) {
            return delta;
        }

        @Override
        public ImmutableVector3D getMultipliedImmutable(ImmutableVector3D factor) {
            return this;
        }
    };

    ImmutableVector3D ONES = new SingleValueVector3D(0) {
        @Override
        public ImmutableVector3D getMultipliedImmutable(ImmutableVector3D factor) {
            return factor;
        }
    };

    static ImmutableVector3D of(double x, double y, double z) {
        return new SimpleImmutableVector3D(x, y, z);
    }

    static ImmutableVector3D of(double[] values) {
        return new SimpleImmutableVector3D(values);
    }

    static ImmutableVector3D of(double value) {
        return new SingleValueVector3D(value);
    }

    double getX();

    double getY();

    double getZ();

    @Override
    default Double getFirst() {
        return getX();
    }

    @Override
    default Double getSecond() {
        return getY();
    }

    @Override
    default Double getThird() {
        return getZ();
    }

    default double getDoubleX() {
        return getX();
    }

    default double getDoubleY() {
        return getY();
    }

    default double getDoubleZ() {
        return getZ();
    }

    default double get(int dimension) {
        switch (dimension) {
            case 0:
                return getX();
            case 1:
                return getY();
            case 2:
                return getZ();
            default:
                throw new IndexOutOfBoundsException(dimension);
        }
    }

    default double getOverflowing(int dimension) {
        switch (dimension % 3) {
            case 0:
                return getX();
            case 1:
                return getY();
            case 2:
                return getZ();
            default:
                throw new RuntimeException();
        }
    }

    default ImmutableVector3D getAdded(Vector3D delta) {
        return delta.getAdded(getX(), getY(), getZ());
    }

    default ImmutableVector3D getAddedImmutable(ImmutableVector3D delta) {
        return new SimpleImmutableVector3D(getX() + delta.getX(), getY() + delta.getY(), getZ() + delta.getZ());
    }

    default ImmutableVector3D getAdded(double x, double y, double z) {
        return new SimpleImmutableVector3D(getX() + x, getY() + y, getZ() + z);
    }

    default ImmutableVector3D getAdded(double value) {
        return new SimpleImmutableVector3D(getX() + value, getY() + value, getZ() + value);
    }

    default ImmutableVector3D getSubtracted(Vector3D delta) {
        return new SimpleImmutableVector3D(getX() - delta.getX(), getY() -  delta.getY(), getZ() -  delta.getZ());
    }

    default ImmutableVector3D getSubtractedImmutable(ImmutableVector3D delta) {
        return new SimpleImmutableVector3D(getX() - delta.getX(), getY() - delta.getY(), getZ() - delta.getZ());
    }

    default ImmutableVector3D getSubtracted(double x, double y, double z) {
        return new SimpleImmutableVector3D(getX() - x, getY() - y, getZ() - z);
    }

    default ImmutableVector3D getSubtracted(double value) {
        return new SimpleImmutableVector3D(getX() - value, getY() - value, getZ() - value);
    }

    default ImmutableVector3D getMultiplied(Vector3D factor) {
        return factor.getMultiplied(getX(), getY(), getZ());
    }

    default ImmutableVector3D getMultipliedImmutable(ImmutableVector3D factor) {
        return new SimpleImmutableVector3D(getX() * factor.getX(), getY() * factor.getY(), getZ() * factor.getZ());
    }

    default ImmutableVector3D getMultiplied(double x, double y, double z) {
        return new SimpleImmutableVector3D(getX() * x, getY() * y, getZ() * z);
    }

    default ImmutableVector3D getMultiplied(double value) {
        return new SimpleImmutableVector3D(getX() * value, getY() * value, getZ() * value);
    }

    default ImmutableVector3D getDivided(Vector3D factor) {
        return factor.getDivided(getX(), getY(), getZ());
    }

    default ImmutableVector3D getDividedImmutable(ImmutableVector3D factor) {
        return new SimpleImmutableVector3D(getX() / factor.getX(), getY() / factor.getY(), getZ() / factor.getZ());
    }

    default ImmutableVector3D getDivided(double x, double y, double z) {
        return new SimpleImmutableVector3D(getX() / x, getY() / y, getZ() / z);
    }

    default ImmutableVector3D getDivided(double value) {
        return new SimpleImmutableVector3D(getX() / value, getY() / value, getZ() / value);
    }

    default double distance(Vector3D other) {
        return distance(this, other);
    }

    default boolean allEqual(double other) {
        return getX() == other && getY() == other && getZ() == other;
    }

    default boolean allGreater(Vector3D other) {
        return getX() > other.getX() && getY() > other.getY() && getZ() > other.getZ();
    }

    default boolean allGreater(double other) {
        return getX() > other && getY() > other && getZ() > other;
    }

    default boolean allGreaterEqual(Vector3D other) {
        return getX() >= other.getX() && getY() >= other.getY() && getZ() >= other.getZ();
    }

    default boolean allGreaterEqual(double other) {
        return getX() >= other && getY() >= other && getZ() >= other;
    }

    default ImmutableVector2D stripDimension(int dimension) {
        switch (dimension) {
            case 0:
                return new SimpleImmutableVector2D(get(1), get(2));
            case 1:
                return new SimpleImmutableVector2D(get(0), get(2));
            case 2:
                return new SimpleImmutableVector2D(get(0), get(1));
        }
        throw new RuntimeException();
    }

    default double length() {
        return distance(this);
    }

    ImmutableVector3D immutable();

    default MutableVector3D mutable() {
        return new SimpleMutableVector3D(getX(), getY(), getZ());
    }

    
    default IntVector3D getRounded() {
        return IntVector3D.of((int)Math.round(getX()), (int)Math.round(getY()), (int)Math.round(getZ()));
    }

    static ImmutableVector3D add(Vector3D first, Vector3D second) {
        return first.getAdded(second);
    }

    static ImmutableVector3D add(Vector3D first, double x, double y, double z) {
        return first.getAdded(x, y, z);
    }

    static ImmutableVector3D add(Vector3D first, double value) {
        return first.getAdded(value);
    }

    static ImmutableVector3D subtract(Vector3D first, Vector3D second) {
        return first.getSubtracted(second);
    }

    static ImmutableVector3D subtract(Vector3D first, double x, double y, double z) {
        return first.getSubtracted(x, y, z);
    }

    static ImmutableVector3D subtract(Vector3D first, double value) {
        return first.getSubtracted(value);
    }

    static ImmutableVector3D subtract(NumberTriplet first, NumberTriplet second) {
        return Vector3D.of(first.getDoubleX() - second.getDoubleX(), first.getDoubleY() - second.getDoubleY(), first.getDoubleZ() - second.getDoubleZ());
    }

    static ImmutableVector3D subtract(NumberTriplet first, double x, double y, double z) {
        return new SimpleImmutableVector3D(first.getDoubleX() - x, first.getDoubleY() - y, first.getDoubleZ() - z);
    }

    static ImmutableVector3D subtract(NumberTriplet first, double v) {
        return new SimpleImmutableVector3D(first.getDoubleX() - v, first.getDoubleY() - v, first.getDoubleZ() - v);
    }

    static ImmutableVector3D multiply(Vector3D first, double x, double y, double z) {
        return first.getMultiplied(x, y, z);
    }

    static ImmutableVector3D multiply(Vector3D first, double factor) {
        return first.getMultiplied(factor);
    }

    static ImmutableVector3D multiply(Vector3D first, Vector3D factor) {
        return first.getMultiplied(factor);
    }

    static Vector3D divide(Vector3D first, double factor) {
        return Vector3D.of(first.getX() / factor, first.getY() / factor, first.getZ() / factor);
    }

    static Vector3D divide(IntVector3D vector, double divisor) {
        return Vector3D.of(vector.getX() / divisor, vector.getY() / divisor, vector.getZ() / divisor);
    }

    static double distance(Vector3D from, Vector3D to) {
        return Math.sqrt(from.getX() * to.getX() + from.getY() * to.getY() + from.getZ() * to.getZ());
    }

    static Vector3D toLength(Vector3D vector, double length) {
        return multiply(vector, length / vector.length());
    }

    static Direction3D normalize(Vector3D vector) {
        if (vector instanceof Direction3D) {
            return (Direction3D) vector;
        }
        return new Direction3D(vector);
    }
}
