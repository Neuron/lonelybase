package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.ImmutableNumberTuple;

/**
 * Created by Georg Plaz
 */
public interface ImmutableIntVector2D extends ImmutableNumberTuple<Integer>, IntVector2D {
}
