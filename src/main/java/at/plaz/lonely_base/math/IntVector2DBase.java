package at.plaz.lonely_base.math;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public abstract class IntVector2DBase implements IntVector2D {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntVector2D)) return false;
        IntVector2D that = (IntVector2D) o;
        return getX() == that.getX() &&
                getY() == that.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "(" + getX() +
                ", " + getY() +
                ')';
    }
}
