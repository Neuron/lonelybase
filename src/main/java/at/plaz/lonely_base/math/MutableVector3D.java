package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public interface MutableVector3D extends Vector3D {

    static MutableVector3D create() {
        return new SimpleMutableVector3D(0, 0, 0);
    }

    static MutableVector3D of(double x, double y, double z) {
        return new SimpleMutableVector3D(x, y, z);
    }

    static MutableVector3D of(double[] values) {
        return new SimpleMutableVector3D(values);
    }

    static MutableVector3D of(Vector3D triplet) {
        return new SimpleMutableVector3D(triplet.getX(), triplet.getY(), triplet.getZ());
    }
    
    void setX(double x);

    void setY(double y);

    void setZ(double z);
    
    default void set(double x, double y, double z) {
        setX(x);
        setY(y);
        setZ(z);
    }

    default void addX(double delta) {
        setX(getX() + delta);
    }

    default void addY(double delta) {
        setY(getY() + delta);
    }

    default void addZ(double delta) {
        setZ(getZ() + delta);
    }

    default void add(Vector3D other) {
        set(getX() + other.getX(), getY() + other.getY(), getZ() + other.getZ());
    }

    default void add(double x, double y, double z) {
        set(getX() + x, getY() + y, getZ() + z);
    }
    
    default void add(double v) {
        set(getX() + v, getY() + v, getZ() + v);
    }

    default void subtract(Vector3D other) {
        set(getX() - other.getX(), getY() - other.getY(), getZ() - other.getZ());
    }

    default void subtract(double x, double y, double z) {
        set(getX() - x, getY() - y, getZ() - z);
    }
    
    default void subtract(double v) {
        set(getX() - v, getY() - v, getZ() - v);
    }

    default void multiply(Vector3D other) {
        set(getX() * other.getX(), getY() * other.getY(), getZ() * other.getZ());
    }

    default void multiply(double x, double y, double z) {
        set(getX() * x, getY() * y, getZ() * z);
    }
    
    default void multiply(double v) {
        set(getX() * v, getY() * v, getZ() * v);
    }

    default void divide(Vector3D other) {
        set(getX() / other.getX(), getY() / other.getY(), getZ() / other.getZ());
    }

    default void divide(double x, double y, double z) {
        set(getX() / x, getY() / y, getZ() / z);
    }
    
    default void divide(double v) {
        set(getX() / v, getY() / v, getZ() / v);
    }

}
