package at.plaz.lonely_base.math;

import at.plaz.lonely_base.triplets.Triplet;

/**
 * Created by Georg Plaz.
 */
public interface NumberTriplet<V extends Number> extends Triplet<V, V, V> {
    double getDoubleX();

    double getDoubleY();

    double getDoubleZ();

    ImmutableVector3D toVector3D();

    ImmutableIntVector3D truncateToInts();
}
