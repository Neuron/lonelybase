package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public interface MutableIntVector3D extends IntVector3D {

    static MutableIntVector3D of(int x, int y, int z) {
        return new SimpleMutableIntVector3D(x, y, z);
    }

    static MutableIntVector3D of(int[] values) {
        return new SimpleMutableIntVector3D(values);
    }

    default ImmutableIntVector3D truncateToInts() {
        return new SimpleImmutableIntVector3D(getX(), getY(), getZ());
    }

    default ImmutableVector3D toVector3D() {
        return new SimpleImmutableVector3D(getX(), getY(), getZ());
    }
    
    void setX(int x);

    void setY(int y);

    void setZ(int z);
    
    default void set(int x, int y, int z) {
        setX(x);
        setY(y);
        setZ(z);
    }

    default void add(IntVector3D first) {
        set(getX() + first.getX(), getY() + first.getY(), getZ() + first.getZ());
    }

    default void add(int x, int y, int z) {
        set(getX() + x, getY() + y, getZ() + z);
    }

    default void multiply(int x, int y, int z) {
        set(getX() * x, getY() * y, getZ() * z);
    }

    default void multiply(int factor) {
        set(getX() * factor, getY() * factor, getZ() * factor);
    }

    default void divide(int v) {
        set(getX() / v, getY() / v, getZ() / v);
    }

    @Override
    default ImmutableIntVector3D immutable() {
        return new SimpleImmutableIntVector3D(getX(), getY(), getZ());
    }

    @Override
    default MutableIntVector3D mutable() {
        return new SimpleMutableIntVector3D(getX(), getY(), getZ());
    }
}
