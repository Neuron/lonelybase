package at.plaz.lonely_base.math;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static at.plaz.lonely_base.math.Sign.MINUS;
import static at.plaz.lonely_base.math.Sign.PLUS;
import static at.plaz.lonely_base.math.Sign.ZERO;


/**
 * Created by Georg Plaz.
 */
public class VoxelDirection extends Direction3D {
    public static final VoxelDirection[][][] DIRECTIONS = new VoxelDirection[3][3][3];
    public static final VoxelDirection[] DIRECTIONS_FLATTENED = new VoxelDirection[3*3*3-1];

    static {
        for (Sign x : Sign.values()) {
            for (Sign y : Sign.values()) {
                for (Sign z : Sign.values()) {
                    if (DIRECTIONS[x.getIndex()][y.getIndex()][z.getIndex()] == null && !(Stream.of(x, y, z).allMatch(s -> Objects.equals(s, Sign.ZERO)))) {
                        new VoxelDirection(x, y, z);
                    }
                }
            }
        }
    }

    public static final VoxelDirection RIGHT = of(PLUS,  ZERO,  ZERO).orElseThrow();
    public static final VoxelDirection UP    = of(ZERO,  PLUS,  ZERO).orElseThrow();
    public static final VoxelDirection FRONT = of(ZERO,  ZERO,  PLUS).orElseThrow();
    public static final VoxelDirection LEFT  = of(MINUS, ZERO,  ZERO).orElseThrow();
    public static final VoxelDirection DOWN  = of(ZERO,  MINUS, ZERO).orElseThrow();
    public static final VoxelDirection BACK  = of(ZERO,  ZERO,  MINUS).orElseThrow();

    private static int DIRECTIONS_COUNT = 0;

    public static Optional<VoxelDirection> of(Sign x, Sign y, Sign z) {
        if (x == y && y == z && z == Sign.ZERO) {
            return Optional.empty();
        }
        return Optional.of(DIRECTIONS[x.getIndex()][y.getIndex()][z.getIndex()]);
    }

    public static VoxelDirection of(int x, int y, int z) {
        return DIRECTIONS[x + 1][y + 1][z + 1];
    }

    public static VoxelDirection inDirection(int dimension, boolean positive) {
        int isPositiveIndex = positive ? 2 : 0;
        switch (dimension % 3) {
            case 0:
                return DIRECTIONS[isPositiveIndex][1][1];
            case 1:
                return DIRECTIONS[1][isPositiveIndex][1];
            case 2:
                return DIRECTIONS[1][1][isPositiveIndex];
            default:
                throw new RuntimeException();
        }
    }

    private IntVector3D intDirection;
    private Sign xSign;
    private Sign ySign;
    private Sign zSign;

    private VoxelDirection(Sign x, Sign y, Sign z) {
        super(x.getValue(), y.getValue(), z.getValue());
        DIRECTIONS[x.getIndex()][y.getIndex()][z.getIndex()] = this;
        DIRECTIONS_FLATTENED[DIRECTIONS_COUNT++] = this;
        intDirection = IntVector3D.of(x.getValue(), y.getValue(), z.getValue());
        this.xSign = x;
        this.ySign = y;
        this.zSign = z;
    }

    public static VoxelDirection getClosest(Vector3D triplet) {
        double minDistance = Double.MAX_VALUE;
        VoxelDirection minDirection = null;
        for (VoxelDirection direction : DIRECTIONS_FLATTENED) {
            double distance = Vector3D.subtract(direction, triplet).length();
            if (minDistance > distance) {
                minDistance = distance;
                minDirection = direction;
            }
        }
        return minDirection;
    }

    public IntVector3D asIntDirection() {
        return intDirection;
    }

    public VoxelDirection flipped() {
        return DIRECTIONS[xSign.flipped().getIndex()][ySign.flipped().getIndex()][zSign.flipped().getIndex()];
    }

    public Sign getxSign() {
        return xSign;
    }

    public Sign getySign() {
        return ySign;
    }

    public Sign getzSign() {
        return zSign;
    }
}
