package at.plaz.lonely_base.math;


import static at.plaz.lonely_base.math.Sign.*;

/**
 * Created by Georg Plaz.
 */
public class Direction2D extends Vector2DBase implements ImmutableVector2D {

    public static final PixelDirection UP         = PixelDirection.of(ZERO, PLUS);
    public static final PixelDirection UP_RIGHT   = PixelDirection.of(PLUS, PLUS);
    public static final PixelDirection RIGHT      = PixelDirection.of(PLUS, ZERO);
    public static final PixelDirection RIGHT_DOWN = PixelDirection.of(PLUS, MINUS);

    public static final PixelDirection DOWN       = PixelDirection.of(ZERO, MINUS);
    public static final PixelDirection DOWN_LEFT  = PixelDirection.of(MINUS, MINUS);
    public static final PixelDirection LEFT       = PixelDirection.of(MINUS, ZERO);
    public static final PixelDirection LEFT_UP    = PixelDirection.of(MINUS, PLUS);

    private double x;
    private double y;

    public Direction2D(double x, double y) {
        this.x = x;
        this.y = y;
        double length = ImmutableVector2D.super.length();
        this.x /= length;
        this.y /= length;
    }

    public Direction2D(Vector2D triplet) {
        this(triplet.getX(), triplet.getY());
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double length() {
        return 1;
    }
}
