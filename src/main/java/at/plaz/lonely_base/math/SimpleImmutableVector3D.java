package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableVector3D extends Vector3DBase implements ImmutableVector3D {
    private double x;
    private double y;
    private double z;

    public SimpleImmutableVector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public SimpleImmutableVector3D(double[] values) {
        this.x = values[0];
        this.y = values[1];
        this.z = values[2];
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }
}
