package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableVector3D extends Vector3D {


    default ImmutableVector3D toVector3D() {
        return this;
    }

    default ImmutableIntVector3D truncateToInts() {
        return new SimpleImmutableIntVector3D((int)getX(), (int)getY(), (int)getZ());
    }

    @Override
    default ImmutableVector3D immutable() {
        return this;
    }

    @Override
    default MutableVector3D mutable() {
        return MutableVector3D.of(this);
    }

    @Override
    default ImmutableVector3D getAdded(Vector3D delta) {
        return delta.getAddedImmutable(this);
    }

    @Override
    default ImmutableVector3D getAdded(double x, double y, double z) {
        return (x == 0 && y == 0 && z == 0) ? this : new SimpleImmutableVector3D(getX() + x, getY() + y, getZ() + z);
    }

    @Override
    default ImmutableVector3D getAdded(double value) {
        return value == 0 ? this : new SimpleImmutableVector3D(getX() + value, getY() + value, getZ() + value);
    }

    @Override
    default ImmutableVector3D getMultiplied(Vector3D delta) {
        return delta.getMultipliedImmutable(this);
    }

    @Override
    default ImmutableVector3D getMultiplied(double x, double y, double z) {
        if (x == 1 && y == 1 && z == 1) {
            return this;
        } else {
            return (x == 0 && y == 0 && z == 0) ? ZEROS : new SimpleImmutableVector3D(getX() * x, getY() * y, getZ() * z);
        }
    }

    @Override
    default ImmutableVector3D getMultiplied(double value) {
        if (value == 1) {
            return this;
        } else {
            return (value == 0) ? ZEROS : new SimpleImmutableVector3D(getX() * value, getY() * value, getZ() * value);
        }
    }

}
