package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SingleValueVector2D extends Vector2DBase implements ImmutableVector2D {
    private final double value;

    public SingleValueVector2D(double value) {
        this.value = value;
    }

    @Override
    public double getX() {
        return value;
    }

    @Override
    public double getY() {
        return value;
    }

    @Override
    public ImmutableVector2D getAdded(double value) {
        double newValue = this.value + value;
        return newValue == 0 ? ZEROS : (newValue == 1 ? ONES : new SingleValueVector2D(newValue));
    }

    @Override
    public ImmutableVector2D getMultiplied(double factor) {
        return factor == 0 ? ZEROS : (factor == 1 ? this : new SingleValueVector2D(factor * value));
    }
}
