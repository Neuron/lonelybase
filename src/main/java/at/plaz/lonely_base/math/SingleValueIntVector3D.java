package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SingleValueIntVector3D extends IntVector3DBase implements ImmutableIntVector3D {
    private final int value;

    public SingleValueIntVector3D(int value) {
        this.value = value;
    }

    @Override
    public int getX() {
        return value;
    }

    @Override
    public int getY() {
        return value;
    }

    @Override
    public int getZ() {
        return value;
    }

    @Override
    public boolean allEqual(int other) {
        return value == other;
    }

    @Override
    public boolean allGreater(int other) {
        return value > other;
    }

    @Override
    public boolean allGreaterEqual(int other) {
        return value >= other;
    }

    @Override
    public ImmutableIntVector3D getAdded(int value) {
        int newValue = this.value + value;
        return newValue == 0 ? ZEROS : (newValue == 1 ? ONES : new SingleValueIntVector3D(newValue));
    }

    @Override
    public ImmutableIntVector3D getMultiplied(int factor) {
        return factor == 0 ? ZEROS : new SingleValueIntVector3D(factor * value);
    }
}
