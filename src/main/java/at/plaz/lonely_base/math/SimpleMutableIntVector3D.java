package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class SimpleMutableIntVector3D extends IntVector3DBase implements MutableIntVector3D {
    private int x;
    private int y;
    private int z;

    public SimpleMutableIntVector3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public ImmutableIntVector3D truncateToInts() {
        return new SimpleImmutableIntVector3D(x, y, z);
    }

    @Override
    public ImmutableVector3D toVector3D() {
        return new SimpleImmutableVector3D(x, y, z);
    }

    public SimpleMutableIntVector3D(int[] values) {
        this(values[0], values[1], values[2]);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getZ() {
        return z;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public ImmutableIntVector3D immutable() {
        return new SimpleImmutableIntVector3D(x, y, z);
    }

    @Override
    public MutableIntVector3D mutable() {
        return new SimpleMutableIntVector3D(x, y, z);
    }
}
