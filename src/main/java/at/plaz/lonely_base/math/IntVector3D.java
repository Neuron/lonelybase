package at.plaz.lonely_base.math;

import at.plaz.lonely_base.triplets.ImmutableCongruentTriplet;

/**
 * Created by Georg Plaz.
 */
public interface IntVector3D extends NumberTriplet<Integer>, ImmutableCongruentTriplet<Integer> {
    ImmutableIntVector3D ZEROS = new SingleValueIntVector3D(0) {
        @Override
        public ImmutableIntVector3D getAddedImmutable(ImmutableIntVector3D delta) {
            return delta;
        }

        @Override
        public ImmutableIntVector3D getMultipliedImmutable(ImmutableIntVector3D factor) {
            return this;
        }
    };
    
    ImmutableIntVector3D ONES = new SingleValueIntVector3D(0) {
        @Override
        public ImmutableIntVector3D getMultipliedImmutable(ImmutableIntVector3D factor) {
            return factor;
        }
    };

    static ImmutableIntVector3D of(int x, int y, int z) {
        return new SimpleImmutableIntVector3D(x, y, z);
    }

    static ImmutableIntVector3D of(int[] values) {
        return new SimpleImmutableIntVector3D(values);
    }

    static ImmutableIntVector3D of(int value) {
        return new SingleValueIntVector3D(value);
    }

    static int[] fromIndicesArray(
            int aIndex, int aValue,
            int bIndex, int bValue,
            int cIndex, int cValue) {
        if (aIndex == bIndex || bIndex == cIndex || cIndex == aIndex) {
            throw new RuntimeException("All indices must occur exactly once.");
        }
        int[] values = new int[3];
        values[aIndex] = aValue;
        values[bIndex] = bValue;
        values[cIndex] = cValue;
        return values;
    }

    static ImmutableIntVector3D fromIndices(
            int aIndex, int aValue,
            int bIndex, int bValue,
            int cIndex, int cValue) {
        return new SimpleImmutableIntVector3D(IntVector3D.fromIndicesArray(
                aIndex, aValue,
                bIndex, bValue,
                cIndex, cValue
        ));
    }

    int getX();

    int getY();

    int getZ();

    @Override
    default Integer getFirst() {
        return getX();
    }

    @Override
    default Integer getSecond() {
        return getY();
    }

    @Override
    default Integer getThird() {
        return getZ();
    }

    default boolean isMultiplicativeNeutral() {
        return allEqual(1);
    }
    
    default boolean isAdditiveNeutral() {
        return allEqual(0);
    }

    default ImmutableIntVector3D getAdded(IntVector3D delta) {
        return delta.getAdded(getX(), getY(), getZ());
    }

    default ImmutableIntVector3D getAddedImmutable(ImmutableIntVector3D delta) {
        return new SimpleImmutableIntVector3D(getX() + delta.getX(), getY() + delta.getY(), getZ() + delta.getZ());
    }

    default ImmutableIntVector3D getAdded(int x, int y, int z) {
        return new SimpleImmutableIntVector3D(getX() + x, getY() + y, getZ() + z);
    }

    default ImmutableIntVector3D getAdded(int value) {
        return new SimpleImmutableIntVector3D(getX() + value, getY() + value, getZ() + value);
    }

    default ImmutableIntVector3D getSubtracted(IntVector3D delta) {
        return getSubtracted(delta.getX(), delta.getY(), delta.getZ());
    }

    default ImmutableIntVector3D getSubtractedImmutable(ImmutableIntVector3D delta) {
        return new SimpleImmutableIntVector3D(getX() - delta.getX(), getY() - delta.getY(), getZ() - delta.getZ());
    }

    default ImmutableIntVector3D getSubtracted(int x, int y, int z) {
        return new SimpleImmutableIntVector3D(getX() - x, getY() - y, getZ() - z);
    }

    default ImmutableIntVector3D getSubtracted(int value) {
        return new SimpleImmutableIntVector3D(getX() - value, getY() - value, getZ() - value);
    }

    default ImmutableIntVector3D getMultiplied(IntVector3D factor) {
        return factor.getMultiplied(getX(), getY(), getZ());
    }

    default ImmutableIntVector3D getMultipliedImmutable(ImmutableIntVector3D factor) {
        return new SimpleImmutableIntVector3D(getX() * factor.getX(), getY() * factor.getY(), getZ() * factor.getZ());
    }

    default ImmutableIntVector3D getMultiplied(int x, int y, int z) {
        return new SimpleImmutableIntVector3D(getX() * x, getY() * y, getZ() * z);
    }

    default ImmutableIntVector3D getMultiplied(int value) {
        return new SimpleImmutableIntVector3D(getX() * value, getY() * value, getZ() * value);
    }

    default ImmutableVector3D getDivided(IntVector3D divisor) {
        return getDivided(divisor.getX(), divisor.getY(), divisor.getZ());
    }

    default ImmutableVector3D getDividedImmutable(ImmutableIntVector3D factor) {
        return new SimpleImmutableVector3D(getX() / (double)factor.getX(), getY() / (double)factor.getY(), getZ() / (double)factor.getZ());
    }

    default ImmutableVector3D getDivided(double x, double y, double z) {
        return new SimpleImmutableVector3D(getX() / x, getY() / y, getZ() / z);
    }

    default ImmutableVector3D getDivided(double value) {
        return new SimpleImmutableVector3D(getX() / value, getY() / value, getZ() / value);
    }

    default ImmutableIntVector3D getIntDivided(IntVector3D factor) {
        return factor.getIntDivided(getX(), getY(), getZ());
    }

    default ImmutableIntVector3D getIntDividedImmutable(ImmutableIntVector3D factor) {
        return new SimpleImmutableIntVector3D(getX() / factor.getX(), getY() / factor.getY(), getZ() / factor.getZ());
    }

    default ImmutableIntVector3D getIntDivided(int x, int y, int z) {
        return new SimpleImmutableIntVector3D(getX() / x, getY() / y, getZ() / z);
    }

    default ImmutableIntVector3D getIntDivided(int value) {
        return new SimpleImmutableIntVector3D(getX() / value, getY() / value, getZ() / value);
    }

    default double getDoubleX() {
        return getX();
    }

    default double getDoubleY() {
        return getY();
    }

    default double getDoubleZ() {
        return getZ();
    }

    default int get(int dimension) {
        switch (dimension) {
            case 0:
                return getX();
            case 1:
                return getY();
            case 2:
                return getZ();
            default:
                throw new IndexOutOfBoundsException(dimension);
        }
    }

    default int getOverflowing(int dimension) {
        switch (dimension % 3) {
            case 0:
                return getX();
            case 1:
                return getY();
            case 2:
                return getZ();
            default:
                throw new RuntimeException();
        }
    }

    default boolean allEqual(int other) {
        return getX() == other && getY() == other && getZ() == other;
    }

    default boolean allGreater(IntVector3D other) {
        return getX() > other.getX() && getY() > other.getY() && getZ() > other.getZ();
    }

    default boolean allGreater(int other) {
        return getX() > other && getY() > other && getZ() > other;
    }

    default boolean allGreaterEqual(IntVector3D other) {
        return getX() >= other.getX() && getY() >= other.getY() && getZ() >= other.getZ();
    }

    default boolean allGreaterEqual(int other) {
        return getX() >= other && getY() >= other && getZ() >= other;
    }

    default ImmutableIntVector2D stripDimension(int dimension) {
        switch (dimension) {
            case 0:
                return new SimpleImmutableIntVector2D(get(1), get(2));
            case 1:
                return new SimpleImmutableIntVector2D(get(0), get(2));
            case 2:
                return new SimpleImmutableIntVector2D(get(0), get(1));
        }
        throw new RuntimeException();
    }

    default Vector3D toDoubles() {
        return new SimpleImmutableVector3D(getX(), getY(), getZ());
    }

    default double length() {
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ());
    }

    ImmutableIntVector3D immutable();
    
    MutableIntVector3D mutable();


    // static

    static ImmutableIntVector3D insertInto(IntVector2D ints, int dimension, int value) {
        switch (dimension) {
            case 0:
                return new SimpleImmutableIntVector3D(value, ints.getX(), ints.getY());
            case 1:
                return new SimpleImmutableIntVector3D(ints.getX(), value, ints.getY());
            case 2:
                return new SimpleImmutableIntVector3D(ints.getX(), ints.getY(), value);
            default:
                throw new IndexOutOfBoundsException(dimension);
        }
    }

    static ImmutableIntVector3D add(IntVector3D first, IntVector3D second) {
        return first.getAdded(second);
    }

    static ImmutableIntVector3D add(IntVector3D first, int x, int y, int z) {
        return first.getAdded(x, y, z);
    }

    static ImmutableIntVector3D add(IntVector3D first, int value) {
        return first.getAdded(value);
    }

    static ImmutableIntVector3D subtract(IntVector3D first, IntVector3D second) {
        return first.getSubtracted(second);
    }

    static ImmutableIntVector3D subtract(IntVector3D first, int x, int y, int z) {
        return first.getSubtracted(x, y, z);
    }

    static ImmutableIntVector3D subtract(IntVector3D first, int value) {
        return first.getSubtracted(value);
    }

    static ImmutableIntVector3D multiply(IntVector3D first, IntVector3D factor) {
        return first.getMultiplied(factor);
    }

    static ImmutableIntVector3D multiply(IntVector3D first, int x, int y, int z) {
        return first.getMultiplied(x, y, z);
    }

    static ImmutableIntVector3D multiply(IntVector3D first, int factor) {
        return first.getMultiplied(factor);
    }
}
