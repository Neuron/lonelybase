package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.StringTuple;
import at.plaz.lonely_base.tuples.Tuple;
import at.plaz.lonely_base.tuples.Tuples;

import java.awt.*;
import java.util.Random;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableVector2D extends Vector2DBase implements ImmutableVector2D {
    private final double x;
    private final double y;

    public SimpleImmutableVector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public SimpleImmutableVector2D(Vector2D tuple) {
        this(tuple.getX(), tuple.getY());
    }

    public SimpleImmutableVector2D(Tuple<Double, Double> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleImmutableVector2D(double values) {
        this(values, values);
    }

    public SimpleImmutableVector2D(Double[] values) {
        this(values[0], values[1]);
    }

    public SimpleImmutableVector2D(double[] array) {
        this(array[0], array[1]);
    }

    public SimpleImmutableVector2D(String toParse) {
        this(Tuples.parseDoubleTuple(toParse));
    }

    public SimpleImmutableVector2D(StringTuple valuesToParse) {
        this(Double.parseDouble(valuesToParse.getFirst()), Double.parseDouble(valuesToParse.getSecond()));
    }

    public SimpleImmutableVector2D(Random random, double scalar) {
        this(random, 0, 0, scalar, scalar);
    }

    public SimpleImmutableVector2D(Random random) {
        this(random.nextDouble(), random.nextDouble());
    }

    public SimpleImmutableVector2D(Random random, double upperBoundX, double upperBoundY) {
        this(random, 0, 0, upperBoundX, upperBoundY);
    }

    public SimpleImmutableVector2D(Random random, Vector2D upperBound) {
        this(random, 0, 0, upperBound.getX(), upperBound.getY());
    }

    public SimpleImmutableVector2D(Random random, Vector2D lowerBounds, Vector2D upperBounds) {
        this(random, lowerBounds.getX(), lowerBounds.getY(), upperBounds.getX(), upperBounds.getY());
    }

    public SimpleImmutableVector2D(Random random, double lowerX, double lowerY, double upperX, double upperY) {
        this(lowerX+random.nextDouble()*(upperX-lowerX),
                lowerY+random.nextDouble()*(upperY-lowerY));
    }

    public SimpleImmutableVector2D(Point point) {
        this(point.getX(), point.getY());
    }

    public SimpleImmutableVector2D(Dimension dimension) {
        this(dimension.getWidth(), dimension.getHeight());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public ImmutableVector2D immutable() {
        return this;
    }

    @Override
    public String toString() {
        return "("+ x +", "+ y +")";
    }
}
