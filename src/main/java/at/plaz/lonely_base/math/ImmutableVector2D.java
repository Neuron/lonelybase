package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.ImmutableNumberTuple;

/**
 * Created by Georg Plaz
 */
public interface ImmutableVector2D extends ImmutableNumberTuple<Double>, Vector2D {

    @Override
    default ImmutableVector2D immutable() {
        return this;
    }
}
