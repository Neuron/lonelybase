package at.plaz.lonely_base.math;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public abstract class Vector2DBase implements Vector2D {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector2D)) return false;
        Vector2D that = (Vector2D) o;
        return getX() == that.getX() &&
                getY() == that.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "(" + getX() +
                ", " + getY() +
                ')';
    }
}
