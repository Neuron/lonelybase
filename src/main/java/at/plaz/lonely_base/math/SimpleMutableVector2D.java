package at.plaz.lonely_base.math;

import at.plaz.lonely_base.tuples.StringTuple;
import at.plaz.lonely_base.tuples.Tuple;
import at.plaz.lonely_base.tuples.Tuples;

import java.awt.*;
import java.util.Random;

/**
 * Created by Georg Plaz.
 */
public class SimpleMutableVector2D extends Vector2DBase implements MutableVector2D {
    private double x;
    private double y;

    public SimpleMutableVector2D() {
        this(0, 0);
    }

    public SimpleMutableVector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public SimpleMutableVector2D(Vector2D tuple) {
        this(tuple.getX(), tuple.getY());
    }

    public SimpleMutableVector2D(Tuple<Double, Double> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleMutableVector2D(double values) {
        this(values, values);
    }

    public SimpleMutableVector2D(Double[] values) {
        this(values[0], values[1]);
    }

    public SimpleMutableVector2D(double[] array) {
        this(array[0], array[1]);
    }

    public SimpleMutableVector2D(String toParse) {
        this(Tuples.parseDoubleTuple(toParse));
    }

    public SimpleMutableVector2D(StringTuple valuesToParse) {
        this(Double.parseDouble(valuesToParse.getFirst()), Double.parseDouble(valuesToParse.getSecond()));
    }

    public SimpleMutableVector2D(Random random, double scalar) {
        this(random, 0, 0, scalar, scalar);
    }

    public SimpleMutableVector2D(Random random) {
        this(random.nextDouble(), random.nextDouble());
    }

    public SimpleMutableVector2D(Random random, double upperBoundX, double upperBoundY) {
        this(random, 0, 0, upperBoundX, upperBoundY);
    }

    public SimpleMutableVector2D(Random random, Vector2D upperBound) {
        this(random, 0, 0, upperBound.getX(), upperBound.getY());
    }

    public SimpleMutableVector2D(Random random, Vector2D lowerBounds, Vector2D upperBounds) {
        this(random, lowerBounds.getX(), lowerBounds.getY(), upperBounds.getX(), upperBounds.getY());
    }

    public SimpleMutableVector2D(Random random, double lowerX, double lowerY, double upperX, double upperY) {
        this(lowerX+random.nextDouble()*(upperX-lowerX),
                lowerY+random.nextDouble()*(upperY-lowerY));
    }

    public SimpleMutableVector2D(Point point) {
        this(point.getX(), point.getY());
    }

    public SimpleMutableVector2D(Dimension dimension) {
        this(dimension.getWidth(), dimension.getHeight());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public ImmutableVector2D immutable() {
        return new SimpleImmutableVector2D(x, y);
    }

    public void setX(double first) {
        this.x = first;
    }

    public void setY(double second) {
        this.y = second;
    }

    @Override
    public String toString() {
        return "("+ x +", "+ y +")";
    }
}
