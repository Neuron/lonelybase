package at.plaz.lonely_base.math;

/**
 * Created by Georg Plaz.
 */
public class Vector2Ds {
    public static final ImmutableVector2D ZEROS = new SimpleImmutableVector2D(0);
    public static final ImmutableVector2D HALVES = new SimpleImmutableVector2D(0.5);
    public static final ImmutableVector2D ONES = new SimpleImmutableVector2D(1);
    public static final ImmutableVector2D NEGATIVE_ONES = new SimpleImmutableVector2D(-1);
    public static final ImmutableVector2D TWOS = new SimpleImmutableVector2D(2);
    public static final ImmutableVector2D MIN = new SimpleImmutableVector2D(-Double.MAX_VALUE);
    public static final ImmutableVector2D MAX = new SimpleImmutableVector2D(Double.MAX_VALUE);
    public static final double EPSILON_VALUE = 0.00000001;
    public static final ImmutableVector2D EPSILON = new SimpleImmutableVector2D(EPSILON_VALUE);

    public static boolean equals(Vector2D tuple, Object o) {
        if (tuple == o) return true;
        if (!(o instanceof Vector2D)) return false;

        Vector2D other = (Vector2D) o;
        return tuple.getX() == other.getX() && tuple.getY() == other.getY();
    }
    
    public static ImmutableVector2D add(double firstX, double firstY, double secondX, double secondY) {
        return new SimpleImmutableVector2D(firstX + secondX, firstY + secondY);
    }
    
    public static ImmutableVector2D add(Vector2D first, double secondX, double secondY) {
        return add(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableVector2D add(double firstX, double firstY, Vector2D second) {
        return add(firstX, firstY, second.getX(), second.getY());
    }
    
    public static ImmutableVector2D add(Vector2D first, Vector2D second) {
        return add(first.getX(), first.getY(), second.getX(), second.getY());
    }
    
    public static ImmutableVector2D add(Vector2D first, double value) {
        return add(first.getX(), first.getY(), value, value);
    }
    
    public static ImmutableVector2D subtract(double firstX, double firstY, double secondX, double secondY) {
        return new SimpleImmutableVector2D(firstX - secondX, firstY - secondY);
    }
    
    public static ImmutableVector2D subtract(Vector2D first, double secondX, double secondY) {
        return subtract(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableVector2D subtract(double firstX, double firstY, Vector2D second) {
        return subtract(firstX, firstY, second.getX(), second.getY());
    }
    
    public static ImmutableVector2D subtract(Vector2D first, Vector2D second) {
        return subtract(first.getX(), first.getY(), second.getX(), second.getY());
    }
    
    public static ImmutableVector2D subtract(Vector2D first, double value) {
        return subtract(first.getX(), first.getY(), value, value);
    }
    
    public static ImmutableVector2D multiply(double firstX, double firstY, double secondX, double secondY) {
        return new SimpleImmutableVector2D(firstX * secondX, firstY * secondY);
    }
    
    public static ImmutableVector2D multiply(Vector2D first, double secondX, double secondY) {
        return multiply(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableVector2D multiply(double firstX, double firstY, Vector2D second) {
        return multiply(firstX, firstY, second.getX(), second.getY());
    }
    
    public static ImmutableVector2D multiply(Vector2D first, Vector2D second) {
        return multiply(first.getX(), first.getY(), second.getX(), second.getY());
    }
    
    public static ImmutableVector2D multiply(Vector2D first, double value) {
        return multiply(first.getX(), first.getY(), value, value);
    }
    
    public static ImmutableVector2D divide(double firstX, double firstY, double secondX, double secondY) {
        return new SimpleImmutableVector2D(firstX / secondX, firstY / secondY);
    }
    
    public static ImmutableVector2D divide(Vector2D first, double secondX, double secondY) {
        return divide(first.getX(), first.getY(), secondX, secondY);
    }

    public static ImmutableVector2D divide(double firstX, double firstY, Vector2D second) {
        return divide(firstX, firstY, second.getX(), second.getY());
    }
    
    public static ImmutableVector2D divide(Vector2D first, Vector2D second) {
        return divide(first.getX(), first.getY(), second.getX(), second.getY());
    }
    
    public static ImmutableVector2D divide(Vector2D first, double value) {
        return divide(first.getX(), first.getY(), value, value);
    }

    public static ImmutableVector2D normalize(Vector2D tuple) {
        return divide(tuple, tuple.length());
    }

//    public static ImmutableDoubleTuple round(DoubleTuple tuple, int precision) {
//        double precisionPow = Math.round(Math.pow(10, precision));
//        return new SimpleImmutableDoubleTuple(Util.round(tuple.first(), precisionPow), Util.round(tuple.second(), precisionPow));
//    }

    public static ImmutableVector2D fromRotationAndDistance(double angle, double distance) {
        return new SimpleImmutableVector2D(Math.cos(Math.toRadians(angle)) * distance, Math.sin(Math.toRadians(angle)) * distance);
    }
}
