package at.plaz.lonely_base.math;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public abstract class IntVector3DBase implements IntVector3D {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntVector3D)) return false;
        IntVector3D that = (IntVector3D) o;
        return getX() == that.getX() &&
                getY() == that.getY() &&
                getZ() == that.getZ();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getZ());
    }

    @Override
    public String toString() {
        return "(" + getX() +
                ", " + getY() +
                ", " + getZ() +
                ')';
    }
}
