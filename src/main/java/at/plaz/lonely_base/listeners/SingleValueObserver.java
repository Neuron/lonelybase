package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface SingleValueObserver<A> {

    void addListener(SingleValueListener<A> listener);

    void removeListener(SingleValueListener<A> listener);

}
