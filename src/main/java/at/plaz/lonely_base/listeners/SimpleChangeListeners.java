package at.plaz.lonely_base.listeners;

import at.plaz.lonely_base.function.DoubleBiConsumer;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created by Georg Plaz.
 */
public class SimpleChangeListeners<A> implements ChangeListeners<A> {
    private final Map<Object, BiConsumer<? super A, ? super A>> listenerMapping = new LinkedHashMap<>();

    @Override
    public void addListener(BiConsumer<? super A, ? super A> listener) {
        synchronized (listenerMapping) {
            listenerMapping.putIfAbsent(listener, listener);
        }
    }

    @Override
    public void addListener(Consumer<? super A> listener) {
        synchronized (listenerMapping) {
            listenerMapping.computeIfAbsent(listener, l -> (o, n) -> listener.accept(n));
        }
    }

    @Override
    public void removeListener(BiConsumer<? super A, ? super A> listener) {
        synchronized (listenerMapping) {
            listenerMapping.remove(listener);
        }
    }

    @Override
    public void removeListener(Consumer<? super A> listener) {
        synchronized (listenerMapping) {
            listenerMapping.remove(listener);
        }
    }

    @Override
    public void announce(A oldValue, A newValue) {
        synchronized (listenerMapping) {
            listenerMapping.values().forEach(listener -> listener.accept(oldValue, newValue));
        }
    }
}
