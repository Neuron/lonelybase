package at.plaz.lonely_base.listeners.doubles;

import at.plaz.lonely_base.function.DoubleBiConsumer;
import at.plaz.lonely_base.listeners.ChangeObserver;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;

/**
 * Created by Georg Plaz.
 */
public interface DoubleChangeObserver extends ChangeObserver<Double> {

    void addListener(DoubleBiConsumer listener);

    void removeListener(DoubleBiConsumer listener);

    void addListener(DoubleConsumer listener);

    void removeListener(DoubleConsumer listener);

}
