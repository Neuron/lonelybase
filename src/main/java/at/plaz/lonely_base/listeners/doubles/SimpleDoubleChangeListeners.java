package at.plaz.lonely_base.listeners.doubles;

import at.plaz.lonely_base.function.DoubleBiConsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;

/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleChangeListeners implements DoubleChangeListeners {
    private final Map<Object, DoubleBiConsumer> biListenerMapping = new HashMap<>();

    @Override
    public void addListener(BiConsumer<? super Double, ? super Double> listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.computeIfAbsent(listener, l -> listener::accept);
        }
    }

    @Override
    public void addListener(DoubleBiConsumer listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.putIfAbsent(listener, listener);
        }
    }

    @Override
    public void addListener(Consumer<? super Double> listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.computeIfAbsent(listener, l -> (o, n) -> listener.accept(n));
        }
    }

    @Override
    public void addListener(DoubleConsumer listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.computeIfAbsent(listener, l -> (o, n) -> listener.accept(n));
        }
    }

    @Override
    public void removeListener(BiConsumer<? super Double, ? super Double> listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.remove(listener);
        }
    }

    @Override
    public void removeListener(DoubleBiConsumer listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.remove(listener);
        }
    }

    @Override
    public void removeListener(Consumer<? super Double> listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.remove(listener);
     }
    }

    @Override
    public void removeListener(DoubleConsumer listener) {
        synchronized (biListenerMapping) {
            biListenerMapping.remove(listener);
        }
    }

    @Override
    public void announce(double oldValue, double newValue) {
        synchronized (biListenerMapping) {
            biListenerMapping.values().forEach(listener -> listener.accept(oldValue, newValue));
        }
    }
}
