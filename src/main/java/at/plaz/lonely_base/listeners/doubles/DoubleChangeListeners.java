package at.plaz.lonely_base.listeners.doubles;

import at.plaz.lonely_base.listeners.ChangeListeners;

/**
 * Created by Georg Plaz.
 */
public interface DoubleChangeListeners extends ChangeListeners<Double>, DoubleChangeObserver {

    @Override
    default void announce(Double oldValue, Double newValue) {
        announce(oldValue.doubleValue(), newValue.doubleValue());
    }

    void announce(double oldValue, double newValue);

}
