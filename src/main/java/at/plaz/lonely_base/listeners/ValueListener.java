package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface ValueListener<A> {
    void announce(A object);
}
