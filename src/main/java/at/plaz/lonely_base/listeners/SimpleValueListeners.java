package at.plaz.lonely_base.listeners;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class SimpleValueListeners<A> implements SingleValueListeners<A> {
    private Set<SingleValueListener<A>> listeners = new LinkedHashSet<>();

    @Override
    public void addListener(SingleValueListener<A> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(SingleValueListener<A> listener) {
        listeners.remove(listener);
    }

    @Override
    public void announce(A value) {
        listeners.forEach(listener -> listener.announce(value));
    }
}
