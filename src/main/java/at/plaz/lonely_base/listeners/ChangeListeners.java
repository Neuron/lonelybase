package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface ChangeListeners<A> extends ChangeObserver<A>{
    void announce(A oldValue, A newValue);

}
