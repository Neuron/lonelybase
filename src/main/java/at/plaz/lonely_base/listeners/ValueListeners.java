package at.plaz.lonely_base.listeners;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class ValueListeners<A> {
    private Set<ValueListener<A>> listeners = new LinkedHashSet<>();

    public void addListener(ValueListener<A> listener) {
        listeners.add(listener);
    }

    public void removeListener(ValueListener<A> listener) {
        listeners.remove(listener);
    }

    public void announce(A object) {
        for (ValueListener<A> listener : listeners) {
            listener.announce(object);
        }
    }
}
