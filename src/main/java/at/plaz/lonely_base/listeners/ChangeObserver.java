package at.plaz.lonely_base.listeners;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created by Georg Plaz.
 */
public interface ChangeObserver<A> {

    void addListener(BiConsumer<? super A, ? super A> listener);

    void removeListener(BiConsumer<? super A, ? super A> listener);

    void addListener(Consumer<? super A> listener);

    void removeListener(Consumer<? super A> listener);
}
