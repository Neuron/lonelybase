package at.plaz.lonely_base.listeners.bool;

import at.plaz.lonely_base.function.BooleanConsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import java.util.function.Consumer;

/**
 * Created by Georg Plaz.
 */
public class SimpleBooleanChangeListeners implements BooleanChangeListeners {
    private Map<Object, BooleanConsumer> listenerMapping = new HashMap<>();

    @Override
    public void addListener(BiConsumer<? super Boolean, ? super Boolean> listener) {
        listenerMapping.computeIfAbsent(listener, l -> b -> listener.accept(!b, b));
    }

    @Override
    public void addListener(Consumer<? super Boolean> listener) {
        listenerMapping.computeIfAbsent(listener, l -> listener::accept);
    }

    @Override
    public void addListener(BooleanConsumer listener) {
        listenerMapping.putIfAbsent(listener, listener);
    }

    @Override
    public void removeListener(BiConsumer<? super Boolean, ? super Boolean> listener) {
        listenerMapping.remove(listener);
    }

    @Override
    public void removeListener(Consumer<? super Boolean> listener) {
        listenerMapping.remove(listener);
    }

    @Override
    public void removeListener(BooleanConsumer listener) {
        listenerMapping.remove(listener);
    }

    @Override
    public void announce(boolean newValue) {
        for (BooleanConsumer listener : listenerMapping.values()) {
            listener.accept(newValue);
        }
    }
}
