package at.plaz.lonely_base.listeners.bool;

import at.plaz.lonely_base.listeners.ChangeListeners;

/**
 * Created by Georg Plaz.
 */
public interface BooleanChangeListeners extends ChangeListeners<Boolean>, BooleanChangeObserver {

    @Override
    default void announce(Boolean oldValue, Boolean newValue) {
        announce(newValue);
    }

    void announce(boolean newValue);

}
