package at.plaz.lonely_base.listeners.bool;

import at.plaz.lonely_base.function.BooleanConsumer;
import at.plaz.lonely_base.listeners.*;

/**
 * Created by Georg Plaz.
 */
public interface BooleanChangeObserver extends ChangeObserver<Boolean> {

    void removeListener(BooleanConsumer listener);

    void addListener(BooleanConsumer listener);

}
