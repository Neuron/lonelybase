package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface Observer {
    void addListener(Runnable listener);

    void removeListener(Runnable listener);
}
