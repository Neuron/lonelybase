package at.plaz.lonely_base.listeners;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class Listeners implements Observer {
    private Set<Runnable> listeners = new LinkedHashSet<>();

    public void addListener(Runnable listener) {
        listeners.add(listener);
    }

    public void removeListener(Runnable listener) {
        listeners.remove(listener);
    }

    public void announce() {
        listeners.forEach(Runnable::run);
    }
}
