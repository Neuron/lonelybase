package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface SingleValueListeners<A> extends SingleValueObserver<A> {

    void announce(A value);

}
