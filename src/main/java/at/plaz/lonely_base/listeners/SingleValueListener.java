package at.plaz.lonely_base.listeners;

/**
 * Created by Georg Plaz.
 */
public interface SingleValueListener<A> {
    void announce(A value);
}
