package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz
 */
public interface MutableNumberTuple<A extends Number> extends NumberTuple<A>, MutableCongruentTuple<A> {

}
