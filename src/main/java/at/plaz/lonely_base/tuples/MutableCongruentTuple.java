package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz
 */
public interface MutableCongruentTuple<A> extends MutableTuple<A, A>, Iterable<A>, CongruentTuple<A> {
    default void swap() {
        A second = getSecond();
        setSecond(getFirst());
        setFirst(second);
    }

}
