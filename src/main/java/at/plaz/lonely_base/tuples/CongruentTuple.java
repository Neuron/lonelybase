package at.plaz.lonely_base.tuples;

import java.util.Iterator;

/**
 * Created by Georg Plaz
 */
public interface CongruentTuple<A> extends Tuple<A, A>, Iterable<A>{
    @SuppressWarnings("unchecked")
    default A[] toArray() {
        return (A[]) new Object[]{getFirst(), getSecond()};
    }

    @Override
    default Iterator<A> iterator() {
        return new CongruentTupleIterator<>(this);
    }

    @Override
    default CongruentTuple<A> flipped() {
        return Tuple.ofCongruent(getSecond(), getFirst());
    }

    default ImmutableCongruentTuple<A> getSwapped() {
        return new SimpleImmutableCongruentTuple<>(getSecond(), getFirst());
    }

    class CongruentTupleIterator<A> implements Iterator<A> {
        private CongruentTuple<A> tuple;
        short pos = 0;

        public CongruentTupleIterator(CongruentTuple<A> tuple) {
            this.tuple = tuple;
        }

        @Override
        public boolean hasNext() {
            return pos<=1;
        }

        @Override
        public A next() {
            switch (pos++) {
                case 0:
                    return tuple.getFirst();
                case 1:
                    return tuple.getSecond();
                default:
                    return null;
            }
        }
    }

    default A get(int index) {
        switch (index) {
            case 0:
                return getFirst();
            case 1:
                return getSecond();
            default:
                throw new IndexOutOfBoundsException(index);
        }
    }
}
