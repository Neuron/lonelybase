package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz
 */
public interface ImmutableCongruentTuple<A> extends ImmutableTuple<A, A>, CongruentTuple<A> {

}
