package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableTuple<A, B> implements ImmutableTuple<A, B> {
    private final A first;
    private final B second;

    public SimpleImmutableTuple(A first, B second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public A getFirst() {
        return first;
    }

    @Override
    public B getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return "("+first+", "+second+")";
    }
}
