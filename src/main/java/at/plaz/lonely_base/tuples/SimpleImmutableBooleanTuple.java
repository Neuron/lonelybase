package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableBooleanTuple implements ImmutableBooleanTuple {
    private final boolean first;
    private final boolean second;

    public SimpleImmutableBooleanTuple(boolean first, boolean second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean first() {
        return first;
    }

    @Override
    public boolean second() {
        return second;
    }

    @Override
    public String toString() {
        return "("+first+", "+second+")";
    }
}
