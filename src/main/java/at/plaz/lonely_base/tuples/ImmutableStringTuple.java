package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz
 */
public interface ImmutableStringTuple extends StringTuple, ImmutableCongruentTuple<String> {

}
