package at.plaz.lonely_base.tuples;

import at.plaz.lonely_base.math.*;

/**
 * Created by Georg Plaz
 */
public interface NumberTuple<A extends Number> extends CongruentTuple<A> {
    default Vector2D toDoubleVector() {
        return new SimpleImmutableVector2D(getFirst().doubleValue(), getSecond().doubleValue());
    }

    default IntVector2D toIntVector() {
        return new SimpleImmutableIntVector2D(getFirst().intValue(), getSecond().intValue());
    }

//    default ImmutableIntVector2D toIntTupleF() {
//        return new SimpleImmutableIntVector2D(getFirst().intValue(), getSecond().intValue());
//    }
//
//    default MutableIntVector2D toIntTupleD() {
//        return new SimpleIntVector2D(getFirst().intValue(), getSecond().intValue());
//    }

}
