package at.plaz.lonely_base.tuples;


/**
 * Created by Georg Plaz.
 */
public class BooleanTuples {
    public static final ImmutableBooleanTuple TRUE_TRUE = new SimpleImmutableBooleanTuple(true, true);
    public static final ImmutableBooleanTuple TRUE_FALSE = new SimpleImmutableBooleanTuple(true, false);
    public static final ImmutableBooleanTuple FALSE_TRUE = new SimpleImmutableBooleanTuple(false, true);
    public static final ImmutableBooleanTuple FALSE_FALSE = new SimpleImmutableBooleanTuple(false, false);

    public static ImmutableBooleanTuple get(boolean first, boolean second) {
        if (first) {
            return second ? BooleanTuple.TRUE_TRUE : BooleanTuple.TRUE_FALSE;
        } else {
            return second ? BooleanTuple.FALSE_TRUE : BooleanTuple.FALSE_FALSE;
        }
    }
}
