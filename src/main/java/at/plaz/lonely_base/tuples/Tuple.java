package at.plaz.lonely_base.tuples;


/**
 * Created by Georg Plaz
 */
public interface Tuple<A, B> {

    static <A, B> ImmutableTuple<A, B> of(A first, B second) {
        return new SimpleImmutableTuple<>(first, second);
    }

    static <V> ImmutableCongruentTuple<V> ofCongruent(V first, V second) {
        return new SimpleImmutableCongruentTuple<>(first, second);
    }

    static <V> ImmutableCongruentTuple<V> of(V value) {
        return new SimpleImmutableCongruentTuple<>(value, value);
    }
    
    A getFirst();

    B getSecond();

    default String[] toStringArray() {
        return new String[]{getFirst().toString(), getSecond().toString()};
    }

    default boolean anyIsNull() {
        return getFirst()==null || getSecond()==null;
    }

    default Tuple<B, A> flipped() {
        return new SimpleImmutableTuple<>(getSecond(), getFirst());
    }

    default boolean equals() {
        return getFirst().equals(getSecond());
    }
}
