package at.plaz.lonely_base.tuples;


/**
 * Created by Georg Plaz.
 */
public interface BooleanTuple extends CongruentTuple<Boolean> {
    ImmutableBooleanTuple TRUE_TRUE = new SimpleImmutableBooleanTuple(true, true);
    ImmutableBooleanTuple TRUE_FALSE = new SimpleImmutableBooleanTuple(true, false);
    ImmutableBooleanTuple FALSE_TRUE = new SimpleImmutableBooleanTuple(false, true);
    ImmutableBooleanTuple FALSE_FALSE = new SimpleImmutableBooleanTuple(false, false);

    boolean first();

    boolean second();

    @Override
    default Boolean getFirst() {
        return first();
    }

    @Override
    default Boolean getSecond() {
        return second();
    }

    default boolean and() {
        return first() && second();
    }

    default boolean or() {
        return first() || second();
    }


}
