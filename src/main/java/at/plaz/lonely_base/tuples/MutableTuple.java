package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz
 */
public interface MutableTuple<A, B> extends Tuple<A, B>{

    void setFirst(A first);

    void setSecond(B second);

    default boolean equals() {
        return getFirst().equals(getSecond());
    }
}
