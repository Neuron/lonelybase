package at.plaz.lonely_base.tuples;

import at.plaz.lonely_base.math.ImmutableIntVector2D;
import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.SimpleImmutableIntVector2D;
import at.plaz.lonely_base.math.SimpleImmutableVector2D;

import java.util.regex.Pattern;

/**
 * Created by Georg Plaz.
 */
public class Tuples {
    public static final String DELIMITER = ",";
    public static final String START = "(";
    public static final String END = ")";

    public static <A, B> boolean equals(Tuple<A, B> tuple, Object o) {
        if (tuple == o) return true;
        if (!(o instanceof Tuple)) return false;

        Tuple oTuple = (Tuple) o;

        if (tuple.getFirst() != null ? !tuple.getFirst().equals(oTuple.getFirst()) : oTuple.getFirst() != null) return false;
        if (tuple.getSecond() != null ? !tuple.getSecond().equals(oTuple.getSecond()) : oTuple.getSecond() != null) return false;

        return true;
    }

    public static <A, B> int hashCode(Tuple<A, B> tuple) {
        int result = tuple.getFirst() != null ? tuple.getFirst().hashCode() : 0;
        result = 31 * result + (tuple.getSecond() != null ? tuple.getSecond().hashCode() : 0);
        return result;
    }

    public static ImmutableVector2D parseDoubleTuple(String toParse) {
        try {
            return new SimpleImmutableVector2D(getValues(toParse));
        }catch (Exception exception) {
            throw new NumberFormatException("could not parse \""+toParse+"\" to a DoubleTuple!");
        }
    }

    public static ImmutableIntVector2D parseIntTuple(String toParse) {
        try {
            return new SimpleImmutableIntVector2D(getValues(toParse));
        }catch (Exception exception) {
            throw new NumberFormatException("could not parse \""+toParse+"\" to an IntTuple!");
        }
    }

    private static at.plaz.lonely_base.tuples.StringTuple getValues(String toParse) {
        at.plaz.lonely_base.tuples.StringTuple values = StringTuples.remove(new SimpleImmutableStringTuple(toParse.split(Pattern.quote(DELIMITER))), " ");
        if (values.getFirst().startsWith(START) && values.getSecond().endsWith(END)) {
            values = new SimpleImmutableStringTuple(values.getFirst().substring(1), values.getSecond().substring(0, values.getSecond().length() - 1));
        }
        return values;
    }
}
