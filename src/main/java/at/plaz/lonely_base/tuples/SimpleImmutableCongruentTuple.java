package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableCongruentTuple<A> implements ImmutableCongruentTuple<A> {
    private A first;
    private A second;

    public SimpleImmutableCongruentTuple(A first, A second) {
        this.first = first;
        this.second = second;
    }

    public SimpleImmutableCongruentTuple(at.plaz.lonely_base.tuples.Tuple<A, A> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleImmutableCongruentTuple(A values) {
        this(values, values);
    }

    public SimpleImmutableCongruentTuple(A[] values) {
        this(values[0], values[1]);
    }

    @Override
    public A getFirst() {
        return first;
    }

    @Override
    public A getSecond() {
        return second;
    }


    @Override
    public String toString() {
        return "("+first+", "+second+")";
    }
}
