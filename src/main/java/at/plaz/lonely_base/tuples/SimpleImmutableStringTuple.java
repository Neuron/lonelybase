package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableStringTuple implements ImmutableStringTuple {
    private final String first;
    private final String second;

    public SimpleImmutableStringTuple(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public SimpleImmutableStringTuple(String[] values) {
        this(values[0], values[1]);
    }

    public SimpleImmutableStringTuple(Tuple<String,String> tuple) {
        this(tuple.getFirst(), tuple.getSecond());
    }

    public SimpleImmutableStringTuple(String s) {
        this(s, s);
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    @Override
    public ImmutableStringTuple toFinal() {
        return this;
    }

    @Override
    public String toString() {
        return ("(\""+first+"\", \""+second+"\")");
    }
}
