package at.plaz.lonely_base.tuples;


/**
 * Created by Georg Plaz
 */
public interface ImmutableBooleanTuple extends BooleanTuple, ImmutableCongruentTuple<Boolean> {
}
