package at.plaz.lonely_base.tuples;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableTuple<A, B> extends Tuple<A, B> {

}
