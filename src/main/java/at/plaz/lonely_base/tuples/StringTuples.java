package at.plaz.lonely_base.tuples;

import at.plaz.lonely_base.math.ImmutableIntVector2D;

import java.util.regex.Pattern;

/**
 * Created by Georg Plaz.
 */
public class StringTuples {
    public static final ImmutableStringTuple EMPTY = new SimpleImmutableStringTuple("", "");

    public static at.plaz.lonely_base.tuples.StringTuple trim(at.plaz.lonely_base.tuples.StringTuple tuple) {
        String firstTrimmed = tuple.getFirst().trim();
        String secondTrimmed = tuple.getSecond().trim();
        if (firstTrimmed.equals(tuple.getFirst()) && secondTrimmed.equals(tuple.getSecond())) {
            return tuple;
        } else{
            return new SimpleImmutableStringTuple(firstTrimmed, secondTrimmed);
        }
    }

    public static at.plaz.lonely_base.tuples.StringTuple toLowerCase(at.plaz.lonely_base.tuples.StringTuple tuple) {
        String firstLower = tuple.getFirst().toLowerCase();
        String secondLower = tuple.getSecond().toLowerCase();
        if (tuple.getFirst().equals(firstLower) && tuple.getSecond().equals(secondLower)) {
            return tuple;
        }
        return new SimpleImmutableStringTuple(firstLower, secondLower);
    }

    public static at.plaz.lonely_base.tuples.StringTuple remove(at.plaz.lonely_base.tuples.StringTuple tuple, String toRemove) {
        if (tuple.anyContains(toRemove)) {
            return new SimpleImmutableStringTuple(remove(tuple.getFirst(), toRemove), remove(tuple.getSecond(), toRemove));
        } else{
            return tuple;
        }
    }

    private static String remove(String from, String toRemove) {
        return Pattern.compile(Pattern.quote(toRemove)).matcher(from).replaceAll("");
    }

    public static at.plaz.lonely_base.tuples.StringTuple remove(at.plaz.lonely_base.tuples.StringTuple tuple, String... toRemove) {
        String first = tuple.getFirst();
        String second = tuple.getSecond();
        boolean changed = false;
        for(String s:toRemove) {
            if (first.contains(s) || second.contains(s)) {
                first = remove(first, s);
                second = remove(second, s);
                changed = true;
            }
        }
        if (!changed) {
            return tuple;
        } else{
            return new SimpleImmutableStringTuple(first, second);
        }
    }

    public static at.plaz.lonely_base.tuples.StringTuple swap(at.plaz.lonely_base.tuples.StringTuple tuple) {
        return new SimpleImmutableStringTuple(tuple.getSecond(), tuple.getFirst());
    }

    public static at.plaz.lonely_base.tuples.StringTuple replace(at.plaz.lonely_base.tuples.StringTuple tuple, char oldChar, char newChar) {
        return new SimpleImmutableStringTuple(tuple.getFirst().replace(oldChar, newChar), tuple.getSecond().replace(oldChar, newChar));
    }

    public static at.plaz.lonely_base.tuples.StringTuple replace(at.plaz.lonely_base.tuples.StringTuple tuple, String target, String replacement) {
        return new SimpleImmutableStringTuple(tuple.getFirst().replace(target, replacement), tuple.getSecond().replace(target, replacement));
    }

    public static at.plaz.lonely_base.tuples.StringTuple substring(at.plaz.lonely_base.tuples.StringTuple tuple, ImmutableIntVector2D beginIndex, ImmutableIntVector2D endIndex) {
        return new SimpleImmutableStringTuple(tuple.getFirst().substring(beginIndex.getFirst(), endIndex.getFirst()),
                tuple.getSecond().substring(beginIndex.getSecond(), endIndex.getSecond()));
    }

    public static at.plaz.lonely_base.tuples.StringTuple split(at.plaz.lonely_base.tuples.StringTuple tuple, String regex, int index) {
        return new SimpleImmutableStringTuple(tuple.getFirst().split(regex)[index], tuple.getSecond().split(regex)[index]);
    }

    public static String toString(at.plaz.lonely_base.tuples.StringTuple tuple) {
        return "(\""+ tuple.getFirst()+"\", \""+tuple.getSecond()+"\")";
    }

    public static boolean containsWholeWord(String lookIn, String lookFor) {
        StringBuilder builder = new StringBuilder(" ").append(lookFor);
        return lookIn.endsWith(builder.toString()) ||
                lookIn.contains(builder.append(" ").toString()) ||
                lookIn.startsWith(builder.substring(1)) ||
                lookIn.equals(lookFor);
    }
}
