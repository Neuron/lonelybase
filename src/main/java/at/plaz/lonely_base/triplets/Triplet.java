package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface Triplet<A, B, C> {

    static <A, B, C> ImmutableTriplet<A, B, C> of(A first, B second, C third) {
        return new SimpleImmutableTriplet<>(first, second, third);
    }

    static <V> ImmutableCongruentTriplet<V> ofCongruent(V first, V second, V third) {
        return new SimpleImmutableCongruentTriplet<>(first, second, third);
    }

    static <V> ImmutableCongruentTriplet<V> of(V value) {
        return new SimpleImmutableCongruentTriplet<>(value, value, value);
    }

    A getFirst();

    B getSecond();

    C getThird();
}
