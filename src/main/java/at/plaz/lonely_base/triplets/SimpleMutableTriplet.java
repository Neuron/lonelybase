package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public class SimpleMutableTriplet<A, B, C> implements MutableTriplet<A, B, C> {
    private A first;
    private B second;
    private C third;

    public SimpleMutableTriplet(A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public A getFirst() {
        return first;
    }

    @Override
    public void setFirst(A first) {
        this.first = first;
    }

    @Override
    public B getSecond() {
        return second;
    }

    @Override
    public void setSecond(B second) {
        this.second = second;
    }

    @Override
    public C getThird() {
        return third;
    }

    @Override
    public void setThird(C third) {
        this.third = third;
    }
}
