package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableTriplet<A, B, C> extends Triplet<A, B, C> {

}
