package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableCongruentTriplet<V> extends ImmutableTriplet<V, V, V>, CongruentTriplet<V> {
}
