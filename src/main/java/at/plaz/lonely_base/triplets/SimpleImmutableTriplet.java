package at.plaz.lonely_base.triplets;

import at.plaz.lonely_base.tuples.Tuple;

/**
 * Created by Georg Plaz
 */
public class SimpleImmutableTriplet<A, B, C> implements ImmutableTriplet<A, B, C> {
    private final A first;
    private final B second;
    private final C third;

    public SimpleImmutableTriplet(Tuple<A, B> tuple, C third) {
        this(tuple.getFirst(), tuple.getSecond(), third);
    }

    public SimpleImmutableTriplet(A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public A getFirst() {
        return first;
    }

    @Override
    public B getSecond() {
        return second;
    }

    @Override
    public C getThird() {
        return third;
    }

    @Override
    public String toString() {
        return "(" + getFirst() + ", " + getSecond() + ", " + getThird() + ")";
    }
}