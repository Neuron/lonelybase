package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface MutableTriplet<A, B, C> extends Triplet<A, B, C> {

    static <A, B, C> MutableTriplet<A, B, C> of(A first, B second, C third) {
        return new SimpleMutableTriplet<>(first, second, third);
    }

    static <V> MutableTriplet<V, V, V> of(V value) {
        return new SimpleMutableTriplet<>(value, value, value);
    }
    
    void setFirst(A value);

    void setSecond(B value);

    void setThird(C value);

}
