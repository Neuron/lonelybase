package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface CongruentMutableTriplet<V> extends MutableTriplet<V, V, V>, CongruentTriplet<V> {
}
