package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public class SimpleImmutableCongruentTriplet<V> implements ImmutableCongruentTriplet<V> {
    private final V first;
    private final V second;
    private final V third;

    public SimpleImmutableCongruentTriplet(V first, V second, V third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public V getFirst() {
        return first;
    }

    @Override
    public V getSecond() {
        return second;
    }

    @Override
    public V getThird() {
        return third;
    }

    @Override
    public String toString() {
        return "(" + getFirst() + ", " + getSecond() + ", " + getThird() + ")";
    }
}
