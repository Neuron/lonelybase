package at.plaz.lonely_base.triplets;

/**
 * Created by Georg Plaz.
 */
public interface CongruentTriplet<V> extends Triplet<V, V, V> {

}
