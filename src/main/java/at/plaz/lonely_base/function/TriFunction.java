package at.plaz.lonely_base.function;

import at.plaz.lonely_base.triplets.Triplet;

/**
 * Created by Georg Plaz.
 */
public interface TriFunction<T, U, V, R> {
    R apply(T t, U u, V v);

    default R apply(Triplet<T, U, V> triplet) {
        return apply(triplet.getFirst(), triplet.getSecond(), triplet.getThird());
    }
}
