package at.plaz.lonely_base.function;

import java.util.function.Consumer;

/**
 * Created by Georg Plaz.
 */
public interface BooleanConsumer extends Consumer<Boolean> {
    default void accept(Boolean value) {
        this.accept(value.booleanValue());
    }

    void accept(boolean newValue);
}
