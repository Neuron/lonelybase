package at.plaz.lonely_base.function;

import java.util.function.BiConsumer;

/**
 * Created by Georg Plaz.
 */
public interface DoubleBiConsumer extends BiConsumer<Double, Double> {
    default void accept(Double oldValue, Double newValue) {
        this.accept(oldValue.doubleValue(), newValue.doubleValue());
    }

    void accept(double oldValue, double newValue);
}
