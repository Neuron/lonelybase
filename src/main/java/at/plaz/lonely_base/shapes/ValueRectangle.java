package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.math.SimpleImmutableVector2D;
import at.plaz.lonely_base.values.doubles.DoubleValue;

/**
 * Created by Georg Plaz.
 */
public interface ValueRectangle extends Shape, Rectangle {

    @Override
    default Vector2D getCenter() {
        return new SimpleImmutableVector2D(getLeft() + getWidth() / 2, getTop() + getHeight() / 2);
    }
    
    @Override
    default double getLeft() {
        return leftValue().getDouble();
    }

    @Override
    default double getTop() {
        return topValue().getDouble();
    }

    @Override
    default double getWidth() {
        return widthValue().getDouble();
    }

    @Override
    default double getHeight() {
        return heightValue().getDouble();
    }

    DoubleValue leftValue();

    DoubleValue topValue();

    DoubleValue rightValue();

    DoubleValue bottomValue();

    DoubleValue widthValue();

    DoubleValue heightValue();
}
