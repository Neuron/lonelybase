package at.plaz.lonely_base.shapes;


import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public class ImmutableFastRectangle implements ImmutableRectangle {
    private ImmutableVector2D position;
    private ImmutableVector2D dimensions;

    public ImmutableFastRectangle(Vector2D position, Vector2D dimensions) {
        this.position = position.immutable();
        this.dimensions = dimensions.immutable();
    }

    @Override
    public double getX() {
        return position.getX();
    }

    @Override
    public double getY() {
        return getPosition().getY();
    }

    @Override
    public double getWidth() {
        return dimensions.getX();
    }

    @Override
    public double getHeight() {
        return dimensions.getY();
    }

    @Override
    public ImmutableVector2D getPosition() {
        return position;
    }

    @Override
    public ImmutableVector2D getDimensions() {
        return dimensions;
    }
}
