package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableRectangle extends Rectangle {

}
