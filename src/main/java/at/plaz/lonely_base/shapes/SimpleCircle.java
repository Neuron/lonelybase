package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public class SimpleCircle implements MutableCircle {
    private at.plaz.lonely_base.values.doubles.DoubleVariable x;
    private at.plaz.lonely_base.values.doubles.DoubleVariable y;
    private at.plaz.lonely_base.values.doubles.DoubleVariable radius;

    public SimpleCircle(Vector2D center, double radius) {
        this.x = new at.plaz.lonely_base.values.doubles.SimpleDoubleVariable(center.getX());
        this.y = new at.plaz.lonely_base.values.doubles.SimpleDoubleVariable(center.getY());
        this.radius = new at.plaz.lonely_base.values.doubles.SimpleDoubleVariable(radius);
    }

    @Override
    public at.plaz.lonely_base.values.doubles.DoubleVariable xValue() {
        return x;
    }

    @Override
    public at.plaz.lonely_base.values.doubles.DoubleVariable yValue() {
        return y;
    }

    @Override
    public at.plaz.lonely_base.values.doubles.DoubleVariable radiusValue() {
        return radius;
    }

}
