package at.plaz.lonely_base.shapes;


import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by Georg Plaz.
 */
public class ImmutablePrimitiveRectangle implements ImmutableRectangle {

    private double x;
    private double y;
    private double width;
    private double height;

    public ImmutablePrimitiveRectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getHeight() {
        return height;
    }

}
