package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.math.SimpleImmutableVector2D;
import at.plaz.lonely_base.values.doubles.DoubleValue;

/**
 * Created by Georg Plaz.
 */
public interface Circle extends Shape {

    default double getRadius() {
        return radiusValue().getDouble();
    }

    default double getX() {
        return xValue().getDouble();
    }

    default double getY() {
        return yValue().getDouble();
    }

    default Vector2D getCenter() {
        return new SimpleImmutableVector2D(getX(), getY());
    }

    DoubleValue radiusValue();

    DoubleValue xValue();

    DoubleValue yValue();

    @Override
    default boolean collides(Shape other) {
        return other.collidesWithCircle(this);
    }

    @Override
    default boolean collidesWithCircle(Circle circle) {
        final double a = getRadius() + circle.getRadius();
        final double dx = getCenter().getX() - circle.getCenter().getX();
        final double dy = getCenter().getY() - circle.getCenter().getY();
        return a * a > (dx * dx + dy * dy);
    }
}
