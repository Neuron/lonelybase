package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.values.doubles.DoubleVariable;

/**
 * Created by Georg Plaz.
 */
public interface MutableCircle extends Circle {

    default void setCenter(Vector2D center) {
        setCenter(center.getX(), center.getY());
    }

    default void setCenter(double x, double y) {
        setX(x);
        setY(y);
    }

    default void setRadius(double radius) {
        radiusValue().setDouble(radius);
    }

    default void setX(double x) {
        xValue().setDouble(x);
    }

    default void setY(double y) {
        yValue().setDouble(y);
    }

    DoubleVariable radiusValue();

    DoubleVariable xValue();

    DoubleVariable yValue();


}
