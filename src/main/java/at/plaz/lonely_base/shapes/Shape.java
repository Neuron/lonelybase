package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public interface Shape {
    Vector2D getCenter();

    boolean collides(Shape other);

    default boolean collidesWithCircle(Circle circle) {
        throw new UnsupportedOperationException();
    }
}
