package at.plaz.lonely_base.shapes.collision;

import at.plaz.lonely_base.LonelyMath;
import at.plaz.lonely_base.shapes.Circle;
import at.plaz.lonely_base.math.SimpleImmutableIntVector2D;
import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.ImmutableIntVector2D;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class CollisionGrid<A extends Circle> implements Iterable<A> {
    private double gridSize;
    private final ImmutableVector2D dimensions;
    private final ImmutableIntVector2D gridCount;
    private Set<A>[][] circles;
    private Map<A, List<Set<A>>> circleToGridMap = new HashMap<>();

    public CollisionGrid(double gridSize, Vector2D dimensions) {
        this.dimensions = dimensions.immutable();
        this.gridSize = gridSize;
        this.gridCount = new SimpleImmutableIntVector2D((int) (dimensions.getX() / gridSize) + 1, (int) (dimensions.getY() / gridSize) + 1);
        circles = new Set[gridCount.getX()][gridCount.getY()];
        for (int x = 0; x < circles.length; x++) {
            for (int y = 0; y < circles[0].length; y++) {
                circles[x][y] = new HashSet<>();
            }
        }
    }

    public void add(A circle) {
        add(circle, circle.getRadius());
    }

    public void add(A circle, double maxRadius) {
        List<Set<A>> list = new LinkedList<>();
        circleToGridMap.put(circle, list);
        actOnGridCells(circle, maxRadius, (gridX, gridY) -> {
            circles[gridX][gridY].add(circle);
            list.add(circles[gridX][gridY]);
        });
    }

    public void remove(A circle) {
        circleToGridMap.remove(circle);
    }

    private int toGridX(double x) {
        return (int)(LonelyMath.snip(x / gridSize, 0, gridCount.getY()));
    }

    private int toGridY(double y) {
        return (int)(LonelyMath.snip(y / gridSize, 0, gridCount.getX()));
    }

    private Set<A> getCluster(Vector2D doubles) {
        return circles[getGridIndexX(doubles)][getGridIndexY(doubles)];
    }

    private int getGridIndexX(Vector2D position) {
        return (int) (position.getX() / gridSize);
    }

    private int getGridIndexY(Vector2D position) {
        return (int) (position.getY() / gridSize);
    }

    public ImmutableVector2D getDimensions() {
        return dimensions;
    }

    @Override
    public Iterator<A> iterator() {
        return circleToGridMap.keySet().iterator();
    }

    public Set<A> getColliding(Circle collisionObject) {
        SetActor actor = new SetActor();
        actOnGridCells(collisionObject, collisionObject.getRadius(), actor);
        return actor;
    }

    public Set<A> actOnGridCells(Circle collisionObject, double maxRadius, GridActor actor) {
        Set<A> result = new HashSet<>();
        double x = collisionObject.getCenter().getX();
        double left = toGridX(x - maxRadius);
        double right = toGridX(x + maxRadius);
        double y = collisionObject.getCenter().getY() - maxRadius;
        double down = toGridY(y);
        double up = toGridY(collisionObject.getCenter().getY() + maxRadius);
        for (int gridX = (int) (left / gridSize); gridX < right / gridSize; gridX++) {
            for (int gridY = (int) (down / gridSize); gridY < up / gridSize; gridY++) {
                actor.act(gridX, gridY);
            }
        }
        return result;
    }

    public interface GridActor {
        void act(int gridX, int gridY);
    }

    public class SetActor extends HashSet<A> implements GridActor {
        @Override
        public void act(int gridX, int gridY) {
            for (A gridObject : circles[gridX][gridY]) {
                add(gridObject);
            }
        }
    }

    public class CollisionActor extends HashSet<A> implements GridActor {
        private A collisionObject;

        public CollisionActor(A collisionObject) {
            this.collisionObject = collisionObject;
        }

        @Override
        public void act(int gridX, int gridY) {
            for (A gridObject : circles[gridX][gridY]) {
                if (gridObject.collides(collisionObject)) {
                    add(gridObject);
                }
            }
        }
    }
}
