package at.plaz.lonely_base.shapes.collision;

import at.plaz.lonely_base.shapes.SimpleCircle;
import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public class CollisionCircle extends SimpleCircle {
    public CollisionCircle(Vector2D center, double radius) {
        super(center, radius);
    }
}
