package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;

/**
 * Created by Georg Plaz.
 */
public interface Rectangle {
    Rectangle CONTAINING_ALL = ofFast(
            Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,
            Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

    static boolean intersects(Rectangle first, Rectangle second) {
        return intersects(first.getLeft(), first.getBottom(), first.getWidth(), first.getHeight(),
                second.getLeft(), second.getBottom(), second.getWidth(), second.getHeight());
    }

    static boolean intersects(double firstX, double firstY, double firstWidth, double firstHeight,
                              double secondX, double secondY, double secondWidth, double secondHeight) {
        return firstX + firstWidth > secondX &&
                firstY + firstHeight > secondY &&
                secondX + secondWidth > firstX &&
                secondY + secondHeight > firstY;
    }

    static boolean contains(double firstX, double firstY, double firstWidth, double firstHeight,
                              double secondX, double secondY, double secondWidth, double secondHeight) {
        return firstX + firstWidth >= secondX + secondWidth &&
                firstY + firstHeight >= secondY + secondHeight&&
                secondX >= firstX &&
                secondY >= firstY;
    }

    static boolean containsPoint(double x, double y, double width, double height,
                              double pointX, double pointY) {
        return pointX >= x &&
                pointY >= y &&
                x + width >= pointX &&
                y + height >= pointY;
    }

    static ImmutableFastRectangle ofFast(double x, double y, double width, double height) {
        return new ImmutableFastRectangle(Vector2D.of(x, y), Vector2D.of(width, height));
    }

    static ImmutableFastRectangle ofFast(Vector2D position, Vector2D dimensions) {
        return new ImmutableFastRectangle(position, dimensions);
    }

    static Rectangle containingAll() {
        return CONTAINING_ALL;
    }

    default boolean intersects(Rectangle other) {
        return intersects(getX(), getY(), getWidth(), getHeight(),
                other.getX(), other.getY(), other.getWidth(), other.getHeight());
    }

    default boolean contains(Rectangle other) {
        return contains(getX(), getY(), getWidth(), getHeight(),
                other.getX(), other.getY(), other.getWidth(), other.getHeight());
    }

    default boolean contains(Vector2D point) {
        return containsPoint(getX(), getY(), getWidth(), getHeight(),
                point.getX(), point.getY());
    }


    double getX();

    double getY();

    default double getLeft() {
        return getX();
    }

    default double getBottom() {
        return getY();
    }

    default double getTop() {
        return getBottom() + getHeight();
    }

    default double getRight() {
        return getLeft() + getWidth();
    }

    double getWidth();

    double getHeight();

    default ImmutableVector2D getPosition() {
        return Vector2D.of(getX(), getY());
    }

    default ImmutableVector2D getDimensions() {
        return Vector2D.of(getWidth(), getHeight());
    }
}
