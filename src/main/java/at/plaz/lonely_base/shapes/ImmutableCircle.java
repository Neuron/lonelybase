package at.plaz.lonely_base.shapes;

import at.plaz.lonely_base.math.ImmutableVector2D;

/**
 * Created by Georg Plaz.
 */
public interface ImmutableCircle extends Circle {
    double radius();

    @Override
    ImmutableVector2D getCenter();
}
