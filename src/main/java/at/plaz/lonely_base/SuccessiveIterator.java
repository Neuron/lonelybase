package at.plaz.lonely_base;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public class SuccessiveIterator<A> implements Iterator<A> {
    private A next;
    private Function<A, A> successorFunction;

    public SuccessiveIterator(A next, Function<A, A> successorFunction) {
        this.next = next;
        this.successorFunction = successorFunction;
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public A next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        A result = next;
        next = successorFunction.apply(next);
        return result;
    }
}
