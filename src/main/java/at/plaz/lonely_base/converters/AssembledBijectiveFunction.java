package at.plaz.lonely_base.converters;

import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
class AssembledBijectiveFunction<A, B> implements BijectiveFunction<A, B> {
    private final Function<A, B> straight;
    private final Function<B, A> reverse;

    public AssembledBijectiveFunction(Function<A, B> straight, Function<B, A> reverse) {
        this.straight = straight;
        this.reverse = reverse;
    }

    @Override
    public B apply(A object) {
        return straight.apply(object);
    }

    @Override
    public A applyInverse(B object) {
        return reverse.apply(object);
    }

    public Function<A, B> getStraight() {
        return straight;
    }

    public Function<B, A> getInverse() {
        return reverse;
    }
}
