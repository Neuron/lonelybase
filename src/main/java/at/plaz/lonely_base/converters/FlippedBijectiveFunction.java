package at.plaz.lonely_base.converters;

/**
 * Created by Georg Plaz.
 */
class FlippedBijectiveFunction<T, R> implements BijectiveFunction<T, R> {
    private BijectiveFunction<R, T> toFlip;

    public FlippedBijectiveFunction(BijectiveFunction<R, T> toFlip) {
        this.toFlip = toFlip;
    }

    @Override
    public R apply(T t) {
        return toFlip.applyInverse(t);
    }

    @Override
    public T applyInverse(R object) {
        return toFlip.apply(object);
    }

    public BijectiveFunction<R, T> getToFlip() {
        return toFlip;
    }
}
