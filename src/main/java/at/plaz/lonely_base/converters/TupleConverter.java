package at.plaz.lonely_base.converters;

import at.plaz.lonely_base.tuples.Tuple;

/**
 * Created by Georg Plaz.
 */
public interface TupleConverter<A, B, C, D> extends Converter<Tuple<A, B>, Tuple<C, D>> {

}
