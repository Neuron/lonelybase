package at.plaz.lonely_base.converters;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Georg Plaz.
 */
public interface Converter<A, B> {
    B convert(A object) throws ConversionException;

    static <A, B> Converter<A, B> of(Function<A, B> function, Predicate<A> validator) {
        return of(function, validator, a -> new ConversionException("Invalid input value."));
    }

    static <A, B> Converter<A, B> of(Function<A, B> function) {
        return function::apply;
    }

    static <A, B> Converter<A, B> of(Function<A, B> function, Predicate<A> validator,
                                     Function<A, ? extends ConversionException> exceptionFactory) {
        return a -> {
            if (validator.test(a)) {
                return function.apply(a);
            } else {
                throw exceptionFactory.apply(a);
            }
        };
    }

    static <A, B> TwoWayConverter<A, B> of(BijectiveFunction<A, B> bijectiveFunction) {
        return new AssembledTwoWayConverter<>(bijectiveFunction, bijectiveFunction.inverse());
    }

    static <A, B> TwoWayConverter<B, A> of(Converter<B, A> straight, Converter<A, B> reverse) {
        return new AssembledTwoWayConverter<>(straight, reverse);
    }

    static <B> TwoWayConverter<B, String> ofToString(Converter<String, B> reverse) {
        return new AssembledTwoWayConverter<>(Objects::toString, reverse);
    }

    static <B> TwoWayConverter<B, B> identity() {
        return IdentityTwoWayConverter.singleton();
    }



//    static <A, B> TwoWayConverter<A, B> of(Converter<A, B> straight, Function<B, A> reverse) {
//        return new AssembledTwoWayConverter<>(straight, reverse::apply);
//    }
//
//    static <A, B> TwoWayConverter<A, B> of(Function<A, B> straight, Converter<B, A> reverse) {
//        return new AssembledTwoWayConverter<>(straight::apply, reverse);
//    }
}
