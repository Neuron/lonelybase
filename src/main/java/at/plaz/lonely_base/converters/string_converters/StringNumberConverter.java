package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;
import at.plaz.lonely_base.converters.Converter;
import at.plaz.lonely_base.converters.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public class StringNumberConverter {
    private static final TwoWayConverter<String, Number> SINGLETON =
            Converter.of(StringNumberConverter::convert, StringNumberConverter::convertBack);
    private StringNumberConverter() {}

    public static TwoWayConverter<String, Number> singleton() {
        return SINGLETON;
    }

    public static Number convert(String input) throws ConversionException {
        try{
            return Double.valueOf(input.trim());
        }catch (NumberFormatException e) {
            throw new NotADoubleException(input.trim());
        }
    }

    public static String convertBack(Number input) {
        return input.toString();
    }
}
