package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.*;

import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public interface ToStringFunction<A> extends Function<A, String>, ToStringConverter<A> {
    @Override
    default String convert(A object) {
        return String.valueOf(object);
    }
}
