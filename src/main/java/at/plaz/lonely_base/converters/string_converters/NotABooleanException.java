package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;

/**
 * Created by Georg Plaz.
 */
public class NotABooleanException extends ConversionException {
    public NotABooleanException(String invalidInput) {
        super("Couldn't parse \""+invalidInput+"\" to a boolean");
    }
}
