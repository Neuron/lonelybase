package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;
import at.plaz.lonely_base.converters.Converter;
import at.plaz.lonely_base.converters.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public class StringIntegerConverter {
    private static final TwoWayConverter<String, Integer> SINGLETON =
            Converter.of(StringIntegerConverter::convert, StringIntegerConverter::convertBack);

    private StringIntegerConverter() {}

    public static TwoWayConverter<String, Integer> singleton() {
        return SINGLETON;
    }

    public static int convert(String input) throws ConversionException {
        try{
            return Integer.valueOf(input.trim());
        }catch (NumberFormatException e) {
            throw new NotAnIntegerException(input.trim());
        }
    }

    public static String convertBack(Integer input) {
        return input.toString();
    }
}
