package at.plaz.lonely_base.converters.string_converters;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class SimpleToStringFunction implements ToStringFunction<Object> {

    private static final SimpleToStringFunction SINGLETON = new SimpleToStringFunction();
    private SimpleToStringFunction() {}

    public static SimpleToStringFunction singleton() {
        return SINGLETON;
    }
    
    @Override
    public String apply(Object object) {
        return Objects.toString(object);
    }

}
