package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;
import at.plaz.lonely_base.converters.Converter;
import at.plaz.lonely_base.converters.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public class StringDoubleConverter {
    private static final TwoWayConverter<String, Double> SINGLETON =
            Converter.of(StringDoubleConverter::convert, StringDoubleConverter::convertBack);
    private StringDoubleConverter() {}

    public static TwoWayConverter<String, Double> singleton() {
        return SINGLETON;
    }

    public static double convert(String input) throws ConversionException {
        try{
            return Double.parseDouble(input.trim());
        }catch (NumberFormatException e) {
            throw new NotADoubleException(input.trim());
        }
    }

    public static String convertBack(Double input) {
        return input.toString();
    }
}
