package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.Converter;
import at.plaz.lonely_base.converters.TwoWayConverter;


/**
 * Created by Georg Plaz.
 */
public class StringBooleanConverter  {
    private static final TwoWayConverter<String, Boolean> SINGLETON =
            Converter.of(StringBooleanConverter::convert, StringBooleanConverter::convertBack);
    private StringBooleanConverter() {}

    public static TwoWayConverter<String, Boolean> singleton() {
        return SINGLETON;
    }

    public static Boolean convert(String input) throws NotABooleanException {
        switch (input.trim().toLowerCase()) {
            case "true":
                return true;
            case "false":
                return false;
            default:
                throw new NotABooleanException(input.trim());
        }
    }

    public static String convertBack(Boolean input) {
        return input.toString();
    }
}
