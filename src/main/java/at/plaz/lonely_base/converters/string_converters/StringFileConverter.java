package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.Converter;
import at.plaz.lonely_base.converters.TwoWayConverter;

import java.io.File;

/**
 * Created by Georg Plaz.
 */
public class StringFileConverter {
    private static final TwoWayConverter<String, File> SINGLETON =
            Converter.of(StringFileConverter::convert, StringFileConverter::convertBack);
    private StringFileConverter() {}

    public static TwoWayConverter<String, File> singleton() {
        return SINGLETON;
    }

    public static File convert(String input) {
        return new File(input.trim());
    }

    public static String convertBack(File input) {
        return input.getPath().replace('\\', '/');
    }
}
