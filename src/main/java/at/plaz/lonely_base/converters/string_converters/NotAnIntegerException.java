package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;

/**
 * Created by Georg Plaz.
 */
public class NotAnIntegerException extends ConversionException {
    public NotAnIntegerException(Object invalidInput) {
        super("Couldn't parse \""+invalidInput+"\" to an integer.");
    }
}
