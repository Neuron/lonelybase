package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.Converter;

/**
 * Created by Georg Plaz.
 */
public interface ToStringConverter<A> extends Converter<A, String> {
}
