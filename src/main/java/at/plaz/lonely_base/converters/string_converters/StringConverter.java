package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public interface StringConverter<A> extends TwoWayConverter<String, A>, FromStringConverter<A> {

}
