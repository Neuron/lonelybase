package at.plaz.lonely_base.converters.string_converters;

import at.plaz.lonely_base.converters.ConversionException;

/**
 * Created by Georg Plaz.
 */
public class NotADoubleException extends ConversionException {
    public NotADoubleException(Object invalidInput) {
        super("Couldn't parse \""+invalidInput+"\" to a double");
    }
}
