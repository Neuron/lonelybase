package at.plaz.lonely_base.converters;

import at.plaz.lonely_base.converters.number_converters.DoubleStringTwoWayConverter;

import java.io.Serializable;

/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleConverter implements DoubleStringTwoWayConverter, Serializable{
    @Override
    public double convertDouble(String object) {
        return Double.parseDouble(object);
    }

    @Override
    public String convertString(double object) {
        return String.valueOf(object);
    }
}
