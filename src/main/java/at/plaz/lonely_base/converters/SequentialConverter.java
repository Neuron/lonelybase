package at.plaz.lonely_base.converters;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.UnaryOperator;

/**
 * Created by Georg Plaz.
 */
public class SequentialConverter<A, Z> implements Converter<A, Z> {
    private final Converter<A, Object> first;
    private List<Converter<Object, Object>> intermediateConverters;
    private final Converter<Object, Z> last;

    @SuppressWarnings("unchecked")
    public <B> SequentialConverter(Converter<A, B> first, Converter<B, Z> second) {
        intermediateConverters = Collections.emptyList();
        this.first = (Converter<A, Object>) first;
        this.last = (Converter<Object, Z>) second;
    }

    @SuppressWarnings("unchecked")
    public <B, C> SequentialConverter(Converter<A, B> first, Converter<B, C> second, Converter<C, Z> third) {
        intermediateConverters = new LinkedList<>();
        this.first = (Converter<A, Object>) first;
        intermediateConverters.add((Converter<Object, Object>) second);
        this.last = (Converter<Object, Z>) third;
    }

    @Override
    public Z convert(A toConvert) throws ConversionException {
        Object intermediate = first.convert(toConvert);
        for(Converter<Object, Object> converter : intermediateConverters) {
            intermediate = converter.convert(intermediate);
        }
        return last.convert(intermediate);
    }
}
