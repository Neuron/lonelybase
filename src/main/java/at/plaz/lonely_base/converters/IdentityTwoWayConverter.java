package at.plaz.lonely_base.converters;


import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
class IdentityTwoWayConverter<A> extends AssembledTwoWayConverter<A, A> {
    private static final IdentityTwoWayConverter<?> SINGLETON = new IdentityTwoWayConverter<>();
    private IdentityTwoWayConverter() {
        super(Function.identity(), Function.identity());
    }

    @SuppressWarnings("unchecked")
    public static <A> IdentityTwoWayConverter<A> singleton() {
        return (IdentityTwoWayConverter<A>) SINGLETON;
    }


    @Override
    public A convert(A input) {
        return input;
    }

    @Override
    public A convertBack(A input) {
        return input;
    }

    @Override
    public TwoWayConverter<A, A> flip() {
        return this;
    }

    @Override
    public String toString() {
        return "identity";
    }
}
