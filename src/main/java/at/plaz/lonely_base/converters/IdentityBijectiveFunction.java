package at.plaz.lonely_base.converters;


/**
 * Created by Georg Plaz.
 */
class IdentityBijectiveFunction<A> implements BijectiveFunction<A, A> {
    private static final IdentityBijectiveFunction SINGLETON = new IdentityBijectiveFunction();
    private IdentityBijectiveFunction() {}

    @SuppressWarnings("unchecked")
    public static <A> IdentityBijectiveFunction<A> singleton() {
        return SINGLETON;
    }


    @Override
    public A apply(A input) {
        return input;
    }

    @Override
    public A applyInverse(A input) {
        return input;
    }

    @Override
    public BijectiveFunction<A, A> inverse() {
        return this;
    }
}
