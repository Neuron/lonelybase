package at.plaz.lonely_base.converters;

import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public interface BijectiveFunction<A, B> extends Function<A, B> {
    A applyInverse(B object);

    default BijectiveFunction<B, A> inverse() {
        if (this instanceof FlippedBijectiveFunction) {
            return ((FlippedBijectiveFunction<A, B>)this).getToFlip();
        } else if (this instanceof AssembledBijectiveFunction) {
            AssembledBijectiveFunction<A, B> cast = (AssembledBijectiveFunction<A, B>) this;
            return new AssembledBijectiveFunction<>(cast.getInverse(), cast.getStraight());
        }
        return new FlippedBijectiveFunction<>(this);
    }

    static <A, B> BijectiveFunction<A, B> of(Function<A, B> convertStraight, Function<B, A> convertBack) {
        return new AssembledBijectiveFunction<>(convertStraight, convertBack);
    }

    static <A> BijectiveFunction<A, A> identity() {
        return IdentityBijectiveFunction.singleton();
    }

}
