package at.plaz.lonely_base.converters;

/**
 * Created by Georg Plaz.
 */
public interface TwoWayConverter<A, B> extends Converter<A, B> {
    static <A, B> TwoWayConverter<A, B> of(Converter<A, B> straight, Converter<B, A> reverse) {
        return new AssembledTwoWayConverter<>(straight, reverse);
    }

    A convertBack(B object) throws ConversionException;

    default TwoWayConverter<B, A> flip() {
        return FlippedConverter.of(this);
    }

}
