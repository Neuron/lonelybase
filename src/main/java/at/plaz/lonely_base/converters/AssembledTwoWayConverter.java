package at.plaz.lonely_base.converters;

import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public class AssembledTwoWayConverter<A, B> implements TwoWayConverter<A, B> {
    private final Converter<A, B> straight;
    private final Converter<B, A> reverse;

    public AssembledTwoWayConverter(Converter<A, B> straight, Converter<B, A> reverse) {
        this.straight = straight;
        this.reverse = reverse;
    }

    public AssembledTwoWayConverter(Function<A, B> straight, Function<B, A> reverse) {
        this.straight = straight::apply;
        this.reverse = reverse::apply;
    }

    @Override
    public B convert(A object) throws ConversionException {
        return straight.convert(object);
    }

    @Override
    public A convertBack(B object) throws ConversionException {
        return reverse.convert(object);
    }
}
