package at.plaz.lonely_base.converters;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class SymmetricalSequentialConverter<A, Z> implements TwoWayConverter<A, Z> {
    private final TwoWayConverter<A, Object> first;
    private List<TwoWayConverter<Object, Object>> intermediateConverters;// = new LinkedList<>();
    private final TwoWayConverter<Object, Z> last;

    @SuppressWarnings("unchecked")
    public <B> SymmetricalSequentialConverter(TwoWayConverter<A, B> first, TwoWayConverter<B, Z> second) {
        intermediateConverters = Collections.emptyList();
        this.first = (TwoWayConverter<A, Object>) first;
        this.last = (TwoWayConverter<Object, Z>) second;
    }

    @SuppressWarnings("unchecked")
    public <B, C> SymmetricalSequentialConverter(TwoWayConverter<A, B> first, TwoWayConverter<B, C> second, TwoWayConverter<C, Z> third) {
        intermediateConverters = new LinkedList<>();
        this.first = (TwoWayConverter<A, Object>) first;
        intermediateConverters.add((TwoWayConverter<Object, Object>) second);
        this.last = (TwoWayConverter<Object, Z>) third;
    }

    @Override
    public Z convert(A toConvert) throws ConversionException {
        Object intermediate = first.convert(toConvert);
        if (!intermediateConverters.isEmpty()) {
            for(TwoWayConverter<Object, Object> converter : intermediateConverters) {
                intermediate = converter.convert(intermediate);
            }
        }
        return last.convert(intermediate);
    }

    @Override
    public A convertBack(Z toConvert) throws ConversionException {
        Object intermediate = last.convertBack(toConvert);
        if (!intermediateConverters.isEmpty()) {
            ListIterator<TwoWayConverter<Object, Object>> iterator = intermediateConverters.listIterator(intermediateConverters.size());
            while (iterator.hasPrevious()) {
                TwoWayConverter<Object, Object> converter = iterator.previous();
                intermediate = converter.convertBack(intermediate);
            }
        }
        return first.convertBack(intermediate);
    }
}
