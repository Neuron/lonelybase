package at.plaz.lonely_base.converters;

/**
 * Created by Georg Plaz.
 */
class FlippedConverter<A, B> implements TwoWayConverter<A, B> {
    private TwoWayConverter<B, A> converter;

    private FlippedConverter(TwoWayConverter<B, A> converter) {
        this.converter = converter;
    }

    static <A, B> TwoWayConverter<B, A> of(TwoWayConverter<A, B> toFlip) {
        return new FlippedConverter<>(toFlip);
    }

    @Override
    public A convertBack(B object) throws ConversionException {
        return converter.convert(object);
    }

    @Override
    public B convert(A object) throws ConversionException {
        return converter.convertBack(object);
    }

}
