package at.plaz.lonely_base.converters;

/**
 * Created by Georg Plaz.
 */
public class Converters {
    public TwoWayConverter<String, Integer> stringIntegerConverter = TwoWayConverter.of(Integer::parseInt, Object::toString);
}
