package at.plaz.lonely_base.converters;

/**
 * Created by Georg Plaz.
 */
public interface BiConverter<A, B, C> {
    C convert(A first, B second) throws ConversionException;
}
