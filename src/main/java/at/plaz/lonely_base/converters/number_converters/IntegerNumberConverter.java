package at.plaz.lonely_base.converters.number_converters;

/**
 * Created by Georg Plaz.
 */
public class IntegerNumberConverter extends NumberNumberConverter<Integer> {
    private static final IntegerNumberConverter SINGLETON = new IntegerNumberConverter();
    private IntegerNumberConverter() {}

    public static IntegerNumberConverter singleton() {
        return SINGLETON;
    }



    @Override
    public Integer convertBack(Number input) {
        return input.intValue();
    }
}
