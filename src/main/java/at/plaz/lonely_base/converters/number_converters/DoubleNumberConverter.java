package at.plaz.lonely_base.converters.number_converters;

/**
 * Created by Georg Plaz.
 */
public class DoubleNumberConverter extends NumberNumberConverter<Double> {
    private static final DoubleNumberConverter SINGLETON = new DoubleNumberConverter();
    private DoubleNumberConverter() {}

    public static DoubleNumberConverter singleton() {
        return SINGLETON;
    }

    @Override
    public Double convertBack(Number input) {
        return input.doubleValue();
    }
}
