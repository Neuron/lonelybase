package at.plaz.lonely_base.converters.number_converters;

import at.plaz.lonely_base.converters.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public interface DoubleStringTwoWayConverter extends TwoWayConverter<Double, String> {
    @Override
    default Double convertBack(String object) {
        return convertDouble(object);
    }

    @Override
    default String convert(Double object) {
        return convertString(object);
    }

    double convertDouble(String object);

    String convertString(double object);

}
