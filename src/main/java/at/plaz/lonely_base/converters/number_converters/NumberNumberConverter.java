package at.plaz.lonely_base.converters.number_converters;

import at.plaz.lonely_base.converters.TwoWayConverter;
import at.plaz.lonely_base.converters.ConversionException;

/**
 * Created by Georg Plaz.
 */
public abstract class NumberNumberConverter<A extends Number> implements TwoWayConverter<A, Number> {
    @Override
    public Number convert(A input) throws ConversionException {
        return input;
    }
}
