package at.plaz.lonely_base.converters.number_converters;

/**
 * Created by Georg Plaz.
 */
public class LongNumberConverter extends NumberNumberConverter<Long> {
    private static final LongNumberConverter SINGLETON = new LongNumberConverter();
    private LongNumberConverter() {}

    public static LongNumberConverter singleton() {
        return SINGLETON;
    }

    @Override
    public Long convertBack(Number input) {
        return input.longValue();
    }
}
