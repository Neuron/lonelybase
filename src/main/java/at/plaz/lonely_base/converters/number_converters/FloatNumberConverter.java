package at.plaz.lonely_base.converters.number_converters;

/**
 * Created by Georg Plaz.
 */
public class FloatNumberConverter extends NumberNumberConverter<Float> {
    private static final FloatNumberConverter SINGLETON = new FloatNumberConverter();
    private FloatNumberConverter() {}

    public static FloatNumberConverter singleton() {
        return SINGLETON;
    }



    @Override
    public Float convertBack(Number input) {
        return input.floatValue();
    }
}
