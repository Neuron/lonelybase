package at.plaz.lonely_base.values;

import java.util.function.BiConsumer;
import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class SimpleVariable<A> extends AbstractBoundVariable<A> implements Variable<A> {

    private ChangeListeners<A> listeners = new SimpleChangeListeners<>();
    private A value;

    public SimpleVariable() {
        this(null);
    }

    public SimpleVariable(A value) {
        this.value = value;
    }

    @Override
    void setValueWithoutChecking(A value) {
        if (!Objects.equals(value, this.value)) {
            A oldValue = this.value;
            this.value = value;
            listeners.announce(oldValue, value);
        }
    }

    @Override
    public ChangeObserver<A> getChangeObserver() {
        return listeners;
    }

    @Override
    public A get() {
        return value;
    }

    @Override
    public String toString() {
        return "SimpleVariable{" +
                "value=" + value +
                '}';
    }
}
