package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.*;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public abstract class DependentValue<A> implements Value<A> {
    private ChangeListeners<A> listeners = new SimpleChangeListeners<>();
    private Supplier<A> supplier;
    private A value;

    public DependentValue(Stream<ChangeObserver<?>> values, Supplier<A> supplier) {
        this.supplier = supplier;
        values.forEach(o -> o.addListener((oldValue, newValue) -> set(this.supplier.get())));
        value = supplier.get();
    }

    private void set(A newValue) {
        A oldValue = value;
        if (!Objects.equals(oldValue, newValue)) {
            value = newValue;
            listeners.announce(oldValue, newValue);
        }
    }

    @Override
    public A get() {
        return value;
    }

    @Override
    public ChangeObserver<A> getChangeObserver() {
        return listeners;
    }
}
