package at.plaz.lonely_base.values;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractVariable<A> implements Variable<A> {
    protected void checkSet() {
        if (isBound()) {
            throw new RuntimeException("Can't setDouble bound Variable.");
        }
    }

    protected void checkBind(Value<? extends A> other) {
        if (other == this) {
            throw new RuntimeException("Can't bind to itself.");
        }
    }

    protected void checkUnbind() {
    }
}
