package at.plaz.lonely_base.values;

/**
 * Created by Georg Plaz.
 */
public interface InceptionValueSetter<A, B> {
    void set(A from, B value);
}
