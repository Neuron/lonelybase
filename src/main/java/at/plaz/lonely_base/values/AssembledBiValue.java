package at.plaz.lonely_base.values;

import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class AssembledBiValue<A, B, C> extends DependentValue<C> {
    private Value<A> first;
    private Value<B> second;
    private BiFunction<A, B, C> function;


    public AssembledBiValue(Value<A> first, Value<B> second, BiFunction<A, B, C> function) {
        super(Stream.of(first, second).map(Value::getChangeObserver), () -> function.apply(first.get(), second.get()));
        this.first = first;
        this.second = second;
        this.function = function;
    }
}
