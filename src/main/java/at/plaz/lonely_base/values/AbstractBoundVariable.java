package at.plaz.lonely_base.values;

import java.util.function.BiConsumer;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractBoundVariable<A> extends AbstractVariable<A> {
    private Value<? extends A> boundTo;
    private BiConsumer<A, A> boundListener = (oldValue, newValue) -> setValueWithoutChecking(newValue);

    abstract void setValueWithoutChecking(A value);

    @Override
    public void set(A value) {
        checkSet();
        setValueWithoutChecking(value);
    }

    @Override
    public void bind(Value<? extends A> other) {
        checkBind(other);
        unbind();
        boundTo = other;
        boundTo.getChangeObserver().addListener(boundListener);
        setValueWithoutChecking(other.get());
    }

    @Override
    public void unbind() {
        checkUnbind();
        if (isBound()) {
            boundTo.getChangeObserver().removeListener(boundListener);
            boundTo = null;
        }
    }

    @Override
    public boolean isBound() {
        return boundTo != null;
    }
}
