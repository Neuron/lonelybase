package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.values.bool.*;
import at.plaz.lonely_base.values.string.StringValue;

import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by Georg Plaz.
 */
public interface Value<A> {

    static <A> Value<A> of(A constant) {
        return new SimpleConstant<>(constant);
    }

    A get();

    ChangeObserver<A> getChangeObserver();

    default BooleanValue isPresent() {
        return IsPresent.of(this);
    }

    default BooleanValue notPresent() {
        return NotPresent.of(this);
    }

    default Value<A> ifNotPresent(Value<A> elseValue) {
        return isPresent().ifElse(this, elseValue);
    }

    default Value<A> ifNotPresent(A constant) {
        return isPresent().ifElse(this, new SimpleConstant<A>(constant));
    }

    default BooleanValue isEqual(Value<?> other) {
        return new Equals(this, other);
    }

    default BooleanValue test(Predicate<A> predicate) {
        return new Test<>(this, predicate);
    }

    default Optional<A> optional() {
        return Optional.ofNullable(get());
    }

    default boolean valueIsPresent() {
        return get() != null;
    }

    default boolean valueNotPresent() {
        return get() == null;
    }

    default StringValue toStringValue() {
        return new ToString<>(this);
    }

    default StringValue toStringValue(String ifNotPresent) {
        return new ToString<>(this, ifNotPresent);
    }
}