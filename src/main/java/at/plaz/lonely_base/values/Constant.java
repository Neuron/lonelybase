package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.ChangeObserver;
import java.util.function.BiConsumer;

import java.util.Collections;
import java.util.function.Consumer;

/**
 * Created by Georg Plaz.
 */
public interface Constant<A> extends Value<A> {

    static <A> Constant<A> of(A value) {
        return new SimpleConstant<>(value);
    }

    @Override
    default ChangeObserver<A> getChangeObserver() {
        return ConstantObserver.singleton();
    }

    class ConstantObserver<A> implements ChangeObserver<A> {
        private static final ConstantObserver singleton = new ConstantObserver();

        private ConstantObserver() {
        }

        @Override
        public void removeListener(BiConsumer<? super A, ? super A> listener) { }

        @Override
        public void addListener(Consumer<? super A> listener) { }

        @Override
        public void removeListener(Consumer<? super A> listener) {}

        @Override
        public void addListener(BiConsumer<? super A, ? super A> listener) { }

        @SuppressWarnings("unchecked")
        public static <A> ConstantObserver<A> singleton() {
            return (ConstantObserver<A>) singleton;
        }
    }
}
