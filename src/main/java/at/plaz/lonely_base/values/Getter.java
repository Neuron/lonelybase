package at.plaz.lonely_base.values;

import java.util.function.Function;
import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class Getter<A, B> implements Value<B> {
    private ChangeListeners<B> listeners = new SimpleChangeListeners<>();
    private Value<A> getFrom;
    private Function<A, B> valueGetter;
    private B currentValue;

    public Getter(Value<A> from, Function<A, B> valueGetter) {
        this.getFrom = from;
        this.valueGetter = valueGetter;
        forceUpdate();
        this.getFrom.getChangeObserver().addListener((this::updateThis));
    }

    public void forceUpdate() {
        updateThis(null, getFrom.get());
    }

    private void updateThis(A oldA, A newA) {
        B oldValue = currentValue;
        if (getFrom.valueIsPresent()) {
            currentValue = valueGetter.apply(newA);
        } else {
            currentValue = null;
        }
        if (!Objects.equals(oldValue, currentValue)) {
            listeners.announce(oldValue, currentValue);
        }
    }

    @Override
    public B get() {
        return currentValue;
    }

    @Override
    public ChangeObserver<B> getChangeObserver() {
        return listeners;
    }

    @Override
    public String toString() {
        return "DeepGetter{" +
                "currentValue=" + currentValue +
                ", getFrom=" + getFrom +
                '}';
    }
}
