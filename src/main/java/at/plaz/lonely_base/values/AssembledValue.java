package at.plaz.lonely_base.values;

import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class AssembledValue<A, B> extends DependentValue<A> {
    public AssembledValue(Collection<Value<B>> values, Collector<? super B, ?, ? extends A> collector) {
        this(values, s -> s.collect(collector));
    }

    public AssembledValue(Collection<Value<B>> values, StreamToOne<B, A> streamToOne) {
        super(values.stream().map(Value::getChangeObserver),
                () -> streamToOne.toOne(values.stream().map(Value::get)));
    }

    public interface StreamToOne<A, B> {
        B toOne(Stream<A> stream);
    }
}
