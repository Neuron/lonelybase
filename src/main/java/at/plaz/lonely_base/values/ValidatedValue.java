package at.plaz.lonely_base.values;

import at.plaz.lonely_base.Validator;
import at.plaz.lonely_base.listeners.ChangeObserver;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by Georg Plaz.
 */
public class ValidatedValue<A> implements Value<A> {
    private Variable<A> container = new SimpleVariable<>();
    private Collection<Validator<A>> validators = new LinkedList<>();

    public ValidatedValue(A value) {
        container.set(value);
    }

    public ValidatedValue() {

    }

    public void set(A value) throws NotValidException {
        for (Validator<A> validator : validators) {
            if (!validator.isValid(value)) {
                throw new NotValidException();
            }
        }
        container.set(value);
    }

    public void unsafeSet(A value) {
        try {
            set(value);
        } catch (NotValidException e) {
            throw new RuntimeException(e);
        }
    }

    public void attemptSet(A value) {
        try {
            set(value);
        } catch (NotValidException e) {
            // do nothing
        }
    }

    @Override
    public A get() {
        return container.get();
    }

    @Override
    public ChangeObserver<A> getChangeObserver() {
        return container.getChangeObserver();
    }

    public Collection<Validator<A>> getValidators() {
        return validators;
    }

    public static class NotValidException extends Exception {

    }

    @Override
    public String toString() {
        return container.toString();
    }
}
