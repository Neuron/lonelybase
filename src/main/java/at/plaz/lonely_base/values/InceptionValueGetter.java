package at.plaz.lonely_base.values;

/**
 * Created by Georg Plaz.
 */
public interface InceptionValueGetter<A, B> {
    B get(A from);
}
