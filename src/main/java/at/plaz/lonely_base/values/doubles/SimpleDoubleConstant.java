package at.plaz.lonely_base.values.doubles;

/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleConstant implements DoubleConstant {
    private final double value;

    public SimpleDoubleConstant(double value) {
        this.value = value;
    }

    @Override
    public double getDouble() {
        return value;
    }
}
