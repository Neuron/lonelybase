package at.plaz.lonely_base.values.doubles;

import at.plaz.lonely_base.values.*;

/**
 * Created by Georg Plaz.
 */
public interface DoubleVariable extends DoubleValue, Variable<Double> {

    void setDouble(double value);

    @Override
    default void set(Double value) {
        setDouble(value);
    }

    void bind(DoubleValue other);
}
