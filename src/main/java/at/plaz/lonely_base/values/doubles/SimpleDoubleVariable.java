package at.plaz.lonely_base.values.doubles;

import at.plaz.lonely_base.listeners.doubles.DoubleChangeListeners;
import at.plaz.lonely_base.listeners.doubles.DoubleChangeObserver;
import at.plaz.lonely_base.function.DoubleBiConsumer;
import at.plaz.lonely_base.listeners.doubles.SimpleDoubleChangeListeners;
import at.plaz.lonely_base.values.*;

/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleVariable extends PrimitiveVariable<Double> implements DoubleVariable {

    private DoubleChangeListeners listeners = new SimpleDoubleChangeListeners();
    private double value;
    private Value<? extends Double> boundTo;
    private DoubleBiConsumer doubleBiConsumer = (oldValue, newValue) -> safeSet(newValue);

    public SimpleDoubleVariable() {
        this(0);
    }

    public SimpleDoubleVariable(double value) {
        this.value = value;
    }

    @Override
    public double getDouble() {
        return value;
    }

    @Override
    public DoubleChangeObserver getDoubleChangeObserver() {
        return listeners;
    }

    @Override
    public void setDouble(double value) {
        checkSet();
        safeSet(value);
    }

    private void safeSet(double value) {
        double oldValue = this.value;
        this.value = value;
        listeners.announce(oldValue, value);
    }

    @Override
    public void bind(Value<? extends Double> other) {
        checkBind(other);
        unbind();
        boundTo = other;
        if (other instanceof DoubleValue) {
            bind((DoubleValue)other);
        } else {
            other.getChangeObserver().addListener(doubleBiConsumer);
            safeSet(other.get());
        }
    }

    @Override
    public void bind(DoubleValue other) {
        checkBind(other);
        unbind();
        other.getDoubleChangeObserver().addListener(((oldValue, newValue) -> safeSet(newValue)));
        safeSet(other.getDouble());
    }

    @Override
    public void unbind() {
        checkUnbind();
        if (isBound()) {
            if (boundTo instanceof DoubleValue) {
                ((DoubleValue) boundTo).getDoubleChangeObserver().removeListener(doubleBiConsumer);
            } else {
                boundTo.getChangeObserver().removeListener(doubleBiConsumer);
            }
            boundTo = null;
        }
    }

    @Override
    public boolean isBound() {
        return boundTo != null;
    }
}
