package at.plaz.lonely_base.values.doubles;

import at.plaz.lonely_base.function.DoubleBiConsumer;
import at.plaz.lonely_base.listeners.ChangeObserver;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;

import at.plaz.lonely_base.listeners.doubles.DoubleChangeObserver;
import at.plaz.lonely_base.values.Constant;

/**
 * Created by Georg Plaz.
 */
public interface DoubleConstant extends Constant<Double>, DoubleValue {
    @Override
    default DoubleChangeObserver getChangeObserver() {
        return getDoubleChangeObserver();
    }

    @Override
    default DoubleChangeObserver getDoubleChangeObserver() {
        return ConstantDoubleObserver.singleton();
    }

    class ConstantDoubleObserver implements DoubleChangeObserver, ChangeObserver<Double> {
        private static final ConstantDoubleObserver singleton = new ConstantDoubleObserver();

        @SuppressWarnings("unchecked")
        public static ConstantDoubleObserver singleton() {
            return singleton;
        }

        private ConstantDoubleObserver() {
        }

        @Override
        public void removeListener(BiConsumer<? super Double, ? super Double> listener) { }

        @Override
        public void addListener(Consumer<? super Double> listener)  { }

        @Override
        public void removeListener(Consumer<? super Double> listener)  { }

        @Override
        public void addListener(BiConsumer<? super Double, ? super Double> listener) { }

        @Override
        public void addListener(DoubleBiConsumer listener) { }

        @Override
        public void removeListener(DoubleBiConsumer listener)  { }

        @Override
        public void addListener(DoubleConsumer listener)  { }

        @Override
        public void removeListener(DoubleConsumer listener)  { }
    }
}
