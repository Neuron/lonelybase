package at.plaz.lonely_base.values.doubles;

import at.plaz.lonely_base.listeners.doubles.DoubleChangeObserver;
import at.plaz.lonely_base.values.ToString;
import at.plaz.lonely_base.values.Value;
import at.plaz.lonely_base.values.bool.BooleanValue;
import at.plaz.lonely_base.values.string.StringValue;

import java.text.DecimalFormat;

/**
 * Created by Georg Plaz.
 */
public interface DoubleValue extends Value<Double> {

    double getDouble();

    @Override
    default Double get() {
        return getDouble();
    }

    @Override
    default BooleanValue isPresent() {
        return BooleanValue.ofTrue();
    }

    default StringValue toStringValue(DecimalFormat decimalFormat) {
        return new ToString<>(this, decimalFormat::format);
    }

    @Override
    default DoubleChangeObserver getChangeObserver() {
        return getDoubleChangeObserver();
    }

    DoubleChangeObserver getDoubleChangeObserver();
}
