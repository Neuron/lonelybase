package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;
import at.plaz.lonely_base.values.bool.BooleanValue;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class Conditional<A> implements Value<A> {
    private ChangeListeners<A> listeners = new SimpleChangeListeners<>();
    private Value<A> ifValue;
    private Value<A> elseValue;
    private BooleanValue booleanValue;
    private A currentValue;

    public static <A> Value<A> of(BooleanValue booleanValue, Value<A> ifValue, Value<A> elseValue) {
        return new Conditional<>(booleanValue, ifValue, elseValue);
    }

    private Conditional(BooleanValue booleanValue, Value<A> ifValue, Value<A> elseValue) {
        this.booleanValue = booleanValue;
        this.ifValue = ifValue;
        this.elseValue = elseValue;
        update();
        booleanValue.getBooleanChangeObserver().addListener(condition -> update());
        ifValue.getChangeObserver().addListener((oldValue, newValue) -> update());
        elseValue.getChangeObserver().addListener((oldValue, newValue) -> update());
    }

    @Override
    public A get() {
        return booleanValue.getBoolean() ? ifValue.get() : elseValue.get();
    }

    private void update() {
        A oldValue = currentValue;
        currentValue = get();
        if (!Objects.equals(oldValue, currentValue)) {
            listeners.announce(oldValue, currentValue);
        }
    }

    @Override
    public ChangeObserver<A> getChangeObserver() {
        return listeners;
    }
}
