package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.*;
import at.plaz.lonely_base.values.Constant;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
class And implements BooleanValue {
    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();
    private Collection<BooleanValue> values;
    private int trueValueCount;

    public static BooleanValue of(BooleanValue... values) {
        return of(Set.of(values));
    }

    public static BooleanValue of(Collection<BooleanValue> values) {
        Set<BooleanValue> andValues = new HashSet<>(values);
        for (BooleanValue value : values) {
            if (value instanceof And) {
                andValues.remove(value);
                andValues.addAll(((And)value).getValues());
            }
        }
        for (Iterator<BooleanValue> iterator = andValues.iterator(); iterator.hasNext(); ) {
            BooleanValue value = iterator.next();
            if (value instanceof Constant) {
                if (!value.getBoolean()) {
                    return BooleanValue.ofFalse();
                } else {
                    iterator.remove();
                }
            }
        }
        if (andValues.isEmpty()) {
            return BooleanValue.ofTrue();
        }
        return new And(andValues);
    }

    private And(Collection<BooleanValue> values) {
        this.values = values;
        for (BooleanValue value : values) {
            value.getBooleanChangeObserver().addListener(this::update);
            if (value.getBoolean()) {
                trueValueCount++;
            }
        }
    }

    private void update(boolean oldValue, boolean newValue) {
        if (newValue) {
            if (++trueValueCount == values.size()) {
                listeners.announce(true);
            }
        } else {
            if (trueValueCount -- == values.size()) {
                listeners.announce(false);
            }
        }
    }

    Collection<BooleanValue> getValues() {
        return values;
    }

    @Override
    public boolean getBoolean() {
        return trueValueCount == values.size();
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }

    @Override
    public String toString() {
        return "And{" +
                "values=" + values +
                '}';
    }
}
