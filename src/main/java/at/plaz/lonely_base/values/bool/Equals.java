package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeListeners;
import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.listeners.bool.SimpleBooleanChangeListeners;
import at.plaz.lonely_base.values.Value;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class Equals implements BooleanValue {
    private Value<?> firstValue;
    private Value<?> secondValue;
    private boolean currentValue;
    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();

    public Equals(Value<?> firstValue, Value<?> secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;

        firstValue.getChangeObserver().addListener((oldValue, newValue) -> updateAndAnnounce());
        secondValue.getChangeObserver().addListener((oldValue, newValue) -> updateAndAnnounce());
        update();
    }

    private void update() {
        currentValue = Objects.equals(firstValue.get(), secondValue.get());
    }

    private void updateAndAnnounce() {
        boolean oldValue = currentValue;
        currentValue = Objects.equals(firstValue.get(), secondValue.get());
        if (oldValue != currentValue) {
            listeners.announce(oldValue, currentValue);
        }
    }

    @Override
    public boolean getBoolean() {
        return currentValue;
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }
}
