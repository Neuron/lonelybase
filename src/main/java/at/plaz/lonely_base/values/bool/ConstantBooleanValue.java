package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.values.SimpleConstant;
import at.plaz.lonely_base.values.Value;

/**
 * Created by Georg Plaz.
 */
public class ConstantBooleanValue extends SimpleConstant<Boolean> implements BooleanValue {
    private boolean value;

    static BooleanValue FALSE = new ConstantBooleanValue(false);
    static BooleanValue TRUE = new ConstantBooleanValue(true);

    private ConstantBooleanValue(boolean value) {
        super(value);
        this.value = value;
    }

    @Override
    public boolean getBoolean() {
        return value;
    }

    @Override
    public BooleanValue not() {
        return BooleanValue.of(!value);
    }

    @Override
    public BooleanChangeObserver getChangeObserver() {
        return BooleanConstant.ConstantBooleanObserver.singleton();
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return BooleanConstant.ConstantBooleanObserver.singleton();
    }

    @Override
    public <A> Value<A> ifElse(A ifValue, A elseValue) {
        return Value.of(value ? ifValue : elseValue);
    }

    @Override
    public <A> Value<A> ifElse(A ifValue, Value<A> elseValue) {
        return value ? Value.of(ifValue) : elseValue;
    }

    @Override
    public <A> Value<A> ifElse(Value<A> ifValue, A elseValue) {
        return value ? ifValue : Value.of(elseValue);
    }

    @Override
    public <A> Value<A> ifElse(Value<A> ifValue, Value<A> elseValue) {
        return value ? ifValue : elseValue;
    }
}
