package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.listeners.ChangeObserver;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import at.plaz.lonely_base.function.BooleanConsumer;
import at.plaz.lonely_base.values.Constant;

/**
 * Created by Georg Plaz.
 */
public interface BooleanConstant extends Constant<Boolean>, BooleanValue {

    @Override
    default BooleanChangeObserver getChangeObserver() {
        return getBooleanChangeObserver();
    }

    @Override
    default BooleanChangeObserver getBooleanChangeObserver() {
        return ConstantBooleanObserver.singleton();
    }

    class ConstantBooleanObserver implements BooleanChangeObserver, ChangeObserver<Boolean> {
        private static final ConstantBooleanObserver singleton = new ConstantBooleanObserver();

        private ConstantBooleanObserver() {
        }

        @Override
        public void removeListener(BiConsumer<? super Boolean, ? super Boolean> listener) { }

        @Override
        public void addListener(Consumer<? super Boolean> listener) {

        }

        @Override
        public void removeListener(Consumer<? super Boolean> listener) {

        }

        @Override
        public void addListener(BiConsumer<? super Boolean, ? super Boolean> listener) { }

        @SuppressWarnings("unchecked")
        public static ConstantBooleanObserver singleton() {
            return singleton;
        }

        @Override
        public void removeListener(BooleanConsumer listener) {

        }

        @Override
        public void addListener(BooleanConsumer listener) {

        }
    }
}
