package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.values.Conditional;
import at.plaz.lonely_base.values.SimpleConstant;
import at.plaz.lonely_base.values.Value;

/**
 * Created by Georg Plaz.
 */
public interface BooleanValue extends Value<Boolean> {

    static BooleanValue of(boolean constant) {
        return constant ? ConstantBooleanValue.TRUE : ConstantBooleanValue.FALSE;
    }

    static BooleanValue ofTrue() {
        return ConstantBooleanValue.TRUE;
    }

    static BooleanValue ofFalse() {
        return ConstantBooleanValue.FALSE;
    }

    default <A> Value<A> ifElse(Value<A> ifValue, Value<A> elseValue) {
        return Conditional.of(this, ifValue, elseValue);
    }

    default <A> Value<A> ifElse(A ifValue, Value<A> elseValue) {
        return Conditional.of(this, new SimpleConstant<>(ifValue), elseValue);
    }

    default <A> Value<A> ifElse(Value<A> ifValue, A elseValue) {
        return Conditional.of(this, ifValue, new SimpleConstant<>(elseValue));
    }

    default <A> Value<A> ifElse(A ifValue, A elseValue) {
        return Conditional.of(this, new SimpleConstant<>(ifValue), new SimpleConstant<>(elseValue));
    }

    boolean getBoolean();

    default BooleanValue and(BooleanValue other) {
        return And.of(this, other);
    }

    default BooleanValue not() {
        return Not.of(this);
    }

    @Override
    default BooleanValue isPresent() {
        return BooleanValue.ofTrue();
    }

    @Override
    default Boolean get() {
        return getBoolean();
    }

    @Override
    default BooleanChangeObserver getChangeObserver() {
        return getBooleanChangeObserver();
    }

    BooleanChangeObserver getBooleanChangeObserver();




}
