package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeListeners;
import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.listeners.bool.SimpleBooleanChangeListeners;
import at.plaz.lonely_base.values.Constant;
import at.plaz.lonely_base.values.Value;

/**
 * Created by Georg Plaz.
 */
public class NotPresent implements BooleanValue {
    private Value<?> value;
    private boolean currentValue;
    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();

    public static BooleanValue of(Value<?> value) {
        if (value instanceof Constant) {
            return BooleanValue.of(value.valueNotPresent());
        }
        return new NotPresent(value);
    }

    private NotPresent(Value<?> value) {
        this.value = value;

        value.getChangeObserver().addListener((oldValue, newValue) -> updateAndAnnounce());
        update();
    }

    private void update() {
        currentValue = value.get() == null;
    }

    private void updateAndAnnounce() {
        boolean oldValue = currentValue;
        update();
        if (oldValue != currentValue) {
            listeners.announce(oldValue, currentValue);
        }
    }

    @Override
    public boolean getBoolean() {
        return currentValue;
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }
}
