package at.plaz.lonely_base.values.bool;

/**
 * Created by Georg Plaz.
 */
public class SimpleBooleanConstant implements BooleanConstant {
    public static final SimpleBooleanConstant FALSE = new SimpleBooleanConstant(false);
    public static final SimpleBooleanConstant TRUE = new SimpleBooleanConstant(true);

    private final boolean value;

    private SimpleBooleanConstant(boolean value) {
        this.value = value;
    }

    @Override
    public boolean getBoolean() {
        return value;
    }
}
