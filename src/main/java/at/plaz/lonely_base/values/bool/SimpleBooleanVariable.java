package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.function.BooleanConsumer;
import at.plaz.lonely_base.listeners.bool.*;
import at.plaz.lonely_base.values.PrimitiveVariable;
import at.plaz.lonely_base.values.Value;

/**
 * Created by Georg Plaz.
 */
public class SimpleBooleanVariable extends PrimitiveVariable<Boolean> implements BooleanVariable {

    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();
    private boolean value;
    private Value<? extends Boolean> boundTo;
    private BooleanConsumer booleanBoundListener = this::setValueWithoutChecking;

    public SimpleBooleanVariable() {
        this(false);
    }

    public SimpleBooleanVariable(boolean value) {
        this.value = value;
    }

    @Override
    public boolean getBoolean() {
        return value;
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }

    @Override
    public void setBoolean(boolean value) {
        checkSet();
        setValueWithoutChecking(value);
    }

    private void setValueWithoutChecking(boolean value) {
        boolean oldValue = this.value;
        this.value = value;
        listeners.announce(oldValue, value);
    }

    @Override
    public void bind(Value<? extends Boolean> other) {
        checkBind(other);
        unbind();
        boundTo = other;
        if (other instanceof BooleanValue) {
            bind((BooleanValue)other);
        } else {
            other.getChangeObserver().addListener(booleanBoundListener);
            setValueWithoutChecking(other.get());
        }
    }

    @Override
    public void bind(BooleanValue other) {
        checkBind(other);
        other.getBooleanChangeObserver().addListener(booleanBoundListener);
        setValueWithoutChecking(other.getBoolean());
    }

    @Override
    public void unbind() {
        checkUnbind();
        if (isBound()) {
            if (boundTo instanceof BooleanValue) {
                ((BooleanValue) boundTo).getBooleanChangeObserver().removeListener(booleanBoundListener);
            } else {
                boundTo.getChangeObserver().removeListener(booleanBoundListener);
            }
            boundTo = null;
        }
    }

    @Override
    public boolean isBound() {
        return boundTo != null;
    }
}
