package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.values.Variable;

/**
 * Created by Georg Plaz.
 */
public interface BooleanVariable extends BooleanValue, Variable<Boolean> {

    void setBoolean(boolean value);

    @Override
    default void set(Boolean value) {
        setBoolean(value);
    }

    void bind(BooleanValue other);
}
