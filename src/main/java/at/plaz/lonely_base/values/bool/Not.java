package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.listeners.bool.BooleanChangeListeners;
import at.plaz.lonely_base.listeners.bool.SimpleBooleanChangeListeners;

/**
 * Created by Georg Plaz.
 */
public class Not implements BooleanValue {
    private BooleanValue value;
    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();

    public static BooleanValue of(BooleanValue toNegate) {
        if (toNegate instanceof Not) {
            return ((Not) toNegate).value;
        }
        return new Not(toNegate);
    }

    private Not(BooleanValue value) {
        this.value = value;
        value.getBooleanChangeObserver().addListener((newValue -> listeners.announce(!newValue)));
    }

    @Override
    public boolean getBoolean() {
        return !value.getBoolean();
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }
}
