package at.plaz.lonely_base.values.bool;

import at.plaz.lonely_base.listeners.bool.BooleanChangeListeners;
import at.plaz.lonely_base.listeners.bool.BooleanChangeObserver;
import at.plaz.lonely_base.listeners.bool.SimpleBooleanChangeListeners;
import at.plaz.lonely_base.values.Value;

import java.util.function.Predicate;

/**
 * Created by Georg Plaz.
 */
public class Test<A> implements BooleanValue {
    private final Predicate<A> test;
    private BooleanChangeListeners listeners = new SimpleBooleanChangeListeners();
    private boolean lastResult;

    public Test(Value<A> value, Predicate<A> test) {
        this.test = test;
        value.getChangeObserver().addListener((this::updateThis));
        lastResult = test.test(value.get());
    }

    private void updateThis(A oldA, A newA) {
        boolean oldValue = lastResult;
        lastResult = test.test(newA);
        if (lastResult != oldValue) {
            listeners.announce(lastResult);
        }
    }

    @Override
    public boolean getBoolean() {
        return lastResult;
    }

    @Override
    public BooleanChangeObserver getBooleanChangeObserver() {
        return listeners;
    }
}
