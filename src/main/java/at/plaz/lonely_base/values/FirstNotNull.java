package at.plaz.lonely_base.values;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class FirstNotNull<A> extends AssembledValue<A, A> {

    public FirstNotNull(List<Value<A>> values) {
        super(values, s -> s.filter(Objects::nonNull).findFirst().orElse(null));
    }
}
