package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;

import java.util.Objects;
import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public class DeepGetter<A, B> implements Value<B> {
    private ChangeListeners<B> listeners = new SimpleChangeListeners<>();
    private Getter<A, Value<B>> getter;
    private Value<A> getFrom;
    private B currentV;

    public DeepGetter(Value<A> from, Function<A, Value<B>> getterFunction) {
        this.getFrom = from;
        this.getter = new Getter<>(from, getterFunction);

        this.getter.getChangeObserver().addListener(this::updateValue);
        Value<B> value = getter.get();
        updateValue(null, value);
    }

    private void updateValue(Value<B> oldValue, Value<B> newValue) {
        B oldV = null;
        B newV = null;
        if (oldValue != null) {
            oldValue.getChangeObserver().removeListener(this::update);
            oldV = oldValue.get();
        }
        if (newValue != null) {
            newValue.getChangeObserver().addListener(this::update);
            newV = newValue.get();
        }
        update(oldV, newV);
    }

    private void update(B oldV, B newV) {
        if (!Objects.equals(oldV, newV)) {
            currentV = newV;
            listeners.announce(oldV, currentV);
        }
    }

    @Override
    public B get() {
        return currentV;
    }

    @Override
    public ChangeObserver<B> getChangeObserver() {
        return listeners;
    }

    @Override
    public String toString() {
        return "DeepGetter{" +
                "currentV=" + currentV +
                ", getFrom=" + getFrom +
                '}';
    }
}
