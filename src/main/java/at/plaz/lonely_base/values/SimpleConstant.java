package at.plaz.lonely_base.values;

/**
 * Created by Georg Plaz.
 */
public class SimpleConstant<A> implements Constant<A> {
    private final A value;

    public SimpleConstant(A value) {
        this.value = value;
    }

    @Override
    public A get() {
        return value;
    }

}
