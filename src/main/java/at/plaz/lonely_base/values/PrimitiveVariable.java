package at.plaz.lonely_base.values;

import at.plaz.lonely_base.values.bool.BooleanValue;

/**
 * Created by Georg Plaz.
 */
public abstract class PrimitiveVariable<A> extends AbstractVariable<A> {
    @Override
    public BooleanValue isPresent() {
        return BooleanValue.ofTrue();
    }
}
