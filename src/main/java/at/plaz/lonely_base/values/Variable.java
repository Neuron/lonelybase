package at.plaz.lonely_base.values;

import java.util.function.Supplier;

/**
 * Created by Georg Plaz.
 */
public interface Variable<A> extends Value<A> {

    void set(A value);

    default void clear() {
        if (valueIsPresent()) {
            set(null);
        }
    }

    default void setIfNotPresent(A value) {
        if (valueNotPresent()) {
            set(value);
        }
    }

    default void supplyIfNotPresent(Supplier<A> supplier) {
        if (valueNotPresent()) {
            set(supplier.get());
        }
    }

    void bind(Value<? extends A> other);

    void unbind();

    boolean isBound();
}
