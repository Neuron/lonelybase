package at.plaz.lonely_base.values;

import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;
import at.plaz.lonely_base.values.string.StringValue;

import java.util.Objects;
import java.util.function.Function;

/**
 * Created by Georg Plaz.
 */
public class ToString<A> implements StringValue {
    private ChangeListeners<String> listeners = new SimpleChangeListeners<>();
    private Value<A> value;
    private Function<A, String> toStringFunction;
    private String stringValue;

    public ToString(Value<A> value, String ifNotPresent) {
        this(value, v -> v != null ? v.toString() : ifNotPresent);
    }

    public ToString(Value<A> value) {
        this(value, v -> v != null ? v.toString() : null);
    }

    public ToString(Value<A> value, Function<A, String> toStringFunction) {
        this.value = value;
        this.toStringFunction = toStringFunction;
        value.getChangeObserver().addListener(((oldValue, newValue) -> update()));
    }

    @Override
    public String get() {
        return toStringFunction.apply(value.get());
    }

    private void update() {
        String oldValue = stringValue;
        stringValue = get();
        if (!Objects.equals(oldValue, stringValue)) {
            listeners.announce(oldValue, stringValue);
        }
    }

    @Override
    public ChangeObserver<String> getChangeObserver() {
        return listeners;
    }
}
