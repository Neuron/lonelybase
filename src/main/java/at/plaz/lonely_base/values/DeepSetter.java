package at.plaz.lonely_base.values;

import java.util.function.Function;
import at.plaz.lonely_base.listeners.ChangeListeners;
import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.listeners.SimpleChangeListeners;

import java.util.Objects;

/**
 * Created by Georg Plaz.
 */
public class DeepSetter<A, B> extends AbstractBoundVariable<B> {
    private ChangeListeners<B> listeners = new SimpleChangeListeners<>();
    private Variable<A> from;
    private Function<A, B> valueGetter;
    private Function<B, A> converter;
    private B value;

    public DeepSetter(Variable<A> from, Function<A, B> valueGetter, Function<B, A> converter) {
        this.from = from;
        this.valueGetter = valueGetter;
        this.converter = converter;
        updateThis();
        from.getChangeObserver().addListener(((oldValue, newValue) -> updateThis()));
    }

    private void updateThis() {
        B oldValue = value;
        if (from.valueIsPresent()) {
            value = valueGetter.apply(from.get());
        } else {
            value = null;
        }
        if (!Objects.equals(oldValue, value)) {
            listeners.announce(oldValue, value);
        }
    }

    @Override
    public boolean isBound() {
        return super.isBound() || from.isBound();
    }

    @Override
    void setValueWithoutChecking(B value) {
        from.set(converter.apply(value));
    }

    @Override
    public B get() {
        return value;
    }

    @Override
    public ChangeObserver<B> getChangeObserver() {
        return listeners;
    }
}
