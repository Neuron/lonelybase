package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.values.Constant;
import at.plaz.lonely_base.values.ToString;
import at.plaz.lonely_base.values.Value;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class StringValues {

    public static Concatenate concatenate(Value<String> first, Value<String> second) {
        return new Concatenate(List.of(first, second));
    }

    public static Concatenate concatenate(Value<String> first, String second, Value<String> third) {
        return new Concatenate(List.of(first, new SimpleStringConstant(second), third));
    }

    public static Concatenate concatenate(String first, Value<String> second) {
        return new Concatenate(List.of(new SimpleStringConstant(first), second));
    }

    public static Concatenate concatenate(Value<String> first, String second) {
        return new Concatenate(List.of(first, new SimpleStringConstant(second)));
    }

    public static Concatenate concatenate(String first, Value<String> second, String third) {
        return new Concatenate(List.of(new SimpleStringConstant(first), second, new SimpleStringConstant(third)));
    }

    public static Concatenate concatenate(String first, Value<String> second, Value<String> third) {
        return new Concatenate(List.of(new SimpleStringConstant(first), second, third));
    }

    public static Concatenate concatenate(String first, Value<String> second, String third, Value<String> fourth) {
        return new Concatenate(List.of(new SimpleStringConstant(first), second, new SimpleStringConstant(third), fourth));
    }

    public static Concatenate concatenate(String first, Value<String> second, Value<String> third, Value<String> fourth) {
        return new Concatenate(List.of(new SimpleStringConstant(first), second, third, fourth));
    }

    public static <A> ToString toString(Value<A> value) {
        return new ToString<>(value);
    }
}
