package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.values.Variable;

/**
 * Created by Georg Plaz.
 */
public interface StringVariable extends StringValue, Variable<String> {


}
