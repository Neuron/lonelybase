package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.listeners.ChangeObserver;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import at.plaz.lonely_base.values.Constant;

/**
 * Created by Georg Plaz.
 */
public interface StringConstant extends Constant<String>, StringValue {

    @Override
    default ChangeObserver<String> getChangeObserver() {
        return ConstantStringObserver.singleton();
    }

    class ConstantStringObserver implements ChangeObserver<String> {
        private static final ConstantStringObserver singleton = new ConstantStringObserver();

        private ConstantStringObserver() {
        }

        @Override
        public void removeListener(BiConsumer<? super String, ? super String> listener) { }

        @Override
        public void addListener(Consumer<? super String> listener) { }

        @Override
        public void removeListener(Consumer<? super String> listener) { }

        @Override
        public void addListener(BiConsumer<? super String, ? super String> listener) { }

        @SuppressWarnings("unchecked")
        public static ConstantStringObserver singleton() {
            return singleton;
        }
    }
}
