package at.plaz.lonely_base.values.string;

/**
 * Created by Georg Plaz.
 */
public class SimpleStringConstant implements StringConstant {
    private final String value;

    public SimpleStringConstant(String value) {
        this.value = value;
    }

    @Override
    public String get() {
        return value;
    }
}
