package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.listeners.ChangeObserver;
import at.plaz.lonely_base.values.SimpleConstant;
import at.plaz.lonely_base.values.Value;

/**
 * Created by Georg Plaz.
 */
public interface StringValue extends Value<String> {
    static StringValue of(String constant) {
        return new SimpleStringConstant(constant);
    }

    default StringValue concatenate(Value<String> with) {
        return StringValues.concatenate(this, with);
    }

    default StringValue concatenate(String with) {
        return StringValues.concatenate(this, with);
    }

    @Override
    default StringValue toStringValue() {
        return this;
    }
}
