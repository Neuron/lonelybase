package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.values.SimpleVariable;

/**
 * Created by Georg Plaz.
 */
public class SimpleStringVariable extends SimpleVariable<String> implements StringVariable {

    public SimpleStringVariable() {
    }

    public SimpleStringVariable(String value) {
        super(value);
    }

    public void set(SimpleVariable<String> other) {
        super.set(other.get());
    }
}
