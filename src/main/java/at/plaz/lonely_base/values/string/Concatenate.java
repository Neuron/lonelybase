package at.plaz.lonely_base.values.string;

import at.plaz.lonely_base.values.AssembledValue;
import at.plaz.lonely_base.values.Value;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Georg Plaz.
 */
public class Concatenate extends AssembledValue<String, String> implements StringValue {

    public Concatenate(Collection<Value<String>> values) {
        super(values, Collectors.joining());
    }


}
