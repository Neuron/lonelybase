package at.plaz.lonely_base.k2_tree;

/**
 * Created by Georg Plaz.
 */
public interface PositionRetreiver<A> {
    double getX(A object);
    double getY(A object);
}
