package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public interface K2Node<A> extends Rectangle {

    static <A> K2Node<A> of(Vector2D position, Vector2D dimensions, int depth, int targetDepth) {
       return depth < targetDepth ?
               new ParentK2Node<>(position, dimensions, depth, targetDepth) :
               new K2LeafNode<>(position, dimensions);
    }

    boolean remove(A object, Vector2D position);

    void populate(Rectangle bounding, Collection<A> toPopulate);

    void populate(Rectangle bounding, Stream.Builder<A> builder);

    void store(A object, Vector2D position);

    String getInfos(String indentation);

    void populateBalance(Collection<Integer> balance);
}
