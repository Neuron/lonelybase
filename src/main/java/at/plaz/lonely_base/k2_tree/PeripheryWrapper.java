package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by Georg Plaz.
 */
public class PeripheryWrapper<A> implements UnlimitedPointCollection2D<A> {
    private PointCollection2D<A> delegate;
    private Multimap<A, ImmutableVector2D> outOfBounds = HashMultimap.create();

    public PeripheryWrapper(PointCollection2D<A> delegate) {
        this.delegate = delegate;
    }

    @Override
    public int size() {
        return delegate.size() + outOfBounds.size();
    }

    @Override
    public void store(A object, Vector2D position) {
        ImmutableVector2D immutablePosition = position.immutable();
        if (delegate.getBounds().contains(position)) {
            delegate.store(object, position);
        } else {
            outOfBounds.put(object, immutablePosition);
        }
    }

    @Override
    public Rectangle getBounds() {
        return Rectangle.containingAll();
    }

    @Override
    public Collection<A> getObjects() {
        Collection<A> delegateObjects = delegate.getObjects();
        if (outOfBounds.isEmpty()) {
            return delegateObjects;
        } else if (delegateObjects.isEmpty()) {
            return outOfBounds.keys();
        } else {
            Collection<A> result = new LinkedList<>();
            result.addAll(delegateObjects);
            result.addAll(outOfBounds.keys());
            return result;
        }
    }

    @Override
    public boolean remove(A object, Vector2D position) {
        ImmutableVector2D immutablePosition = position.immutable();
        if (delegate.getBounds().contains(immutablePosition)) {
            return delegate.remove(object, immutablePosition);
        } else {
            return outOfBounds.remove(object, immutablePosition);
        }
    }

    @Override
    public void reMap(A object, Vector2D oldPos, Vector2D newPos) {
        ImmutableVector2D immutableOld = oldPos.immutable();
        ImmutableVector2D immutableNew = newPos.immutable();
        boolean containsOld = delegate.getBounds().contains(immutableOld);
        boolean containsNew = delegate.getBounds().contains(immutableNew);
        if (containsOld && containsNew) {
            delegate.reMap(object, immutableOld, immutableNew);
        } else {
            if (containsOld) {
                delegate.remove(object, immutableOld);
            } else {
                outOfBounds.remove(object, immutableOld);
            }
            if (containsNew) {
                delegate.store(object, immutableNew);
            } else {
                outOfBounds.put(object, immutableNew);
            }
        }
    }

    @Override
    public Collection<A> get(Rectangle rectangle) {
        Collection<A> delegateObjects = delegate.get(rectangle);
        if (outOfBounds.isEmpty()) {
            return delegateObjects;
        } else if (delegateObjects.isEmpty()) {
            return outOfBounds.keys();
        } else {
            Collection<A> result = new LinkedList<>();
            result.addAll(delegateObjects);
            result.addAll(outOfBounds.keys());
            return result;
        }
    }
}
