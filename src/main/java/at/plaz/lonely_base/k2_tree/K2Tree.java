package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class K2Tree<A> implements PointCollection2D<A> {
    private final int maxDepth;
    private K2Node<A> rootNode;
    private Multiset<A> allObjects = HashMultiset.create();
    private Collection<A> publicReference = Collections.unmodifiableCollection(allObjects);

    public K2Tree(double width, double height, int maxDepth) {
        this(Vector2D.ZEROS, Vector2D.of(width, height), maxDepth);
    }

    public K2Tree(Vector2D position, Vector2D dimensions, int maxDepth) {
        rootNode = K2Node.of(position, dimensions, 0, maxDepth);
        this.maxDepth = maxDepth;
    }

    @Override
    public Collection<A> get(Rectangle rectangle) {
        Collection<A> objects = new LinkedList<>();
        rootNode.populate(rectangle, objects);
        return objects;
    }

    public Stream<A> stream(Rectangle rectangle) {
        Stream.Builder<A> builder = Stream.builder();
        rootNode.populate(rectangle, builder);
        return builder.build();
    }

    @Override
    public int size() {
        return allObjects.size();
    }

    @Override
    public void store(A object, Vector2D position) {
        allObjects.add(object);
        rootNode.store(object, position);
    }

    @Override
    public Rectangle getBounds() {
        return rootNode;
    }

    @Override
    public void reMap(A object, Vector2D oldPos, Vector2D newPos) {
        if (rootNode.remove(object, oldPos)) {
            rootNode.store(object, newPos);
        }
    }

    @Override
    public Collection<A> getObjects() {
        return publicReference;
    }

    @Override
    public boolean remove(A object, Vector2D position) {
        if (rootNode.remove(object, position)) {
            allObjects.remove(object);
            return true;
        }
        return false;
    }

    public Collection<Integer> getBalances() {
        List<Integer> balance = new ArrayList<>(1 << maxDepth);
        rootNode.populateBalance(balance);
        return balance;
    }
}
