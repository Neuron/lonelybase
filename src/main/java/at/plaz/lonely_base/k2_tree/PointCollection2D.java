package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Georg Plaz.
 */
public interface PointCollection2D<A> extends Iterable<A> {
    int size();

    default void store(A object, double x, double y) {
        store(object, Vector2D.of(x, y));
    }

    void store(A object, Vector2D position);

    Rectangle getBounds();

    Collection<A> getObjects();

    boolean remove(A object, Vector2D position);

    default boolean remove(A object, double x, double y) {
        return remove(object, Vector2D.of(x, y));
    }

    void reMap(A object, Vector2D oldPos, Vector2D newPos);

    Collection<A> get(Rectangle rectangle);

    default Collection<A> get(Vector2D position, Vector2D dimensions) {
        return get(Rectangle.ofFast(position, dimensions));
    }

    @Override
    default Iterator<A> iterator() {
        return getObjects().iterator();
    }
}
