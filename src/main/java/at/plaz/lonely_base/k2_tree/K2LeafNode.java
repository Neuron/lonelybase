package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.ImmutableFastRectangle;
import at.plaz.lonely_base.shapes.Rectangle;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class K2LeafNode<A> extends ImmutableFastRectangle implements K2Node<A> {
    private Multimap<A, Vector2D> objects = HashMultimap.create();

    public K2LeafNode(Vector2D position, Vector2D dimensions) {
        super(position, dimensions);
    }

    @Override
    public boolean remove(A object, Vector2D position) {
        return objects.remove(object, position);
    }

    @Override
    public void populate(Rectangle bounding, Collection<A> toPopulate) {
        objects.forEach((k, v) -> {
            if (bounding.contains(v)) {
                toPopulate.add(k);
            }
        });
    }

    @Override
    public void populate(Rectangle bounding, Stream.Builder<A> builder) {
        objects.forEach((k, v) -> {
            if (bounding.contains(v)) {
                builder.accept(k);
            }
        });
    }

    @Override
    public void store(A object, Vector2D position) {
        objects.put(object, position);
    }

    @Override
    public String getInfos(String indentation) {
        return indentation + "rectangle: " + getPosition() + ", " + getDimensions() + "\n" +
                indentation + "size:" + objects.size();
    }

    @Override
    public void populateBalance(Collection<Integer> balance) {
        balance.add(objects.size());
    }
}
