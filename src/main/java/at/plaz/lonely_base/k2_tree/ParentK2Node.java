package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.ImmutableFastRectangle;
import at.plaz.lonely_base.shapes.Rectangle;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class ParentK2Node<A> extends ImmutableFastRectangle implements K2Node<A> {
    private final int decisionDimension;
    private int depth;

    private K2Node<A> first;
    private K2Node<A> second;

    private double decisionBoundary;

    public ParentK2Node(Vector2D position, Vector2D dimensions, int depth, int targetDepth) {
        super(position, dimensions);
        this.depth = depth;
        this.decisionDimension = dimensions.getX() > dimensions.getY() ? 0 : 1;
        ImmutableVector2D dimensionVector = Vector2D.of((decisionDimension + 1 ) % 2, decisionDimension);
        ImmutableVector2D subDimensions = dimensions.getSubtracted(dimensionVector.getMultiplied(dimensions.getDivided(2)));
        ImmutableVector2D secondSubPosition = position.getAdded(subDimensions.getMultiplied(dimensionVector));

        first = K2Node.of(position, subDimensions, depth + 1, targetDepth);
        second = K2Node.of(secondSubPosition, subDimensions, depth + 1, targetDepth);

        decisionBoundary = position.get(decisionDimension) + getDimensions().get(decisionDimension) / 2;


//            first = K2Node.of(x, y, width, height / 2, depth, targetDepth);
//            second = K2Node.of(x, y + height / 2, width, height / 2, depth, targetDepth);
    }

    @Override
    public void store(A object, Vector2D position) {
        getNode(position).store(object, position);
    }

    @Override
    public boolean remove(A object, Vector2D position) {
        return getNode(position).remove(object, position);
    }

    public K2Node<A> getNode(Vector2D position) {
        return intersectsFirst(position) ? first : second;
    }

    private boolean intersectsFirst(Vector2D position) {
        return position.get(decisionDimension) < decisionBoundary;
    }

    private boolean intersectsSecond(Vector2D position, Vector2D dimensions) {
        return position.get(decisionDimension) + dimensions.get(decisionDimension) > decisionBoundary;
    }

    @Override
    public void populate(Rectangle bounds, Collection<A> toPopulate) {
        ImmutableVector2D position = bounds.getPosition();
        if (intersectsFirst(position)) {
            first.populate(bounds, toPopulate);
        }
        if (intersectsSecond(position, bounds.getDimensions())) {
            second.populate(bounds, toPopulate);
        }
    }

    @Override
    public void populate(Rectangle bounds, Stream.Builder<A> builder) {
        ImmutableVector2D position = bounds.getPosition();
        if (intersectsFirst(position)) {
            first.populate(bounds, builder);
        }
        if (intersectsSecond(position, bounds.getDimensions())) {
            second.populate(bounds, builder);
        }
    }

    @Override
    public String getInfos(String indentation) {
        return indentation + "split dimension: " + decisionDimension + "\n" +
                indentation + "rectangle: " + getPosition() + ", " + getDimensions() + "\n" +
                first.getInfos(indentation + "  ") + "\n" +
                second.getInfos(indentation + "  ");
    }

    @Override
    public void populateBalance(Collection<Integer> balances) {
        first.populateBalance(balances);
        second.populateBalance(balances);
    }
}
