package at.plaz.lonely_base.k2_tree;

import at.plaz.lonely_base.math.ImmutableVector2D;
import at.plaz.lonely_base.math.Vector2D;
import at.plaz.lonely_base.shapes.Rectangle;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by Georg Plaz.
 */
public interface UnlimitedPointCollection2D<A> extends PointCollection2D<A> {

}
