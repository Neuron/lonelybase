package at.plaz.lonely_base;

/**
 * Created by Georg Plaz.
 */
public interface Validator<A> {
    boolean isValid(A value);
}
