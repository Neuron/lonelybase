package at.plaz.lonely_base.streams;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Georg Plaz.
 */
public class ExtraCollectors {
    public static <T> Collector<T, ?, T> toSingletonOrThrow() {
        return toSingletonOrThrow(i -> new IllegalStateException());
    }

    public static <T> Collector<T, ?, T> toSingletonOrThrow(Function<Integer, RuntimeException> throwIfSizeMismatch) {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw throwIfSizeMismatch.apply(list.size());
                    }
                    return list.get(0);
                }
        );
    }

    public static <T> Collector<T, ?, Optional<T>> toSingleton() {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> list.size() == 1 ? Optional.of(list.get(0)) : Optional.empty()
        );
    }
}
