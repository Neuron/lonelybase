package at.plaz.lonely_base.streams;

import at.plaz.lonely_base.tuples.ImmutableCongruentTuple;
import at.plaz.lonely_base.tuples.ImmutableTuple;
import at.plaz.lonely_base.tuples.Tuple;
import com.google.common.collect.Streams;

import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by Georg Plaz.
 */
public class ExtraStreams {

    public static <K, V> Stream<ImmutableTuple<K, V>> stream(Map<K, V> map) {
        return map.entrySet().stream().map(e -> Tuple.of(e.getKey(), e.getValue()));
    }

    public static <K> Stream<ImmutableTuple<K, K>> streamCongruent(Map<K, K> map) {
        return map.entrySet().stream().map(e -> Tuple.of(e.getKey(), e.getValue()));
    }

    public static Stream<String> append(Stream<? extends ImmutableTuple<String, String>> tupleStream) {
        return tupleStream.map(t -> t.getFirst() + t.getSecond());
    }

    public static <A> Stream<ImmutableTuple<A, Integer>> zipEnumerated(Stream<A> first) {
        return Streams.zip(first, Stream.iterate(0, i -> i + 1), Tuple::of);
    }

    public static <A> Stream<ImmutableTuple<A, Integer>> zipEnumerated(Stream<? extends A> first, Predicate<? super A> predicate) {
        return Streams.zip(first.takeWhile(predicate), Stream.iterate(0, i -> i + 1), Tuple::of);
    }

    public static <A, B> Stream<A> iterate(A seed, List<B> transitionElements, BiFunction<A, B, A> transition) {
        Function<Tuple<A, Integer>, A> nextFromTuple = tuple -> transition.apply(tuple.getFirst(), transitionElements.get(tuple.getSecond()));
        UnaryOperator<Tuple<A, Integer>> nextTuple = tuple -> Tuple.of(nextFromTuple.apply(tuple), tuple.getSecond() + 1);
        Predicate<Tuple<A, Integer>> predicate = tuple -> tuple.getSecond() < transitionElements.size();
        Stream<Tuple<A, Integer>> tupleStream = Stream.iterate(Tuple.of(seed, 0), predicate, nextTuple);
        return tupleStream.map(Tuple::getFirst);
    }

    public static <A, B> Stream<A> iterateOptional(A seed, List<B> transitionElements, BiFunction<A, B, Optional<A>> transition) {
        Function<Tuple<A, Integer>, Optional<A>> nextFromTuple = tuple -> {
            if (tuple.getSecond() >= transitionElements.size()) {
                return Optional.empty();
            } else {
                return transition.apply(tuple.getFirst(), transitionElements.get(tuple.getSecond()));
            }
        };
        Function<Tuple<A, Integer>, Optional<Tuple<A, Integer>>> nextTuple =
                tuple -> nextFromTuple.apply(tuple).map(nextElement -> Tuple.of(nextElement, tuple.getSecond() + 1));

        return iterateOptional(Tuple.of(seed, 0), nextTuple).map(Tuple::getFirst);
    }

    public static <A> Stream<A> iterateOptional(A seed, Function<A, Optional<A>> next) {
        Predicate<A> predicate = a -> next.apply(a).isPresent();
        return Stream.iterate(seed, predicate, a -> next.apply(a).orElseThrow());
    }

    public static <A> Stream<ImmutableTuple<A, Integer>> zipEnumerated(Collection<? extends A> first) {
        return Streams.zip(first.stream(), Stream.iterate(0, i -> i + 1), Tuple::of);
    }

    public static <A, B> Stream<ImmutableTuple<A, B>> zip(Stream<? extends A> first, Stream<? extends B> second) {
        return Streams.zip(first, second, Tuple::of);
    }

    public static <A, B> void zipForEach(Stream<? extends A> first, Stream<? extends B> second, BiConsumer<A, B> forEach) {
        forEach(zip(first, second), forEach);
    }

    public static <A, B> Stream<ImmutableTuple<A, B>> zip(Iterable<? extends A> first, Iterable<? extends B> second) {
        return zip(StreamSupport.stream(first.spliterator(), false), StreamSupport.stream(second.spliterator(), false));
    }

    public static <A, B> void zipForEach(Iterable<? extends A> first, Iterable<? extends B> second, BiConsumer<A, B> forEach) {
        forEach(zip(first, second), forEach);
    }

    public static <A, B> Stream<ImmutableTuple<A, B>> zipConstant(Stream<? extends A> first, B constant) {
        return Streams.zip(first, infinite(constant), Tuple::of);
    }

    public static <A> Stream<ImmutableCongruentTuple<A>> congruentZip(Collection<? extends A> first, Collection<? extends A> second) {
        return Streams.zip(first.stream(), second.stream(), Tuple::ofCongruent);
    }

    public static <A, B, C, D> Stream<ImmutableTuple<C, D>> map(Stream<? extends ImmutableTuple<A, B>> tupleStream,
                                                                Function<A, C> firstMap, Function<B, D> secondMap) {
        return tupleStream.map(t -> Tuple.of(firstMap.apply(t.getFirst()), secondMap.apply(t.getSecond())));
    }

    public static <A, B> Stream<ImmutableCongruentTuple<B>> map(Stream<? extends ImmutableTuple<A, A>> tupleStream,
                                                                Function<A, B> mapping) {
        return tupleStream.map(t -> Tuple.ofCongruent(mapping.apply(t.getFirst()), mapping.apply(t.getSecond())));
    }

    public static <A, B> void forEach(Stream<? extends ImmutableTuple<A, B>> stream, BiConsumer<? super A, ? super B> consumer) {
        stream.forEach(t -> consumer.accept(t.getFirst(), t.getSecond()));
    }

    public static <A, B> Stream<ImmutableTuple<A, B>> zip(Collection<? extends A> first, Collection<? extends B> second) {
        return Streams.zip(first.stream(), second.stream(), Tuple::of);
    }

    public static <A> Stream<A> infinite(A value) {
        return Stream.iterate(value, v -> v);
    }

}
